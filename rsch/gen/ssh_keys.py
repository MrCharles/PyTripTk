#!/usr/bin/env python3

'''
Generates and parses dummy SSH keys for research purposes.

The purpose and intent of this file and it's methods,
is to be able to generate large semi-prime pairs used
in async RSA security. (For further analysis.)

README:
    This is a one-off. Development is not active. It is
    updated unless or until the developer feels like it
    or needs additional or different features. It is
    quickly hacked together over a few days, without
    further intent to refine or continue developing it.
    NAMING CONVENTIONS are inconsistent. In some places ...
    `bits` is used, in other places `b`
    `method` is used, in other places `m`
    `name` is used, in other places `nom`
    IF YOU USE THIS PLEASE PAY CLOSE, CAREFUL ATTENTION
    AND REVIEW THE CODE IF NECESSARY.

Requires system commands:
    ssh-keygen
    openssl

ssh-keygen:
    This should automate the creation of SSH keys.
    (Default for bits between 1024 and 4096.)

openssl:
    This can automate the creation of SSH keys...
    (Default for 512 <= bits < 1024, or bits > 4096)
    AND, This reads the created PRIVATE key for the
    internal prime number and modulus (n) representation(s).

============================================================
NOTES / TODO / MISC:
============================================================
    (0) complete implementation of limits (!!!)

    (1) rng-tools or havged:
        -- detect if running (background daemon), or
        -- run between each generation
        -- Veracity: This is for research purposes,
            a non-critical application. But for needing
            true randomness.
        -- as of initial development, the developer has
            haveged running in the background (no issues)

    (2) statistics for larger gen2 runs.
        -- implement for all (single and multiple)

    (3) clean-up inconsistent naming conventions (refactor)

    (4) Eventually migrate this to a (partitioned) database,
        -- because that's what it's becoming ...

    (?) ... more stuff to go here as or when needed ...
============================================================
============================================================
'''

# Python lib
import os
import tempfile as tf

# 3rd party
import gmpy2 as gmp

PREFSSHKGEN=0 # 1024-4096
PREFOPENSSL=0 # ToDo: Might allow < 1024 or > 4096 (???) :oDoT
LOCATION=os.path.realpath(__file__)
BGENPATH=os.path.dirname(LOCATION)
BASEPATH=str(os.sep).join(BGENPATH.split(os.sep)[0:-1])
SAVEFILE0="BigNum_RSA_SSHKGEN.py"
SAVEFILE1="BigNum_RSA_OPENSSL.py"
SAVEPATH0=os.path.join(BASEPATH, SAVEFILE0)
SAVEPATH1=os.path.join(BASEPATH, SAVEFILE1)
NEXTLINE="#===================== NEXT ============ NEXT ====================="
NEXTLIST="        ] # ========== NEXT ========== LIST ==========\n"
HEADERS = {
        "modulus"         : "mod",
        "publicExponent"  : "pubExp",
        "privateExponent" : "privExp",
        "prime1"          : "f1",
        "prime2"          : "f2",
        "exponent1"       : "exp1",
        "exponent2"       : "exp2",
        "coefficient"     : "coeff"
        }

def _file_cleanup(filename=None, debug=False):
    try:
        assert not filename is None
        assert isinstance(filename, str)
        assert os.path.exists(filename)
        try:
            assert filename.startswith("/tmp")
            if os.path.exists(filename):
                cmd = "rm -f {}".format(filename)
                v2 = os.system(cmd)
            pubfile = "{}.pub".format(filename)
            if os.path.exists(pubfile):
                cmd = "rm -f {}".format(pubfile)
                v2 = os.system(cmd)
            outfile = "{}.out".format(filename)
            if os.path.exists(outfile):
                cmd = "rm -f {}".format(outfile)
                v2 = os.system(cmd)
        except:
            pass
    except:
        pass

def _make_rsa_key(bits=1024, filename=None, m=0, debug=False):
    '''Makes an ssh key with the given parameters.
    Writes to the `/tmp` folder.
    Returns the name of the (private) key-file written.
    (Writing includes or entails the creation of a public key as well.
        which should be cleaned-up alongside the private key once done.)

    Takes three arguments:
        bits, filename, debug

    bits -- is required to be an integer between 1024 and 4096 (inclusive).

    filename -- is required to be a string, and be in the temp directory.
        if not specified, it is generated and returned by the function.

    debug -- is set to False, by default. Turning it on (True) results
        in printing the ssh-keygen system command to the console.

    '''
    #assert not filename is None, "filename cannot be None"
    if filename is None:
        filename = tf.mktemp('_sshrsa', '', '/tmp')
    assert isinstance(filename, str), "filename must be a string"
    assert not os.path.exists(filename), "file {} exists".format(filename)
    assert filename.startswith('/tmp/'), "file should go into the temp (/tmp) directory"
    assert isinstance(bits, int), "bits must be an integer"
    assert bits >= 512 and bits <= 2**14, "bits must be >= 512, and <=2**14 (16384)"
    assert m in [0, 1], "the mode `m` must be an integer, either 0 or 1"
    try:
        cmd = None
        if m == 0:
            cmd = "ssh-keygen -b {} -t rsa -f {} -q -N {}".format(bits, filename, '""')
        elif m == 1:
            cmd = "openssl genpkey -algorithm RSA -out {} -pkeyopt rsa_keygen_bits:{}".format(filename, bits)
        else:
            raise Exception("m must be 0 or 1")
        if debug: print("Executing: '''{}'''".format(cmd))
        var = os.system(cmd)
        assert var == 0, "Exit Code: {}".format(var)
        return filename
    except AssertionError as ae:
        print("\n\tsystem command `ssh-keygen` raised an error during execution")
        print("\t\tcommand '''{}'''\n".format(cmd))
        _file_cleanup(filename)
        raise ae
    except:
        print("\t\tcommand '''{}'''\n".format(sysstr))
        _file_cleanup(filename)
        raise

def _openssl_read(filename=None, m=None, debug=False):
    '''Uses the system command, `openssl`, to read a generated key-pair.
    Comes after `_make_rsa_key`.
    '''
    assert not filename is None, "filename cannot be None"
    assert isinstance(filename, str), "filename must be a string"
    assert os.path.exists(filename), "filename must exist"
    assert filename.startswith("/tmp"), "filename must be in /tmp"
    assert not m is None and m in [0, 1], "the method, `m` must be an integer in [0, 1]"
    try:
        cmd = "openssl rsa -text -noout < {} > {}".format(filename, filename+".out")
        var = os.system(cmd)
        assert var == 0, "Exit Code: {}".format(var)
        return "{}".format(filename+".out")
    except AssertionError as ae:
        print("\n\tsystem command `openssl` raised an error during execution")
        print("\t\tcommand '''{}'''\n".format(cmd))
        _file_cleanup(filename)
        raise ae
    except:
        raise

def _external_parse(filename=None, debug=False):
    '''Reads the file generated by `_openssl_read`
    Returns the data set.

    The header/first line should be: Private-Key (xxxx bit)

    This function expects EXACTLY 8 indented sections:
        (1) modulus
        (2) publicExponent (typically: 65537)
        (3) privateExponent
        (4) prime1
        (5) prime2
        (6) exponent1
        (7) exponent2
        (8) coefficient

    Additional sections or information may be presented or present,
    however, at least these sections should be available to parse,
    in the above specified order (preferably).
    '''
    assert not filename is None, "filename cannot be None"
    assert isinstance(filename, str), "filename must be a string"
    assert os.path.exists(filename), "filename must exist"
    assert filename.startswith("/tmp/"), "filename must be in /tmp"
    dct = {}
    wip = ''
    with open(filename, 'rb') as _fh:
        fil = _fh.read()
    fil = fil.decode('utf-8')
    if debug: print(fil)
    # yeah, it's hackish; but it works!
    fil = fil.replace(' ', '')
    fil = fil.replace("t:\n", "t|")
    fil = fil.replace("me1:\n", "me1|")
    fil = fil.replace("me2:\n", "me2|")
    fil = fil.replace("t1:\n", "t1|")
    fil = fil.replace("t2:\n", "t2|")
    fil = fil.replace("s:\n", "s|")
    fil = fil.replace("t:", "t|")
    fil = fil.replace("y:", "y|")
    fil = fil.replace(":\n", ":")
    if debug: print(fil)
    #return fil
    fil = fil.split('\n')
    for x in fil:
        if x == "": continue
        try:
            tmp = x.split('|')
            assert len(tmp) == 2, "unsuccessful parse!"
            if tmp[0] in HEADERS.keys():
                dct[HEADERS[tmp[0]]] = ''.join(tmp[1].split(':'))
                if HEADERS[tmp[0]] in ["mod", "f1", "f2", "exp1", "exp2", "coeff", "privExp"]:
                    dct[HEADERS[tmp[0]]] = gmp.mpz(dct[HEADERS[tmp[0]]], base=16)
            else:
                dct[tmp[0]] = tmp[1]
        except IndexError as ie:
            print(x)
            raise ie
    if "Private-Key" in dct.keys():
        dct["bits"] = dct["Private-Key"].replace('(', '').replace(')', '').replace("bit", '')
        del dct["Private-Key"]
    if "pubExp" in dct:
        if "(" in dct["pubExp"]:
            dct["pubExp"] = dct["pubExp"].split('(')[0]
    if "publicExponent" in dct:
        if "(" in dct["publicExponent"]:
            dct["publicExponent"] = dct["publicExponent"].split('(')[0]
    assert all([True if x in dct.keys() else False for x in ["mod", "f1", "f2"]]), "parsed dictionary lacks necessary keys"
    assert dct["mod"] == dct["f1"]*dct["f2"], "mod != f1*f2"
    return dct

def ss80(s):
    '''Slices a String into 80-character chunks, and
    inserts continuation \ at line ends.

    Quick and dirty: Assumes a string is quoted before
    the 80'th character, is quoted at the end, and does
    not add a \ to the end after the last character in the string.

    Should result in a quoted string as a block of text about 80-characters on each line.
    '''
    if len(s) <= 80: return s
    else:
        r = s[0:80]+"\\"+"\n"
        t = s[80:]
        while len(t) > 80:
            r += t[0:80]+"\\"+"\n"
            t = t[80:]
        if len(t) > 0:
            if len(t) < 10:
                r = r[0:-2]
            r += t
        try:
            assert r.replace('\\', '').replace('\n', '').strip('\n') == s, "bad 80-character string replacement!"
        except:
            print(r)
            print(r.replace('\\', '').replace('\n', ''))
            print(s)
            raise
        return r

def gen(b=1024, m=0, dbg=False):
    '''Generates and returns an SSH-RSA key's info,
    of a given bit-length (variable `b`).

    Variable `m` is for "method" and it can be 1 or 0 (default=0).
        IIF 0 ::: THEN ssh-keygen
        IIF 1 ::: THEN openssl

    If specified, `m` will override PREFSSHKGEN and PREFOPENSSL global vars.
    The default (0) is equivalent to PREFSSHKGEN==1 (and, PREFOPENSSL==0).
    If `m` is not 1 or 0, then PREFSSHKGEN and PREFOPENSSL will be used.
    By default, both global vars are 0, and, fail-over to 0 (PREFSSHKGEN),
    which is the initial implementation (PREFOPENSSL is a ToDo item).
    '''
    if b < 512:
        raise Exception("Bits should be >= 512.")
    fil, method = None, None
    if b < 1024 or b > 4096:
        # ToDo: test!
        method = 1
    else:
        if m == 0:
            method = 0
        elif m == 1:
            method = 1
        elif (PREFSSHKGEN == PREFOPENSSL) and PREFSSHKGEN == 0:
            method = 0
        elif (PREFSSHKGEN != PREFOPENSSL) and PREFSSHKGEN == 1:
            method = 0
        elif (PREFSSHKGEN != PREFOPENSSL) and PREFOPENSSL == 1:
            method = 1
        else:
            raise Exception("Ambiguous key generation parameters.")
    fil = _make_rsa_key(bits=b, m=method, debug=dbg)
    assert not fil is None, "the initial key generation step cannot be None!"
    assert not method is None and method in [0, 1], "the method of initial key generation is required, 0 or 1!"
    fil2 = _openssl_read(filename=fil, m=method, debug=dbg)
    data = _external_parse(fil2, debug=dbg)
    _file_cleanup(fil, debug=dbg)
    return data

def save(data=None, bits=1024, method=0, nom=None, dbg=False):
    '''Saves generated data from the `gen` function.
    If not passed into the function, new data is generated.
    Naming allowed, as long as the name does not exist.

    Reads the existing file, replaces designated lines,
    adding subsequent replacement lines, and re-writes the whole file.

    '''
    if data is None:
        assert not bits is None, "bits cannot be None"
        assert isinstance(bits, int), "bits must be an Integer"
        #assert bits >= 1024 and bits <= 4096, "1024 <= bits <=4096" # old
        assert bits >= 512, "bits cannot be less than 512"
        if bits < 1024 or bits > 4096:
            method = 1
        else:
            assert method in [0, 1], "method must be 0 or 1"
        data = gen(b=bits, m=method, dbg=dbg)
    else:
        assert isinstance(data, dict)
        assert 'mod' in data, "missing data dictionary element, `mod`"
        bits_test = data['mod'].bit_length()
        if (bits_test < 1024 or bits_test > 4096) and method == 0:
            method = 1
    assert isinstance(data, dict), "data must be a dictionary of terms"
    assert all([True if x in data.keys() else False for x in ["mod", "f1", "f2"]]), "missing data dictionary element in `mod`, `f1`, `f2`"
    _fdata = None
    if os.path.exists(SAVEPATH0) and os.path.exists(SAVEPATH1):
        if method == 0:
            with open(SAVEPATH0, 'rb') as _fh:
                _fdata = _fh.read()
        elif method == 1:
            with open(SAVEPATH1, 'rb') as _fh:
                _fdata = _fh.read()
        else:
            raise Exception("Method must be 0 or 1!")
    else:
        raise NotImplementedError("ToDo!")
    assert not _fdata is None, "SAVEFILE data cannot be None!"
    _fdata = _fdata.decode('utf-8')
    assert isinstance(_fdata, str), "SAVEFILE data must be a string!"
    assert "\n\n"+NEXTLINE+"\n" in _fdata, "Could not find NEXTLINE in save file!!!"
    n, f1, f2 = data['mod'], data['f1'], data['f2']
    if not nom is None:
        assert isinstance(nom, str), "nom, or Name, must be a string!"
        assert len(nom) >= 3, "nom, or Name, must be a string of at least 3 characters!"
        if "_"+nom+"_" in _fdata or "_"+nom+"=" in _fdata:
            raise Exception("Name (nom) already exists in file. nom=`{}`".format(nom))
        else:
            bid = nom
    else:
        bid = tf.mktemp('', '', '')
    bid = bid.replace('_', '').replace('-', '').replace('#', '').replace(' ', '')
    while bid in _fdata:
        bid = tf.mktemp('', '', '')
        bid = bid.replace('_', '').replace('-', '').replace('#', '').replace(' ', '')
    if method == 0:
        bts = "SSHKGENRSA{}_{}".format(n.bit_length(), bid)
    elif method == 1:
        bts = "OPENSSLRSA{}_{}".format(n.bit_length(), bid)
    else:
        raise Exception("method must be an integer in [0, 1]")
    if f1 > f2: f1, f2 = f2, f1
    nxt = "\n\n"+ss80("{}=\"{}\"".format(bts, n))    # REQUIRED
    nxt += "\n"+ss80("{}_F1=\"{}\"".format(bts, f1)) # REQUIRED
    nxt += "\n"+ss80("{}_F2=\"{}\"".format(bts, f2)) # REQUIRED
    # Optional
    if "pubExp" in data.keys():
        nxt += "\n"+ss80("{}_PUB=\"{}\"".format(bts, data['pubExp']))
    if "privExp" in data.keys():
        nxt += "\n"+ss80("{}_PRI=\"{}\"".format(bts, data['privExp']))
    if "exp1" in data.keys():
        nxt += "\n"+ss80("{}_EXP1=\"{}\"".format(bts, data['exp1']))
    if "exp2" in data.keys():
        nxt += "\n"+ss80("{}_EXP2=\"{}\"".format(bts, data['exp2']))
    if "coeff" in data.keys():
        nxt += "\n"+ss80("{}_COEFF=\"{}\"".format(bts, data['coeff']))
    nxt += "\n\n{}\n".format(NEXTLINE)          # REQUIRED, IMPORTANT !!!
    _fdata = _fdata.replace("\n\n"+NEXTLINE+"\n", nxt)
    assert NEXTLINE in _fdata, "Bad nextline replacement!"
    assert NEXTLIST in _fdata, "Could not find NEXTLIST in save file!!!"
    c, d = gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)
    j, k = gmp.f_divmod(f1, d)
    nl = "{}(name='{}', n=gmpz({}), f1=gmpz({}), f2=gmpz({}), bits={}".format((" "*8)+"NInfo", bid, bts, bts+"_F1", bts+"_F2", int(n.bit_length()))
    nl += ", cMod2={}, dMod2={}, jMod2={}, kMod2={}, jVal={}),\n{}".format(int(c%2), int(d%2), int(j%2), int(k%2), int(j), NEXTLIST)
    _fdata = _fdata.replace(NEXTLIST, nl)
    assert NEXTLIST in _fdata, "Bad nextlist replacement!"
    try:
        if method == 0:
            #if dbg: print("Writing:\n{}\n==========\n{}\n==========".format(SAVEPATH0, _fdata))
            with open(SAVEPATH0, 'wb') as _fh:
                _fh.write(_fdata.encode('utf-8'))
        elif method == 1:
            with open(SAVEPATH1, 'wb') as _fh:
                _fh.write(_fdata.encode('utf-8'))
        else:
            raise Exception("Method must be 1 or 0!")
    except:
        raise

def mod2Err(varName=None):
    aestr = "must be an integer; it can be exactly 1 or 0"
    try:
        assert not varName is None
        assert isinstance(varName, str)
        return "{} {}".format(varName, aestr)
    except:
        return "UNK {}".format(aestr)

def gen2(b=1024, m=0, filters={}, limits=(None, 100000), dbg=False):
    '''Generates and returns an SSH-RSA key's info,
    of a given bit-length (variable `b`).

    Variable `m` is for "method" and it can be 1 or 0 (default=0).
        IIF 0 ::: THEN ssh-keygen
        IIF 1 ::: THEN openssl
    See the `gen` function for more info.

    The `limits` variable is used to limit the potential non-deterministic
    nature of this function. It accepts a tuple of exactly 2 elements:
        (seconds, counter)
    The default is (None, 100000).

    IIF the `filters` variable is an empty dictionary,
        then the `gen` function is called and returned.
        (Nothing to do here.)

    ELSE ...

    !!! WARN ! WARN ! WARN !!!
        THIS FUNCTION IS NON-DETERMINISTIC AND COULD TAKE SOME TIME.
    !!! WARN ! WARN ! WARN !!!
    ========================================
    Allows string filters with numeric values (dict):
        'name'    : str(alpha-num)  , # Must be an alpha-numeric string, not already used previously.
        'cMod2'   : int(0) || int(1),
        'dMod2'   : int(1) || int(0), # cMod2 and dMod2 must agree; cMod2 != dMod2
        'jMod2'   : int(0) || int(1),
        'kMod2'   : int(0) || int(1), # kMod2 and/or jMod2 must agree with dMod2
        'nMod4'   : int(1) || int(3), # must be an integer(1||3)
        'f1Mod4'  : int(1) || int(3), # must be an integer(1||3), and agree with nMod4 and/or f2Mod4 if either are specified.
        'f2Mod4'  : int(1) || int(3), # must be an integer(1||3), and agree with nMod4 and/or f1Mod4 if either are specified.
        'f1f2bdd' : int(0) || int(1), # must be an integer, 1 or 0, the bit-depth of difference between f1 and f2.
        'jVal'    : int(>0)         , # the j value must be a reasonably small integer 0 < j <= ? (non-deterministic)
                                      # the MAX j value is dicated by the bit-depth of `n` (modulus), where n=f1*f2,
                                      #      AND has some variability related to the magnitude of difference between f1 and f2 (!!!)
                                      #      SEE NOTES BELOW (!!!)
        #
        # TODO:
        #   EXTRANEOUS, not yet implemented ...
        #
        'cMod4'   : int([0,1,2,3])||, # must be an integer(0<=i<4) ; not implemented (ToDo)
        'dMod4'   : int([0,1,2,3])||, # must be an integer(0<=i<4) ; not implemented (ToDo)
        #
        # COMPLEX:
        #   EXTRANEOUS, not yet implemented (delayed implementation)...
        #
        'kModJ'   : int(?)          , # must be an integer(0<i<j)  ; not implemented (complex; delayed implementation)
        'cModJ'   : int(?)          , # must be an integer(0<i<j)  ; not implemented (complex; delayed implementation)
        'dModJ'   : int(?)          , # must be an integer(0<i<j)  ; not implemented (complex; delayed implementation)
        'nModJ'   : int(?)          , # must be an integer(0<i<j)  ; not implemented (complex; delayed implementation)
        'f1ModJ'  : int(?)          , # must be an integer(0<i<j)  ; not implemented (complex; delayed implementation)
        'f2ModJ'  : int(?)          , # must be an integer(0<i<j)  ; not implemented (complex; delayed implementation)

    The latter filters, `jVal`, and any other variable, mod j,
        are especially troublesome in terms of length of time to produce a result.
        Use with caution, and be prepared to wait. This will be updated if any
        optimizations are discovered such that specified requirements can be met beforehand.
    ========================================
    !!! WARN ! WARN ! WARN !!!
        THIS FUNCTION IS NON-DETERMINISTIC AND COULD TAKE SOME TIME.
    !!! WARN ! WARN ! WARN !!!

    '''
    if b < 512:
        raise Exception("Bits should be >= 512.")
    if b < 1024 or b > 4096:
        # ToDo: test!
        m = 1
    assert isinstance(filters, dict) or filters is None, "filters must be a dictionary of values, or None; see documentation"
    if len(filters.keys()) == 0:
        return gen(b, m, dbg)
    else:
        try:
            # ##########
            # IIF ANY of the implemented filters are available, proceed.
            # ##########
            assert any([True if x in filters.keys() else False for x in ['cMod2', 'dMod2', 'jMod2', 'kMod2', 'jVal']])
        except AssertionError as ae:
            # ##########
            # ELSE ... no filters == general or generic implementation.
            # ##########
            return gen(b, m, dbg)
        except:
            raise
        cMod2, dMod2, jMod2, kMod2, jVal, nMod4, f1Mod4, f2Mod4, f1f2bdd, nom = None, None, None, None, None, None, None, None, None, None
        if 'name' in filters.keys():
            assert isinstance(filters['name'], str), "if a name is specified, it must be a string"
            assert len(filters['name']) >= 3, "if a name is specified, it must be a string of at least 3 characters!"
            nom = filters['name']
        if 'cMod2' in filters.keys():
            assert filters['cMod2'] in [0, 1], mod2Err('cMod2')
            cMod2 = filters['cMod2']
        if 'dMod2' in filters.keys():
            assert filters['dMod2'] in [0, 1], mod2Err('dMod2')
            dMod2 = filters['dMod2']
            if not cMod2 is None:
                assert cMod2 != dMod2, "cMod2 cannot be equal to dMod2 !!!"
        # BEG: Fill-In-The-Blanks
        #   (one of these things is not like the other, one of these things is just not the same)
        if not cMod2 is None and dMod2 is None:
            dMod2 = 1 if cMod2 == 0 else 0
        elif cMod2 is None and not dMod2 is None:
            cMod2 == 1 if dMod2 == 0 else 0
        # END: Fill-In-The-Blanks
        if 'jMod2' in filters.keys():
            assert filters['jMod2'] in [0, 1], mod2Err('jMod2')
            jMod2 = filters['jMod2']
        if 'jVal' in filters.keys():
            assert isinstance(filters['jVal'], int), "jVal must be an Integer"
            assert filters['jVal'] > 0, "jVal <= 0 ; jVal must be an Integer, greater than 0"
            jVal = filters['jVal']
            if not jMod2 is None:
                assert jVal%2 == jMod2, "jVal=={} does not align with jMod2=={}".format(jVal, jMod2)
        # BEG: Fill-In-The-Blanks
        if jMod2 is None and not jVal is None:
            jMod2 = jVal%2
        # END: Fill-In-The-Blanks
        # kMod2 is moved below (after) jVal due to jMod2; logic agreement requirements.
        if 'kMod2' in filters.keys():
            assert filters['kMod2'] in [0, 1], mod2Err('kMod2')
            kMod2 = filters['kMod2']
            if not jMod2 is None and not dMod2 is None:
                assert kMod2 == (1 if ((jMod2 == 0) or (dMod2 == 0)) else 0), "kMod2 does not align with jMod2 and/or dMod2"
            elif not jMod2 is None or not dMod2 is None:
                if not jMod2 is None: assert kMod2 == 1 if jMod2 == 0 else 0, "kMod2 does not align with jMod2"
                elif not dMod2 is None: assert kMod2 == 1 if dMod2 == 0 else 0, "kMod2 does not align with dMod2"
                else: raise Exception("Bad Logic!!! (ID:1)")
            else:
                assert cMod2 is None, "dMod2 should be set if cMod2 is set, and kMod2 is specified as a filter."
        if 'nMod4' in filters:
            assert filters['nMod4'] in [1, 3], "nMod4 should be an integer, 1 or 3, if set"
            nMod4 = filters['nMod4']
        if 'f1Mod4' in filters:
            assert filters['f1Mod4'] in [1, 3], "f1Mod4 should be an integer, 1 or 3, if set"
            f1Mod4 = filters['f1Mod4']
        if 'f2Mod4' in filters:
            assert filters['f2Mod4'] in [1, 3], "f2Mod4 should be an integer, 1 or 3, if set"
            f2Mod4 = filters['f2Mod4']
        if not nMod4 is None:
            if not f1Mod4 is None and not f2Mod4 is None:
                assert (f1Mod4*f2Mod4)%4 == nMod4, "f1Mod4 and f2Mod4 do not align with nMod4"
            elif not f1Mod4 is None and f2Mod4 is None:
                if nMod4 == 1 and f1Mod4 == 1:
                    f2Mod4 = 1
                elif nMod4 == 1 and f1Mod4 == 3:
                    f2Mod4 = 3
                elif nMod4 == 3 and f1Mod4 == 1:
                    f2Mod4 = 3
                elif nMod4 == 3 and f1Mod4 == 3:
                    f2Mod4 = 1
                else:
                    raise Exception("Bad logic! -- Unhandled condition")
            elif f1Mod4 is None and not f2Mod4 is None:
                if nMod4 == 1 and f2Mod4 == 1:
                    f1Mod4 = 1
                elif nMod4 == 1 and f2Mod4 == 3:
                    f1Mod4 = 3
                elif nMod4 == 3 and f2Mod4 == 1:
                    f1Mod4 = 3
                elif nMod4 == 3 and f2Mod4 == 3:
                    f1Mod4 = 1
                else:
                    raise Exception("Bad logic! -- Unhandled condition")
            else:
                raise Exception("Bad logic! -- Unhandled condition")
        if (not f1Mod4 is None) and (not f2Mod4 is None) and (nMod4 is None):
            nMod4 = int((f1Mod4*f2Mod4)%4)
            assert nMod4 in [1, 3], "Bad logic! (f1Mod4*f2Mod4)%4 should be in [1, 3]"
        if 'f1f2bdd' in filters:
            assert filters['f1f2bdd'] in [0, 1], "f1f2bdd only accepts integer arguments 0 or 1"
            f1f2bdd = filters['f2f2bdd']
        # ##########
        # ToDo: Filters not yet implemented (IIF)
        # ##########
        fnyi = ["cMod4", "dMod4", "nModJ", "cModJ", "dModJ", "f1ModJ", "f2ModJ", "kModJ"]
        if any([True if x in filters.keys() else False for x in fnyi]):
            print("WARN: Filters not yet implemented:\n\t{}".format(fnyi))
        data = None
        counter = 0
        while data is None:
            if counter >= 100000:
                raise Exception("More than 100,000 iterations without result! (Stopped)")
            counter += 1
            data = gen(b, m, dbg)
            assert not data is None
            assert isinstance(data, dict)
            assert all([True if x in data.keys() else False for x in ['mod', 'f1', 'f2']])
            n, f1, f2 = data['mod'], data['f1'], data['f2']
            if f1 > f2:
                f1, f2 = f2, f1
            assert f1 < f2, "f1 !< f2"
            c, d = gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)
            if f1 < d:
                continue
            j, k = gmp.f_divmod(f1, d)
            if j <= 0:
                data = None
                continue # while
            if not cMod2 is None:
                if c%2 != cMod2:
                    data = None
                    continue # while
            if not dMod2 is None:
                if d%2 != dMod2:
                    data = None
                    continue # while
            if not jMod2 is None:
                if j%2 != jMod2:
                    data = None
                    continue # while
            if not kMod2 is None:
                if k%2 != kMod2:
                    data = None
                    continue # while
            if not nMod4 is None:
                if n%4 != nMod4:
                    data = None
                    continue # while
            if not f1Mod4 is None:
                if f1%4 != f1Mod4:
                    data = None
                    continue # while
            if not f2Mod4 is None:
                if f2%4 != f2Mod4:
                    data = None
                    continue # while
            if not f1f2bdd is None:
                if f1f2bdd != abs(f2.bit_depth()-f1.bit_depth()):
                    data = None
                    continue # while
            if not jVal is None:
                if j != jVal:
                    data = None
                    continue # while
        assert not data is None, "data is None, no return!"
        return data

def save2(data=None, method=0, bits=1024, nom=None, filters={}, limits=(None, 100000), dbg=False):
    '''Saves generated data from the `gen2` function.
    If not passed into the function, new data is generated.
    Naming allowed, as long as the name does not exist.

    See other function definitions in this file for descriptions of the various variables.

    '''
    if data is None:
        assert not bits is None, "bits cannot be None"
        assert isinstance(bits, int), "bits must be an Integer"
        assert bits >= 512, "bits must be >= 512"
        assert method in [0, 1], "method must be an integer, 0 or 1"
        #assert bits >= 1024 and bits <= 4096, "1024 <= bits <=4096"
        if not nom is None:
            if isinstance(nom, str):
                if not 'name' in filters:
                    filters['name'] = nom
        elif not nom is None:
            if isinstance(nom, str):
                if 'name' in filters:
                    assert filters['name'] == nom, "specified name, and filters name do not match!"
        data = gen2(b=bits, m=method, filters=filters, limits=limits, dbg=dbg)
    assert isinstance(data, dict), "returned data must be a dictionary"
    save(data, bits, method, nom, dbg)

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



