#!/usr/bin/env python3

# Python Library
from collections import namedtuple as nt

# 3rd party
import gmpy2 as gmp

# Local
from rsch.NInfo import NInfo2 as ni2, gen_NInfo2 as gni2

PolStar=nt('PolStar', ['PS', 'NE', 'SE', 'SW', 'NW', 'N', 'E', 'S', 'W'])

def gen_PolStar(n, f1, f2):
    '''Delivers a PolStar named tuple with 9 members.

    Each member is one of the major points on a compass:
        clockwise: (N, NE, E, SE, S, SW, W, NW)

    These surround a central (Pole) point: PS, consisting of the input variables.

    Each member is of type: NInfo2 (a named tuple)
    The NInfo2 named tuple contains the following:
        n, f1, f2, c, d, c%2, d%2, j, j%2, k%s
        Where: n == f1*f2 == c^2-d^2, AND (j, k) == gmp.f_divmod(f1, d)

    Further:
        {NE, SE, NW, SE}.{c|d}%2 != PS.{c|d}%2
        {N, E, S, W}.{c|d}%2 == PS.{c|d}%2
        This is to say:
            assert NE.c%2 != PS.c%2
            assert N.c%2 == PS.c%2
            assert SW.d%2 != PS.d%2
            assert S.d%2 == PS.d%2
    '''
    n, f1, f2 = gmp.mpz(n), gmp.mpz(f1), gmp.mpz(f2)
    assert n%2 == f1%2 == f2%2 == 1, "Input variables must be 1 mod 2."
    assert n >= f2 > f1 >= 1 and f1*f2 == n, "Input variables not aligned properly."
    if n == f2: assert f1 == 1, "If n==f2, then f1 must be 1; error!"
    c, d = gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)
    ps, ne, se, sw, nw, nn, ee, ss, ww = None, None, None, None, None, None, None, None, None
    ps = gni2(n, f1, f2)
    ne_f1, ne_f2 = (c+1)-(d+1), (c+1)+(d+1)
    if ((ne_f1 >= 1) and (ne_f2 >= ne_f1+2)):
        ne = gni2(ne_f1*ne_f2, ne_f1, ne_f2)
    se_f1, se_f2 = (c+1)-(d-1), (c+1)+(d-1)
    if ((se_f1 >= 1) and (se_f2 >= se_f1+2)):
        se = gni2(se_f1*se_f2, se_f1, se_f2)
    sw_f1, sw_f2 = (c-1)-(d-1), (c-1)+(d-1)
    if ((sw_f1 >= 1) and (sw_f2 >= sw_f1+2)):
        sw = gni2(sw_f1*sw_f2, sw_f1, sw_f2)
    nw_f1, nw_f2 = (c-1)-(d+1), (c-1)+(d+1)
    if ((nw_f1 >= 1) and (nw_f2 >= nw_f1+2)):
        nw = gni2(nw_f1*nw_f2, nw_f1, nw_f2)
    nn_f1, nn_f2 = f1-2, f2+2
    if (f1-2 >= 1):
        nn = gni2(nn_f1*nn_f2, nn_f1, nn_f2)
    ee_f1, ee_f2 = f1+2, f2+2
    ee = gni2(ee_f1*ee_f2, ee_f1, ee_f2) # Additive, no checks.
    ss_f1, ss_f2 = f1+2, f2-2
    if (f2-2 > f1+2):
        ss = gni2(ss_f1*ss_f2, ss_f1, ss_f2)
    ww_f1, ww_f2 = f1-2, f2-2
    if (f1-2 >= 1):
        ww = gni2(ww_f1*ww_f2, ww_f1, ww_f2)
    PS = PolStar(PS=ps, NE=ne, SE=se, SW=sw, NW=nw, N=nn, E=ee, S=ss, W=ww)
    fail_assert = "Sumpin' Smells Funny (BHG)"
    if not PS.NW is None:
        assert PS.PS.c%2 != PS.NW.c%2, fail_assert
    if not PS.SW is None:
        assert PS.PS.c%2 != PS.SW.c%2, fail_assert
    if not PS.W is None:
        assert PS.PS.c%2 == PS.W.c%2, fail_assert
    if not PS.N is None:
        assert PS.PS.d%2 == PS.N.d%2, fail_assert
    assert PS.PS.d%2 != PS.NE.d%2, fail_assert
    assert PS.PS.c%2 == PS.E.c%2, fail_assert
    if not PS.SE is None:
        assert PS.PS.d%2 != PS.SE.d%2, fail_assert
    if not PS.S is None:
        assert PS.PS.d%2 == PS.S.d%2, fail_assert
    return PS

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



