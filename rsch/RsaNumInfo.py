#!/usr/bin/env python3

'''
Contains a class breaking out RSA Number information.
'''

# Python Library
from collections import namedtuple as nt

# 3rd party
import gmpy2 as gmp

# Local
from rsch.NInfo import NInfo as ni, gen_NInfo as gni, gen_NInfo2 as gni2
from rsch.PolStar import PolStar as ps, gen_PolStar as gps
from rsch.BigNum_RSA import RSANUMS as rsa
from rsch.BigNum_RSA_OPENSSL import OPENSSLRSANUMS as osl
from rsch.BigNum_RSA_SSHKGEN import SSHKGENRSANUMS as shk

dkPair = nt('dkPair', ['d', 'k'])
ABMX = nt('ABMX', ['am', 'ax', 'bm', 'bx'])

def _list_names():
    print('RSA challenge')
    lst = rsa[::]
    lst.sort(key=lambda x: int(gmp.mpz(x[1]).bit_length()))
    for v in lst:
        print("\t{: >9} | {: >10} ({})".format(v[0], 'unfactored' if v[2] is None else 'factored', gmp.mpz(v[1]).bit_length()))
    print('OpenSSL generated')
    for v in osl:
        print("\t{: >9} ({})".format(v.name, v.n.bit_length()))
    print('ssh-keygen generated')
    for v in shk:
        print("\t{: >9} ({})".format(v.name, v.n.bit_length()))

names=lambda x: _list_names()

def _gen_abmx(j):
    '''Generates {a|b}_{min|max} values for a given j.
    Returns a namedtuple bearing these values.
    '''
    j = gmp.mpz(j)
    a_min, a_max, b_min, b_max = j**2+4*j+3, j**2+2*j, -(2*j+4), 2*j+2
    return ABMX(am=a_min, ax=a_max, bm=b_min, bx=b_max)

class _dkDct(dict):
    '''A customized dictionary for d-k pairs.
    D is the Difference in the Sum and Difference of Squares (Pythagorean Triples).
    K is a value less than D, such that (N-K^2)%D == 0
    If the J value that d and k are based on is >= 1 (or 0).
    '''

    @property
    def DkMinMax(self):
        '''Are both DminKmax and DmaxKmin both set?
        '''
        return self.DminKmax and self.DmaxKmin

    @property
    def DminKmax(self):
        '''Is DminKmax set?
        '''
        if (not self['dMin'] is None) and (not self['kMax'] is None):
            return True
        return False

    @property
    def DmaxKmin(self):
        '''Is DmaxKmin set?
        '''
        if (not self['dMax'] is None) and (not self['kMin'] is None):
            return True
        return False

    def __init__(self, *args, **kwargs):
        super()
        self['dMin'], self['dMax'] = None, None
        self['kMax'], self['kMin'] = None, None
        self['dkLst'] = []
        self.__setattr__('dkLst', self['dkLst'])

    def append(self, val):
        '''Append an item to the primary list held in the dictionary.
        '''
        assert self.DkMinMax, "Before appending to the dkDict, the min and max values must first be set."
        assert isinstance(val, dkPair), "value must be a dkPair instance"
        assert val.d > self['dMin'] and val.d < self['dMax'], "The `d` value in the dkPair must be dMin < d < dMax"
        assert val.k > self['kMin'] and val.k < self['kMax'], "The `k` value in the dkPair must be kMin < k < kMax"
        assert len([x for x in self['dkLst'] if x.d == val.d or x.k == val.k]) == 0, "Respective `d` or `k` values cannot already exist in the primary list!"
        self['dkLst'].append(val)

    def set_DminKmax(self, val, j):
        '''Sets the minimum `d` value, associated with a maximum `k` value.
        '''
        assert isinstance(val, dkPair), "value must be a dkPair instance"
        #assert val.d > val.k, "The `d` value in a dkPair must always be greater than a `k` value."
        if self.DmaxKmin:
            assert val.d < self['dMax'] and val.k > self['kMin'], "Respective values must logically coordinate with the maximum `d` value, and minimum `k` value."
        self['dMin'], self['kMax'] = val.d, val.k
        self.__setattr__('dMin', val.d)
        self.__setattr__('kMax', val.k)
        self['dkLst'].append(val)

    def set_DmaxKmin(self, val, j):
        '''Sets the maximum `d` value, associated with a minimum `k` value.
        '''
        assert isinstance(val, dkPair), "value must be a dkPair instance"
        #assert val.d > val.k, "The `d` value in a dkPair must always be greater than a `k` value."
        if self.DminKmax:
            assert val.d > self['dMin'] and val.k < self['kMax'], "Respective values must logically coordinate with the minimum `d` value, and maximum `k` value."
        self['dMax'], self['kMin'] = val.d, val.k
        self.__setattr__('dMax', val.d)
        self.__setattr__('kMin', val.k)
        self['dkLst'].append(val)

    def __lt__(self, val):
        '''Not implemented.'''
        raise NotImplementedError()
    def __gt__(self, val):
        '''Not implemented.'''
        raise NotImplementedError()
    def __eq__(self, val):
        '''Not implemented.'''
        raise NotImplementedError()
    def __ge__(self, val):
        '''Not implemented.'''
        raise NotImplementedError()
    def __le__(self, val):
        '''Not implemented.'''
        raise NotImplementedError()
    def __ne__(self, val):
        '''Not implemented.'''
        raise NotImplementedError()
    def clear(self):
        '''Not implemented.'''
        raise NotImplementedError()
    def pop(self):
        '''Not implemented.'''
        raise NotImplementedError()
    def popitem(self):
        '''Not implemented.'''
        raise NotImplementedError()

class _jDct(dict):
    '''A customized dictionary for j values.
    Each member is a single integer >= 1 (or 0), such that ...
        ... for j, there exists d-k pairs in a min-max range of d based on j.

    Usage:
        >>> var = _jDct()
        >>> var.set_Key(int)
        >>> var[1].append(dkPair(d, k))
        >>> # ERROR, Fails, because the following are not yet set.
        >>> var[1].set_DminKmax(dkPair(d, k))
        >>> var[1].set_DmaxKmin(dkPair(d, k))
        >>> var[1].append(dkPair(d, k))
        >>> # Succeeds if Dmin, Dmax, Kmin, and Kmax are set properly.
        >>> assert isinstance(var, dict) # True
        >>> assert isinstance(var[1], dict) # True

    In the above example:
        var is a dictionary of keys
        each key is a `j` value (set before assignment)
            each `j` value has a minimum and maximum `d` value (set before assignment)
                each `k` value must be less than the `d` value it is paired with
                `kMin` is associated with `dMax`
                `kMax` is associated with `dMin`
                From `dMin` to `dMax` `k` counts up
                From `dMax` to `dMin` `k` counts down
                From `kMin` to `kMax` `d` counts down
                From `kMax` to `kMin` `d` counts up
            intermediate `dkPair(d, k)` values may be set ...
            ... ONLY AFTER the respective minimum and a maximum ...
            ... for each `dkPair(d, k)` have been set (over the `j`-ranged field).
    '''

    def __init__(self, *args, **kwargs):
        super()
        self._j_abmx = {}
        self._n = None

    def __setitem__(self, key, val):
        set_key_error = "\nKey `{}` does not yet exist.\n".format(key)
        set_key_error += "\tIt needs to be initialized first!\n"
        set_key_error += "\tUse the proprietary functions to handle this!\n"
        set_key_error2 = "\n\t\t>>> var.set_Key(int(key))\n"
        print(set_key_error + set_key_error2)
        raise KeyError(key)

    def set_Key(self, key):
        assert isinstance(key, int) or isinstance(key, type(gmp.mpz(0))), "Key must be an integer"
        if isinstance(key, type(gmp.mpz(0))):
            key = int(key)
        if not key in self:
            super(_jDct, self).__setitem__(key, _dkDct())
            self._j_abmx[key] = _gen_abmx(key)
        else:
            raise KeyError("Key `{}` exists! It cannot be re-initialized.".format(key))

    def set_DminKmax(self, key, value):
        self[key].set_DminKmax(value, key)

    def set_DmaxKmin(self, key, value):
        self[key].set_DmaxKmin(value, key)

    def set_N(self, n):
        n = gmp.mpz(n)
        if self._n is None:
            assert n%2 == 1 and n >= 3, "Input `n` must be 1 mod 2 and >= 3"
            self._n = gmp.mpz(n)

    def set_KN(self, key, n):
        key, n = int(key), gmp.mpz(n)
        self.set_Key(key)
        if self._n is None:
            self.set_N(n)
        else:
            assert n == self._n, "Input `n` does not match for this jDct instance!"

    def __calc_DK_min_max__(self, j):
        assert not self._n is None, "N must be set to use this function."
        if not int(j) in self:
            self.set_Key(int(j))
        assert int(j) in self._j_abmx, "Key not set properly; j does not have an associated {a|b}_{min|max}."
        assert gmp.get_context().precision >= 8*self._n.bit_length(), "Inadequate precision."
        s = gmp.sqrt(self._n)
        c = gmp.mpz(gmp.floor(s))+1
        csq = c**2
        dsq = csq-self._n
        if (csq+dsq)%4 != 1:
            c += 1
            csq = c**2
            dsq = csq-self._n
        assert (csq+dsq)%4 == 1, "Could not align c^2+d^2 \equiv 1 mod 4."
        cMod2 = c%2
        dMod2 = gmp.mpz(0) if cMod2 == 1 else gmp.mpz(1)
        am, ax = self._j_abmx[int(j)].am, self._j_abmx[int(j)].ax
        bm, bx = self._j_abmx[int(j)].bm, self._j_abmx[int(j)].bx
        cc = (1-self._n)
        dm = gmp.mpz(gmp.ceil((-bm+gmp.sqrt(bm**2-4*am*cc))/(2*am)))
        if dm%2 != dMod2: dm += 1
        assert dm%2 == dMod2, "Could not align d_min to dMod2=={}".format(dMod2)
        dx = gmp.mpz(gmp.floor((-bx+gmp.sqrt(bx**2-4*ax*cc))/(2*ax)))
        if dx%2 != dMod2: dx -= 1
        assert dx%2 == dMod2, "Could not align d_max to dMod2=={}".format(dMod2)
        kx = gmp.mpz(gmp.ceil(-dm-j*dm+gmp.sqrt(self._n+dm**2)))
        if kx >= dm:
            dm += 2
            kx = gmp.mpz(gmp.ceil(-dm-j*dm+gmp.sqrt(self._n+dm**2)))
            if km >= dm:
                dm += 2
                kx = gmp.mpz(gmp.ceil(-dm-j*dm+gmp.sqrt(self._n+dm**2)))
                if kx >= dm:
                    raise Exception("k_min failed to align after 3 attempts")
        km = gmp.mpz(gmp.floor(-dx-j*dx+gmp.sqrt(self._n+dx**2)))
        if km <= 0:
            dx -= 2
            km = gmp.mpz(gmp.floor(-dx-j*dx+gmp.sqrt(self._n+dx**2)))
            if km <= 0:
                dx -= 2
                km = gmp.mpz(gmp.floor(-dx-j*dx+gmp.sqrt(self._n+dx**2)))
                if km <= 0:
                    raise Exception("k_max faile dto align after 3 attempts")
        # This process excludes calculating kMod2
        # but it should probably be included.
        self[int(j)].set_DminKmax(dkPair(dm, kx), j)
        self[int(j)].set_DmaxKmin(dkPair(dx, km), j)

class RsaNumInfo:
    '''Includes various equalities and equivalencies, if factored.
    Else, a similar base to work off of for unfactored numbers.
    '''

    @property
    def N(self):
        '''The RSA-number itself.
        '''
        return self._n

    @property
    def n(self):
        '''Alias for N
        '''
        return self.N

    @property
    def F1(self):
        '''The lower of 2 semi-prime factors of N.
        '''
        return self._f1

    @property
    def f1(self):
        '''Alias for F1
        '''
        return self.F1

    @property
    def F2(self):
        '''The greater of 2 semi-prime factors of N.
        '''
        return self._f2

    @property
    def f2(self):
        '''Alias for F2
        '''
        return self.F2

    @property
    def C(self):
        '''The centered value in the center and difference of squares.
        '''
        return self._c

    @property
    def c(self):
        '''Alias for C
        '''
        return self.C

    @property
    def D(self):
        '''The difference value in the center and difference of squares.
        '''
        return self._d

    @property
    def d(self):
        '''Alias for D
        '''
        return self.D

    @property
    def j(self):
        '''j in ...
            F1=(D(j+0)+k)
             C=(D(j+1)+k)
            F2=(D(j+2)+k)
        '''
        return self._j

    @property
    def jArr(self):
        '''Provides a dictionary-based array of potential j-value matches.

        WARN: BE CAUTIOUS WHEN USING THIS!!!

        '''
        if self._jArr is None:
            self._jArr = _jDct()
            if self.factored:
                self._jArr.set_KN(self.j, self.n)
                self._jArr.__calc_DK_min_max__(int(self.j))
                self._jArr[int(self.j)].append(dkPair(self.d, self.k))
            else:
                self._jArr.set_N(self.n)
                for _i in range(1, 436):
                    self._jArr.set_Key(int(_i))
                    self._jArr.__calc_DK_min_max__(int(_i))
        return self._jArr

    @property
    def k(self):
        '''k in ...
            F1=(D(j+0)+k)
             C=(D(j+1)+k)
            F2=(D(j+2)+k)
        '''
        return self._k

    @property
    def i(self):
        '''i == sqrt(nj^2+2nj+k^2)
        '''
        return self._i

    @property
    def Ej(self):
        '''Enumeration of multiplicative j-values.
        '''
        return self._Ej

    @property
    def Delta1(self):
        '''Alias for `d1`
        '''
        return self._d1

    @property
    def d1(self):
        '''Alias for `Delta1`
        '''
        return self._d1

    @property
    def Delta2(self):
        '''Alias for `d2`
        '''
        return self._d2

    @property
    def d2(self):
        '''Alias for `Delta2`
        '''
        return self._d2

    @property
    def Delta3(self):
        '''Alias for `d3`
        '''
        return self._d3

    @property
    def d3(self):
        '''Alias for `Delta3`
        '''
        return self._d3

    @property
    def Q1(self):
        '''Quotient 1 in fib-box cross-product (R1).
        '''
        return self._Q1

    @property
    def R1(self):
        '''Remainder 1 in fib-box cross-product (Q1).
        '''
        return self._R1

    @property
    def Q2(self):
        '''Quotient 2 in fib-box cross-product (R2).
        '''
        return self._Q2

    @property
    def R2(self):
        '''Remainder 2 in fib-box cross-product (Q2).
        '''
        return self._R2

    @property
    def Q0(self):
        '''Sum of Quotion 1 and Quotient 2 in fib-box cross-product.
        '''
        return self._Q0

    @property
    def Q3(self):
        '''Difference (abs, pos) of Quotion 1 and Quotient 2 in fib-box cross-product.
        '''
        return self._Q3

    @property
    def cMod2(self):
        '''The centered value, moduluo 2.
        '''
        return self._cm2

    @property
    def dMod2(self):
        '''The difference value, moduluo 2.
        '''
        return self._dm2

    @property
    def cModj(self):
        '''The centered value, moduluo j.
        (If factored.)
        '''
        return self._cmj

    @property
    def cMin(self):
        '''The minimum centered value.
        '''
        return self._cmin

    @property
    def dMin(self):
        '''The minimum difference value.
        '''
        return self._dmin

    @property
    def dModj(self):
        '''The difference value, moduluo j.
        (If factored.)
        '''
        return self._dmj

    @property
    def f1Modj(self):
        '''The lesser factor, moduluo j.
        (If factored.)
        '''
        return self._f1mj

    @property
    def f2Modj(self):
        '''The greater factor, moduluo j.
        (If factored.)
        '''
        return self._f2mj

    @property
    def factored(self):
        '''Is the number factored yet?
        '''
        return self._factored

    @property
    def name(self):
        '''Returns a string of RSA-str(num)
        '''
        return self._name

    @property
    def nInfo(self):
        '''Returns the NInfo input representation.
        '''
        if self._nInfo is None:
            if self.factored:
                self._nInfo = gNInfo2(self.n, self.f1, self.f2)
                try:
                    assert self._nInfo.c == self._c and self._nInfo.d == self._d, "C or D does not match!"
                    assert self._nInfo.cMod2 == self._cm2 and self._nInfo.dMod2 == self._dm2, "C or D (mod 2) does not match!"
                    assert self._nInfo.jMod2 == self._jm2 and self._nInfo.kMod2 == self._km2, "j or k (mod 2) does not match!"
                    assert self._nInfo.jVal == self._j, "j-Value does not match!"
                except AssertionError as ae:
                    print("\n\tFAILURE: Generating NInfo from RsaNumInfo does not match input by name!\n")
                    raise ae
            else:
                #NInfo=nt('NInfo', ['name', 'n', 'f1', 'f2', 'bits', 'cMod2', 'dMod2', 'jMod2', 'kMod2', 'jVal'])
                self._nInfo = ni(name=self._name, n=self._n, f1=None, f2=None, bits=int(self._n.bit_length()), cMod2=self._cm2, dMod2=self._dm2, jMod2=None, kMod2=None, jVal=None)
        return self._nInfo

    def __init__(self, nom=None, nInfo=None):
        one_not_like_the_other = "One of `nom` and `nInfo` cannot be None; one must be, the other not."
        assert (not nom is None and nInfo is None) or (nom is None and not nInfo is None), one_not_like_the_other
        if not nInfo is None: assert isinstance(nInfo, type(gni(3, 1, 3))) or isinstance(nInfo, type(gni2(3, 1, 3))), "nInfo must be an instance of NInfo!"
        self._setupDone = False
        self._factored = None
        self._name, self._n, self._nInfo = None, None, None
        self._f1, self._f2 = None, None
        self._c, self._d = None, None
        self._j, self._k, self._i = None, None, None
        self._jArr = None
        self._cm2, self._dm2 = None, None
        self._cmj, self._dmj = None, None
        self._f1mj, self._f2mj = None, None
        self._cmin, self._dmin = None, None
        self._d1, self._d2, self._d2 = None, None, None
        self._Q1, self._R1 = None, None
        self._Q2, self._R2 = None, None
        self._Q0, self._Q3 = None, None
        self._Ej = None
        if not nom is None and nInfo is None:
            if str(nom) in [x[0] for x in rsa]:
                x = [x for x in rsa if x[0] == str(nom)][0]
                y = [y for y in osl if y.name == str(nom)]
                z = [z for z in shk if z.name == str(nom)]
                if len(y) > 0 or len(z) > 0:
                    raise Exception("Name `{}` exists in 2 locations; use nInfo to disambiguate.".format(nom))
                self._name = "RSA-{}".format(x[0])
                self._n = gmp.mpz(x[1])
                if not x[2] is None:
                    self._factored = True
                    self._f1, self._f2 = gmp.mpz(x[2]), gmp.mpz(x[3])
                    self._setup_factored(nInfo=False)
                else:
                    self._factored = False
                    self._setup_unfactored(nInfo=False)
            elif str(nom) in [x.name for x in osl]:
                x = [x for x in osl if x.name == str(nom)][0]
                y = [y for y in shk if y.name == str(nom)]
                if len(y) > 0:
                    raise Exception("Name `{}` exists in 2 locations; use nInfo to disambiguate.".format(nom))
                assert isinstance(x, NInfo)
                self._factored = True
                self._nInfo = x
                self._name = x.name
                self._n = x.n
                self._f1, self._f2 = x.f1, x.f2
                self._setup_factored(nInfo=True)
            elif str(nom) in [x.name for x in shk]:
                x = [x for x in shk if x.name == str(nom)][0]
                assert isinstance(x, NInfo)
                self._factored = True
                self._nInfo = x
                self._name = x.name
                self._n = x.n
                self._f1, self._f2 = x.f1, x.f2
                self._setup_factored(nInfo=True)
            else:
                raise Exception("Unrecognized name!")
        elif nom is None and not nInfo is None:
            self._nInfo = nInfo
            self._factored = (True if (not nInfo.f1 is None) else False)
            self._name = nInfo.name
            self._n = nInfo.n
            self._f1, self._f2 = nInfo.f1, nInfo.f2
            self._setup_factored(nInfo=True)
        else:
            raise Exception(one_not_like_the_other)
        assert self._setupDone, "Setup did not complete successfully!"

    def _setup_calc_cdmin(self):
        self._cmin = gmp.iroot_rem(self._n, 2)[0]+1
        csq = self._cmin**2
        dsq = csq-self._n
        if (csq+dsq)%4 != 1:
            self._cmin += 1
            csq = self._cmin**2
            dsq = csq-self._n
        assert (csq+dsq)%4 == 1, "failed to set c^2 in a^2+b^2==c^2 to 1 mod 4"
        cm2 = self._cmin%2
        dm2 = gmp.mpz(1) if cm2 == 0 else gmp.mpz(0)
        assert cm2 != dm2
        self._dmin = gmp.iroot_rem(self._cmin**2-self._n, 2)[0]
        if self._dmin%2 != dm2:
            self._dmin += 1
        assert self._cmin%2 != self._dmin%2

    def _setup_unfactored(self, nInfo=None):
        assert self._setupDone == False, "setup is only run once upon init"
        assert not nInfo is None and isinstance(nInfo, bool)
        self._f1, self._f2 = None, None
        self._c, self._d = None, None
        self._j, self._k, self._i = None, None, None
        self._setup_calc_cdmin()
        self._cm2 = self._cmin%2
        self._dm2 = gmp.mpz(1) if self._cm2 == 0 else gmp.mpz(0)
        assert self._cm2 != self._dm2
        self._cmj, self._dmj, self._f1mj, self._f2mj = None, None, None, None
        self._setupDone = True

    def _setup_factored(self, nInfo=None):
        assert self._setupDone == False, "setup is only run once upon init"
        assert not nInfo is None and isinstance(nInfo, bool)
        assert not ((self._f1 is None) or (self._f2 is None)), "Factors must exist to setup a factored number (?)"
        if self._f1 > self._f2:
            raise Exception("Factors are out-of-order. F1 should be less than F2.")
        self._c = gmp.f_div(self._f1+self._f2, 2)
        self._d = gmp.f_div(self._f2-self._f1, 2)
        self._setup_calc_cdmin()
        self._cm2, self._dm2 = self._c%2, self._d%2
        self._j, self._k = gmp.f_divmod(self._f1, self._d)
        #print("F1={}\n D={}\n C={}\nF2={}".format(self._f1, self._d, self._c, self._f2))
        if self._j >= 0:
            if self._j%2 == 0:
                ja, jb = gmp.f_div(self._j, 2), gmp.f_div(self._j+2, 2)
                self._d1 = ja+jb
                self._d2 = ja*jb
                self._d3 = ja*jb
                self._Ej = ja*jb
                #assert self._d2 == self._d3, "If jMod2==0, then d2 == d3 (failed)."
            else:
                ja = gmp.mpz(self._j)
                self._d1, self._d2, self._d3 = 1, 1, 1
                while ja%2 == 1 and ja > 1: # Could be 0?
                    self._d1 *= ja*(ja+2)
                    self._d2 *= ja**2
                    self._d3 *= (ja+2)**2
                    ja = gmp.f_div(ja-1, 2)
                if ja == 1: # Could be 0?
                    try:
                        td1 = self._d1*3
                        td2 = self._d2*(3**2)
                        td3 = self._d3*(1**2)
                        td1 = td1*(2+3)
                        td2 = td2*(2*3)
                        td3 = td3*(2*3)
                        tQ1, tR1 = gmp.f_divmod(self._n*td1-td2*self._f1**2+td3*self._f2**2, self._n)
                        tQ2, tR2 = gmp.f_divmod(self._n*td1+td2*self._f1**2-td3*self._f2**2, self._n)
                        assert tR1+tR2 == self._n, "R1+R2 != N"
                        assert tQ1 > 0 and tQ2 > 0, "One or both of Q1, Q2 not > 0."
                        self._d1, self._d2, self._d3 = td1, td2, td3
                        self._Ej = gmp.f_div(self._d1, 5)*(2*3)
                    except AssertionError as ae:
                        try:
                            td1 = self._d1*3
                            td2 = self._d2*(3**2)
                            td3 = self._d3*(1**2)
                            td1 = td1*(5*7)
                            td2 = td2*(5**2)
                            td3 = td3*(7**2)
                            td1 = td1*(1+2)
                            td2 = td2*(1*2)
                            td3 = td3*(1*2)
                            tQ1, tR1 = gmp.f_divmod(self._n*td1-td2*self._f1**2+td3*self._f2**2, self._n)
                            tQ2, tR2 = gmp.f_divmod(self._n*td1+td2*self._f1**2-td3*self._f2**2, self._n)
                            assert tR1+tR2 == self._n, "R1+R2 != N"
                            assert tQ1 > 0 and tQ2 > 0, "One or both of Q1, Q2 not > 0."
                            self._d1, self._d2, self._d3 = td1, td2, td3
                            self._Ej = gmp.f_div(self._d1, 3)*(2)
                        except AssertionError as ae2:
                            raise Exception("ToDo: Handle jMod2==1 | j={} -> j==1".format(self._j))
                else:
                    assert ja%2 == 0, "Bad j reduction!"
                    ja = gmp.f_div(ja, 2)
                    jb = ja+1
                    self._Ej = self._d1*(ja*jb)
                    self._d1 *= (ja+jb)
                    self._d2 *= (ja*jb)
                    self._d3 *= (ja*jb)
        self._Q1, self._R1 = gmp.f_divmod(self._n*self._d1-self._d2*self._f1**2+self._d3*self._f2**2, self._n)
        self._Q2, self._R2 = gmp.f_divmod(self._n*self._d1+self._d2*self._f1**2-self._d3*self._f2**2, self._n)
        self._Q0 = self._Q1+self._Q2
        self._Q3 = abs(self._Q1-self._Q2)
        if self._j%2 == 1:
            assert self._Q1 > 0 and self._Q2 > 0 and self._Q0 > self._Q3, "One or both of Q1, Q2 not > 0."
        else:
            assert 0 in [self._Q1, self._Q2] and (self._j*2+1) in [self._Q1, self._Q2], "One or both of Q1, Q2 not in [0, 2j+1]."
            assert self._Q0 == self._Q3, "if j%2 is even, Q0==Q3 (failure)."
        assert self._R1+self._R2 == self._n, "R1+R2 != N"
        self._i = gmp.iroot_rem(self._n*self._j**2+2*self._n*self._j+self._k**2, 2)
        assert self._i[1] == 0
        self._i = self._i[0]
        if self._j > 0:
            self._cmj, self._dmj = self._c%self._j, self._d%self._j
            self._f1mj, self._f2mj = self._f1%self._j, self._f2%self._j
        else:
            self._cmj, self._dmj = None, None
            self._f1mj, self._f2mj = None, None
        self._cmin = gmp.iroot_rem(self._n, 2)[0]+1
        if self._cmin%2 != self._c%2:
            self._cmin += 1
        self._dmin = gmp.iroot_rem(self._cmin**2-self._n, 2)[0]
        if self._dmin%2 != self._d%2:
            self._dmin += 1
        self._setupDone = True

    @staticmethod
    def get(num):
        return RsaNumInfo(num)

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



