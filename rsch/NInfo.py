#!/usr/bin/env python3

'''
Contains a named tuple, NInfo, to hold information about semi-prime numbers.
'''

# Python Library
from collections import namedtuple as nt

# 3rd party
import gmpy2 as gmp

NInfo=nt('NInfo', ['name', 'n', 'f1', 'f2', 'bits', 'cMod2', 'dMod2', 'jMod2', 'kMod2', 'jVal'])
NInfo2=nt('NInfo', ['name', 'n', 'f1', 'f2', 'c', 'd', 'bits', 'cMod2', 'dMod2', 'jMod2', 'kMod2', 'jVal'])

def gen_NInfo(n, f1, f2):
    '''Returns an NInfo instance based on n and 2 factors.
    '''
    assert not n is None and not f1 is None and not f2 is None
    assert isinstance(n, type(gmp.mpz(0))) or isinstance(n, int), "Input `n` must be an integer or mpz."
    assert isinstance(f1, type(gmp.mpz(0))) or isinstance(f1, int), "Input `f1` must be an integer or mpz."
    assert isinstance(f2, type(gmp.mpz(0))) or isinstance(f2, int), "Input `f2` must be an integer or mpz."
    n, f1, f2 = gmp.mpz(n), gmp.mpz(f1), gmp.mpz(f2)
    assert n%2 == f2%2 == f2%2 == 1, "Inputs `n` `f1` and `f2` must be odd!"
    assert n >= f2 > f1 >= 1 and f2 != f1, "Inputs `n` `f1` and `f2` must be greater than 1, and f1 != f2"
    if n == f2: assert f1 == 1, "If n==f2, then f1 must be 1; error!"
    assert n == f1*f2, "Input `n` != f1*f2"
    c, d = gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)
    jVal, kVal = gmp.f_divmod(f1, d)
    hName = hex(hash((int(n), int(f1), int(f2), int(n.bit_length()), int(c%2), int(d%2), int(jVal%2), int(kVal%2), int(jVal))))
    return NInfo(name="TestVar-{}".format(hName), n=n, f1=f1, f2=f2, bits=n.bit_length(), cMod2=c%2, dMod2=d%2, jMod2=jVal%2, kMod2=kVal%2, jVal=jVal)

def gen_NInfo2(n, f1, f2):
    '''Returns an NInfo instance based on n and 2 factors.
    '''
    assert not n is None and not f1 is None and not f2 is None
    assert isinstance(n, type(gmp.mpz(0))) or isinstance(n, int), "Input `n` must be an integer or mpz."
    assert isinstance(f1, type(gmp.mpz(0))) or isinstance(f1, int), "Input `f1` must be an integer or mpz."
    assert isinstance(f2, type(gmp.mpz(0))) or isinstance(f2, int), "Input `f2` must be an integer or mpz."
    n, f1, f2 = gmp.mpz(n), gmp.mpz(f1), gmp.mpz(f2)
    assert n%2 == f2%2 == f2%2 == 1, "Inputs `n` `f1` and `f2` must be odd!"
    assert n >= f2 > f1 >= 1 and f2 != f1, "Inputs `n` `f1` and `f2` must be greater than 1, and f1 != f2"
    if n == f2: assert f1 == 1, "If n==f2, then f1 must be 1; error!"
    assert n == f1*f2, "Input `n` != f1*f2"
    cc, dd = gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)
    jVal, kVal = gmp.f_divmod(f1, dd)
    hName = hex(hash((int(n), int(f1), int(f2), int(n.bit_length()), int(cc%2), int(dd%2), int(jVal%2), int(kVal%2), int(jVal))))
    return NInfo2(name="TestVar-{}".format(hName), n=n, f1=f1, f2=f2, c=cc, d=dd, bits=n.bit_length(), cMod2=cc%2, dMod2=dd%2, jMod2=jVal%2, kMod2=kVal%2, jVal=jVal)

def gen_NInfo_v2(n, f1, f2):
    '''Returns an NInfo instance based on n and center/difference of factors.
    TODO: Not yet implemented.
    '''
    raise NotImplementedError()

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



