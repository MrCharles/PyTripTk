
Sum of Squares
==============

There is a [Sage](https://www.sagemath.org/) _(mathematics package)_ algorithm to parse the sum of squares for numbers `1 mod 4` give by [William Stein](https://wstein.org/), located [here](https://wstein.org/edu/2007/spring/ent/ent-html/node75.html)...

* https://wstein.org/edu/2007/spring/ent/ent-html/node75.html

__See Also ...__

* [Congruum](https://en.wikipedia.org/wiki/Congruum)
* [Sum of two squares theorem](https://en.wikipedia.org/wiki/Sum_of_two_squares_theorem)
* [Brahmagupta&mdash;Fibonacci identity](https://en.wikipedia.org/wiki/Brahmagupta%E2%80%93Fibonacci_identity)

Usage
=====

Most of the PyTripTk software is developed in a Python 3 virtual env. However, Sage has not yet come up-to-par with Python 3.

This script should be run in a Python 2 environment.

```bash
$~/dev/PyTripTk/> python --version
Python 3.5.x
$~/dev/PyTripTk/> echo "If you're in a virtual env, deactivate it, ..."
$~/dev/PyTripTk/> echo "...or open a new console and set Python 2 as the default."
$~/dev/PyTripTk/> deactivate
$~/dev/PyTripTk/> python --version
Python 2.7.x
$~/dev/PyTripTk/> echo "Okay! Good-to-go. Yay!"
```

After ensuring you're in a Python 2 environment, proceed with **Usage** ...

```bash
$~/dev/PyTripTk/> sage rsch/SOS_WStein5m.py ##########
(#####, #####)
$~/dev/PyTripTk/> cd rsch
$~/dev/PyTripTk/> sage SOS_WStein5m.py ##########
(#####, #####)
```

The above example is passing a _(prime)_ number in via command line. Output is to the command line.

**OR**

```bash
$~/dev/PyTripTk/> sage rsch/SOS_WStein5m.py /tmp/file_name
$~/dev/PyTripTk/> cat /tmp/file_name.out
(#####, #####)
$~/dev/PyTripTk/> cd rsch
$~/dev/PyTripTk/> sage SOS_WStein5m.py /tmp/file_name
$~/dev/PyTripTk/> cat /tmp/file_name.out
(#####, #####)
```

Simply put, you can write a file to the `/tmp/file_name` directory and then have the script read that file, in which case it will output another file in the same location, appended with `/tmp/file_name.out`

Notes
=====

The `5m` at the end of the file name is simply the `@fork` statement which prefixes the actual function: Limited to 5 minutes.

