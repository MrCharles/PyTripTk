Usage Notes
===========

-----

Patched the [PyTripTk __init__.py](../../PyTripTk/__init__.py) file with some gmp extensions. Notably, `setprec` and `mpq2`, which are in the [util directory](../../PyTripTk/util).

**ToDo:** Forgetting something which needs to be added to the init file.

-----

For example, using `rsch.XYCoord` and/or `rsch.RsaNumInfo`, typical imports are as follow:

```python
>>> import PyTripTk as pttk
>>> from PyTripTk import PyTrip as pt, PyTripExt as ext
>>> from rsch.RsaNumInfo import RsaNumInfo as rni, gen_NInfo as gNInfo
>>> from rsch.XYCoord import XY as xy, XYCoord as xyc
>>> from rsch.XYCoord PointSlopeIntercept as psi, SlopeIntercept si
>>> from rsch.PolStar import PolStar as ps, gen_PolStar as gps
>>> var = rni(100)
>>> tst = xyc(var.n, var.f1, var.f2)
>>> psi1 = psi(tst.Pt1, tst.Pt38)
>>> psi2 = psi(tst.Pt3, tst.Pt18)
>>> psi3 = psi(tst.Pt8, tst.Pt13)
>>> si1 = si(tst.Pt13, psi3.PtSlope)
>>> ps1 = gps(var.n, var.f1, var.f2)
>>> _
```

The above observations are used to test various RSA numbers in generating the files in [SqFlatCurviLGrid](https://gitlab.com/MrCharles/PyTripTk/tree/master/rsch/jupyter/SqFlatCurviLGrid), such as [this v3 png](https://gitlab.com/MrCharles/PyTripTk/blob/master/rsch/jupyter/SqFlatCurviLGrid/v3/SqFlatCurviLGrid_v3.png).

-----

A Blurb on [this commit](https://gitlab.com/MrCharles/PyTripTk/commit/7fd9ff2edd6a37ad2531701fc95bce5806c005ad):

```python
>>> import PyTripTk as pttk
>>> from rsch.RsaNumInfo import RsaNumInfo as rni
>>> var = rni(230) # RSA-230 is, as of yet, unfactored
>>> gmp.setprec(var.n)
GMP precision set to 6096 # 8*n.bit_length()
>>> jKeys = var.jArr.keys()
>>> len(jKeys), min(jKeys), max(jKeys)
(435, 1, 435) # auto-generated upon calling jArr the first time.
>>> for x in jKeys:
....    y = var.jArr[x]
....    print("\tKey: {}".format(x))
....    print("\t\tdMin: {}".format(y.dMin))
....    print("\t\tkMax: {}".format(y.kMax))
....    print("\t\tdMax: {}".format(y.dMax))
....    print("\t\tkMin: {}".format(y.kMin))
# Prints 5-lines x 435-entries
>>> _
```

**ToDo:** The precision needs to be set when setting `var` &mdash; should not have to set it separately.

Ideally, you never need to set any other value for `j`. It takes a _**VERY LONG TIME**_ to find values around or greater than `100` when auto-generating `ssh key pairs` via `OpenSSL` or `ssh-keygen`. Though it is likely possible you can do this manually with much more efficiency _(independent of programs known to generate reasonably secure key-pairs)_.

Finding `(n-k^2)%d == 0`, for a given `j`-range is still computationally complex. _(This is research code, it is not meant to solve any problems; only provide **insight** into things like jump discontinuity, etc.)_

Documentation on the above mentioned commit &mdash; the `j` and `k` _(and `i`)_ variables can be found in a _(**WIP**, work-in-progress, **draft**)_ PDF in the main docs directory: [FibBox_jki_modulo.pdf](../../docs/FibBox_jki_modulo.pdf)

-----
