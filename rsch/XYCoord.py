#!/usr/bin/env python3

'''

'''

# Python lib
from collections import namedtuple as nt

#3rd Party
import gmpy2 as gmp

XY=nt('XY', ['x', 'y'])

class SlopeIntercept(object):
    '''Determines the intercept of two Point-Slope formulas (if not parallel).

    Accepts:
        - 2 `PtSlope` (class) instances.

    Provides (calculates):
        - `XYIntercept`
            The point where the two slopes intersect.

    Alternatively, use the (class) function `PointSlopeIntercept(point, slope)`
    '''

    @property
    def PtS1(self):
        return self._pts1

    @property
    def PtS2(self):
        return self._pts2

    @property
    def xyi(self):
        return self._xyi

    @property
    def XYIntercept(self):
        return self._xyi

    def __init__(self, pts1, pts2):
        assert isinstance(pts1, PtSlope), "Point 1 must be of type PtSlope"
        assert isinstance(pts2, PtSlope), "Point 2 must be of type PtSlope"
        assert pts1.m != pts2.m, "Slope cannot be equal! Parallel lines will not intersect."
        self._pts1, self._pts2 = pts1, pts2
        self._xyi = None
        # v2
        nb, dm = self._pts1.b-self._pts2.b, self._pts2.m-self._pts1.m
        xval = gmp.mpq2(gmp.mpq(nb._mpq if isinstance(nb, type(gmp.mpq2(gmp.mpq()))) else nb, dm._mpq if isinstance(dm, type(gmp.mpq2(gmp.mpq()))) else dm))
        yval = self._pts1.m*xval+self._pts1.b
        assert yval == self._pts2.m*xval+self._pts2.b, "Bad Slope-Slope Intercept equation!!!"
        # v1
        #xtmp = pts1.m-pts2.m
        #btmp = pts2.b-pts1.b
        #xval = btmp*gmp.mpq2(gmp.mpq(xtmp.d, xtmp.n))
        #try:
        #    assert pts1.m*xval+pts1.b == pts2.m*xval+pts2.b, "Bad Point-Slope Intercept equation!!!"
        #except AssertionError as ae:
        #    eq1, eq2 = pt1.m*xval+pt1.b, pt2.m*xval+pt2.b
        #    print(type(eq1), eq1.n, eq1.d)
        #    print(type(eq2), eq2.n, eq2.d)
        #    raise ae
        #yval = pts1.m*xval+pts1.b
        self._xyi = XY(xval, yval)

class PointSlopeIntercept(object):
    '''Determines the perpendicular intercept from a point to a line.

    Accepts:
        - A Point (XY)
        - A `PtSlope` (class)

    Provides:
        - `PtSlope` output
            Input.imp == Output.PtSlope.m
        - XY Intercept
            The intercept point. Should match the output and input slopes.

    Alternatively, use the (class) function `SlopeIntercept(slope, slope)`
    '''

    @property
    def Point(self):
        '''Returns the input point.
        '''
        return self._p

    @property
    def Slope(self):
        '''Returns the input slope.
        '''
        return self._s

    @property
    def PtS(self):
        '''Returns the calculated point-slope formula, as an instance of PtSlope.
        Alias for `self.PtSlope`
        '''
        return self._pts

    @property
    def PtSlope(self):
        '''Returns the calculated point-slope formula, as an instance of PtSlope.
        Full name, aliased as `self.PtS`
        '''
        return self._pts

    @property
    def Intercept(self):
        '''Where the input Point intercepts the input Slope.
        '''
        return self._i

    def __init__(self, point, slope):
        assert isinstance(point, XY), "The Point input must be of type XY, a set of two points."
        assert isinstance(slope, PtSlope), "The Slope input must be of type PtSlope, the point-slope equation."
        self._p = point
        self._s = slope
        assert not self.__point_on_line(self._p.x, self._p.y, self._s.m, self._s.b), "The point cannot reside on the line. The line already intercepts the point!"
        m = self._s.ipm
        b = self._p.y-(m*self._p.x)
        xval = gmp.mpq2(gmp.mpq((b-self._s.b)._mpq, (self._s.m-m)._mpq))
        yval = m*xval+b
        assert yval == xval*m+b, "Calculated point not on calculated line!"
        assert self._p.y == self._p.x*m+b, "Input point not on calculated line!"
        assert yval == xval*self._s.m+self._s.b, "Calculated point not on input line!"
        self._i = XY(xval if not isinstance(xval, type(gmp.mpq2(gmp.mpq()))) else xval._mpq, yval if not isinstance(yval, type(gmp.mpq2(gmp.mpq()))) else yval._mpq)
        self._pts = PtSlope(self._p, self._i)

    def __point_on_line(self, x, y, m, b):
        if y == m*x+b:
            return True
        return False

class PtSlope(object):
    '''A point-slope object.
    Accepts 2 points (and/or a precision marker, `nbl2`).
    Provides the following:
        - Input Points
        - m (slope)
        - b (y-intercept)
        - l (length of the line between the two points)
        - ipm (the inverse, perpendicular m-slope)

    ToDo:
        - A list of intercepts, and the function to calculate.
    '''

    @property
    def Pt1(self):
        '''One of two points on a line forming the Point-Slope equation, y=mx+b
        '''
        return self._pt1

    @property
    def Pt2(self):
        '''One of two points on a line forming the Point-Slope equation, y=mx+b
        '''
        return self._pt2

    @property
    def m(self):
        '''The slope in y=mx+b
        '''
        return self._m

    @property
    def ipm(self):
        '''The inverted, perpendicular slope
        '''
        return self._ipm

    @property
    def b(self):
        '''The y-intercept (if x==0) in y=mx+b
        '''
        return self._b

    @property
    def l(self):
        '''The length of the segment between the two points.
        '''
        return self._l

    def __init__(self, pt1=None, pt2=None, nbl2=None):
        assert not pt1 is None and not pt2 is None
        assert isinstance(pt1, XY), "Point 1 must be of type(XY): {}".format(type(XY))
        assert isinstance(pt2, XY), "Point 2 must be of type(XY): {}".format(type(XY))
        self._pt1, self._pt2 = pt1, pt2
        self._m = gmp.mpq2(gmp.mpq(self.Pt2.y-self.Pt1.y, self.Pt2.x-self.Pt1.x))
        self._ipm = gmp.mpq2(-gmp.mpq(self.m.d, self.m.n))
        self._b = self.Pt1.y-(self.m*self.Pt1.x)
        if nbl2 is None:
            # Assumes that precision is already 8*bit_length,
            # Reduces precision to 2*bit_length (division by 4)
            nbl2 = gmp.f_div(gmp.get_context().precision, 4)
        assert gmp.mpfr(self.b.f, nbl2) == gmp.mpfr((self.Pt2.y-(self.m*self.Pt2.x)).f, nbl2)
        self._l = gmp.sqrt((abs(self.Pt2.x)-abs(self.Pt1.x))**2+(abs(self.Pt2.y)-abs(self.Pt1.y))**2)

    def intercept(self, pt):
        raise NotImplementedError("ToDo: Removed, re-work.")

class XYCoord(object):
    '''Maintains (X, Y) coordinates of grid information for Pythagorean Triples.

    The primary purpose of this class is investigating (large) semi-prime numbers,
        such that their respective factors have 0-or-1 bits of difference from each other.

    Accepts:
        - an odd number, `n`
        - Optionally:
            - f1, f2: factors that multiply to n (exactly 2).

    Provides:
        If f1 and f2 are provided:
            A large set of points, and point-slope formulas, in addition to basic calculations on the input.
        Else:
            A minimal set of points, and point-slope formulas, and only caluclations on Input `n`
    '''

    @property
    def HasFacts(self):
        '''If f1 and f2 are passed-in such that f1*f2 == n,
        Is input n factored?
        '''
        return self._hasFacts

    @property
    def N(self):
        '''The input number.
        '''
        return self._n

    @property
    def n(self):
        '''The input number.
        '''
        return self._n

    @property
    def F1(self):
        '''If HasFacts, the lower or lesser of 2 factors of n.
        '''
        return self._f1

    @property
    def f1(self):
        '''If HasFacts, the lower or lesser of 2 factors of n.
        '''
        return self._f1

    @property
    def F2(self):
        '''If HasFacts, the greater of 2 factors of n.
        '''
        return self._f2

    @property
    def f2(self):
        '''If HasFacts, the greater of 2 factors of n.
        '''
        return self._f2

    @property
    def C(self):
        '''If HasFacts, the Centered value in the Center and Difference of Squares.
        '''
        return self._c

    @property
    def c(self):
        '''If HasFacts, the Centered value in the Center and Difference of Squares.
        '''
        return self._c

    @property
    def D(self):
        '''If HasFacts, the Difference value in the Center and Difference of Squares.
        '''
        return self._d

    @property
    def d(self):
        '''If HasFacts, the Difference value in the Center and Difference of Squares.
        '''
        return self._d

    @property
    def j(self):
        '''The j value, such that (Dj+k)(D(j+2)+k) == n
        IIF: HasFacts == True
        '''
        return self._j

    @property
    def k(self):
        '''The k value, such that (n-k^2)%d == 0.
        IIF: HasFacts == True
        '''
        return self._k

    @property
    def PointsList(self):
        '''A list of points, as defined by this class.
        '''
        return self._pl

    @property
    def PointSet(self):
        '''A dictionary of 2-point sets (tuples).
        '''
        return self._ps

    def __init__(self, n, f1=None, f2=None):
        assert not n is None, "Input `n` must be a number."
        assert n >= 3, "Input `n` must be an integer, >= 3!"
        self._n = gmp.mpz(n)
        if gmp.get_context().precision < 8*self._n.bit_length():
            gmp.get_context().precision = 8*self._n.bit_length()
        assert n%2 == 1 and n > 15, "Input n must be an odd number, greater than 15"
        assert not gmp.is_prime(n), "Input n must not be prime."
        if not f1 is None and f2 is None:
            if f1 < 0: f1 = -f1
            if gmp.f_divmod(n, f1)[1] == 0: f2 = gmp.f_div(n, f1)
            else: raise Exception("f1 is not a factor of n!\nWhen providing factors, both must be provided, or both must be None!")
        if f1 is None and not f2 is None:
            if f2 < 0: f2 = -f2
            if gmp.f_divmod(n, f2)[1] == 0: f1 = gmp.f_div(n, f2)
            else: raise Exception("f2 is not a factor of n!\nWhen providing factors, both must be provided, or both must be None!")
        if not f1 is None and not f2 is None:
            assert f1*f2 == n, "f1*f2 != n"
            if f1 > f2: f1, f2 = f2, f1
            self._f1, self._f2 = gmp.mpz(f1), gmp.mpz(f2)
            if self._f1 > self._f2:
                self._f1, self._f2 = self._f2, self._f1
            self._hasFacts = True
            self._c, self._d = gmp.f_div(self._f1+self._f2, 2), gmp.f_div(self._f2-self._f1, 2)
            self._j, self._k = gmp.f_divmod(self.f1, self.d)
        else:
            self._hasFacts = False
            self._f1, self._f2 = None, None
            self._c, self._d = None, None
            self._j, self._k = None, None
        self._init_done = False
        # ToDo: Stuff goes here.
        self._pl = {}
        for x in range(0, 9):
            self._pl[x] = None
        self._pl[3.2], self._pl[7.2] = None, None
        self._pl['00'] = XY(gmp.mpz(3), gmp.mpz(4))
        self._pl[4] = XY(self.n, 2*gmp.f_div(self.n+1, 2)*gmp.f_div(self.n-1, 2))
        self._pl[5] = XY(self.n*(self.n-2), 2*gmp.f_div(self.n+(self.n-2), 2))
        self._ps = {}
        self._ps["004"] = ['00', 4]
        self._ps["005"] = ['00', 5]
        self._ps["45"] = [4, 5]
        if self.HasFacts:
            self._pl[0] = XY(self.f1+2, 2*gmp.f_div(self.f1+3, 2)*gmp.f_div(self.f1+1, 2))
            self._pl[1] = XY(self.f1*(self.f1+2), 2*gmp.f_div(self.f1+(self.f1+2), 2))
            self._pl[2] = XY(self.f2, 2*gmp.f_div(self.f2+1, 2)*gmp.f_div(self.f2-1, 2))
            self._pl[6] = XY(self.n*self.f1, 2*gmp.f_div(self.n+self.f1, 2)*gmp.f_div(self.n-self.f1, 2))
            self._pl[8] = XY(self.f1*self.f2, 2*gmp.f_div(self.f1+self.f2, 2)*gmp.f_div(self.f2-self.f1, 2))
            self._pl[3] = XY(self.f2*(self.f2-2), 2*gmp.f_div(self.f2+(self.f2-2), 2))
            self._pl[7] = XY(self.n*(self.f2-2), 2*gmp.f_div(self.n+(self.f2-2), 2)*gmp.f_div(self.n-(self.f2-2), 2))
            self._ps["01"], self._ps["23"] = [0, 1], [2, 3]
            self._ps["18"], self._ps["38"] = [1, 8], [3, 8]
            self._ps["16"], self._ps["37"] = [1, 6], [3, 7]
            self._ps['000'], self._ps['002'] = ['00', 0], ['00', 2]
            self._ps['001'], self._ps['003'] = ['00', 1], ['00', 3]
            self._ps["13"], self._ps["15"], self._ps["35"] = [1, 3], [1, 5], [3, 5]
            self._ps["02"], self._ps["04"], self._ps["24"] = [0, 2], [0, 4], [2, 4]
            self._ps["46"], self._ps["47"], self._ps["67"] = [4, 6], [4, 7], [6, 7]
            self._ps["56"], self._ps["57"] = [5, 6], [5, 7]
            self._pl[2.2] = XY(self.f2+2, 2*gmp.f_div(self.f2+3, 2)*gmp.f_div(self.f2+1, 2))
            self._pl[3.2] = XY(self.f2*(self.f2+2), 2*gmp.f_div(self.f2+(self.f2+2), 2))
            self._pl[7.2] = XY(self.n*self.f2, 2*gmp.f_div(self.n+self.f2, 2)*gmp.f_div(self.n-self.f2, 2))
            self._ps["02_"], self._ps["24_"], self._ps["002_"] = [0, 2.2], [2.2, 4], ['00', 2.2]
            self._ps["23_"], self._ps["38_"], self._ps["37_"], self._ps["003_"] = [2.2, 3.2], [3.2, 8], [3.2, 7.2], ['00', 3.2]
            self._ps["13_"], self._ps["35_"], self._ps["47_"], self._ps["67_"], self._ps["57_"] = [1, 3.2], [3.2, 5], [4, 7.2], [6, 7.2], [5, 7.2]
        nbl2 = self.n.bit_length()*2
        tmp_pl_key_lst = list(self._pl.keys())
        for pl in tmp_pl_key_lst:
            st = str(pl).replace('.2', 'b')
            self._pl["Pt"+st] = self._pl[pl]
            self.__setattr__('Pt{}'.format(st), self._pl["Pt"+st])
        tmp_ps_key_lst = list(self._ps.keys())
        for ps in tmp_ps_key_lst:
            if isinstance(ps, str) and isinstance(self._ps[ps], list):
                if len(self._ps[ps]) == 2 and all([True if y in self._pl else False for y in self._ps[ps]]):
                    tpt1, tpt2 = self._pl[self._ps[ps][0]], self._pl[self._ps[ps][1]]
                    self._ps["Pt"+ps] = PtSlope(tpt1, tpt2, nbl2)
                    self.__setattr__('Pt{}'.format(ps), self._ps["Pt"+ps])
                    self.__setattr__('Pt{}m'.format(ps), self._ps["Pt"+ps].m)
                    self.__setattr__('Pt{}b'.format(ps), self._ps["Pt"+ps].b)
                    self.__setattr__('Pt{}l'.format(ps), self._ps["Pt"+ps].l)
        self._init_done = True

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



