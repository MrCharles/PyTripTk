#!/usr/bin/sage

'''

This (python) script is meant to be run at the command line.

It requires Sage (maths package) be installed.
It requires a Python 2 environment.

It accepts a single argument. One of the following:

    (1) Either a file in the `/tmp` directory containing a single number
    (2) A number.

The input number, in whichever form, must be 1 mod 4 (1 equiv N mod 4).

This file has single function: `sum_of_two_squares(p)`
And implements the script designated at:
    http://wstein.org/edu/2007/spring/ent/ent-html/node75.html

See also:
    https://en.wikipedia.org/wiki/Congruum
    https://en.wikipedia.org/wiki/Sum_of_two_squares_theorem
    https://en.wikipedia.org/wiki/Brahmagupta%E2%80%93Fibonacci_identity

'''

import sys

#from sage.all import *
from sage.all import Integer, Mod, continued_fraction, floor, sqrt, fork, is_prime

@fork(timeout=305, verbose=False)
def sum_of_two_squares(p):
    p = Integer(p)
    assert p%4 == 1, "p must be 1 mod 4"
    assert is_prime(p), "p must be prime"
    try:
        r = Mod(-1, p).sqrt().lift()
        v = continued_fraction(-r/p)
        n = floor(sqrt(p))
        for x in v.convergents():
            c = r*x.denominator() + p*x.numerator()
            if -n <= c and c <= n:
                return (abs(x.denominator()), abs(c))
        return None
    except ZeroDivisionError as zde:
        return None
    except:
        raise

if __name__ == '__main__':
    if sys.argv[1].startswith('/tmp/'):
        import os
        if os.path.isfile(sys.argv[1]):
            if os.path.isfile(sys.argv[1]+".out"):
                ovar = None
                with open(sys.argv[1]+".out", 'rb') as _fh:
                    ovar = _fh.read()
                ovar = ovar.strip('\n').strip(' ')
                print(ovar)
            else:
                ivar = None
                with open(sys.argv[1], 'rb') as _fh:
                    ivar = _fh.read()
                ivar = ivar.decode('utf8').strip('\n').strip(' ')
                ovar = sum_of_two_squares(ivar)
                with open(sys.argv[1]+".out", 'wb') as _fh:
                    if isinstance(ovar, tuple):
                        _fh.write(str(ovar).encode('utf8'))
                    else:
                        _fh.write(repr(ovar).encode('utf8'))
                print(ovar)
        else:
            raise Exception("No such file %s" % (sys.argv[1]))
    else:
        ovar = sum_of_two_squares(sys.argv[1])
        print(ovar)



