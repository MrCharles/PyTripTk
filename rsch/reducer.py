#!/usr/bin/env python3

'''
A reduction methodology.

It's hackish. It needs improvement.

But it works.

It will reduce any number to a list of
sum and difference of squares, such that
each member of the list if a power of 2.

This, however, does not provide a useful
binary reduction.

==================================================
==================================================

This process is heavily RECURSIVE.
You might need to increase the max
recursion depth (default: 1000).

==================================================
==================================================

USAGE:

    >>> from rsch.reducer import Reducer as red
    >>> n = # Some positive integer.
    >>> var = red(n)
    ... # a lot of output...
    >>> var.Reduced
    ... [a list of powers of 2, such that...]
    >>> sum(var.Reduced) == n
    ... True

==================================================
==================================================

To disable the output, modify the `Reducer`
    (class) in this file.

Change: `debug = True`
    To: `debug = False`

Static Information:
    3 == 2^2-1 == DifferenceOfSquares(2, 1)
    5 == 2^2+1 == SumOfSquares(2, 1)

Everything should reduce to the Static Information,
And then be condensed and consolidated.

The top-level (parent) database, from the process
that spawns things should keep track of things.
For example, if in reduction, the number 17 is
encountered ... if encountered a second time, its
reduction is pulled from the database.

A secondary condensation and consolidation is
called in the `MakeSane` references.

Entries in the database should conform to being
sanitized BEFORE entry (otherwise, lists explode
in size).

==================================================
==================================================

A simple example: 28

    28/2 = 14
    14/2 = 7
    Even = 2*2 == 4
    3 == 7%4
    7 == DifferenceOfSquares((7+1)/2, (7-1)/2)
    7 == == DoS(4, 3)
    7 == 4^2 - (2^2-1^2)^2
    7 == 4^2 - (2^4-2^3+1)
    7 == 2^3-1
    Output List == [2^3, -(2^0)]
    Output List == [8, -1]

A counter-point would be 26, which is 13*2, with
1==13%4. Which would be SumOfSquares instead of
DifferenceOfSquares.

Part of this is removing small factors from large
numbers. Where a number cannot be fully factorized
over several 10-second periods (for each fact
found within 10 seconds) reduction, that number is
then reduced to DifferenceOfSquares if 3%4, else
SumOfSquares if 1%4.

==================================================
==================================================

A Quick Observation:

    The final output DOES NOT MATTER in terms of
    absolute reduction. This is to say, instead of
    using SumOfSquares where n==1%4, it just takes
    longer (larger numbers) using only
    DifferenceOfSquares...

    ((n+1)/2)^2-((n-1)/2)^2 == a^2+b^2

    They're equivalent, so the ultimate reduction
    will be the same, except Square Sums are
    smaller numbers.

==================================================
==================================================

RELEVANCE/PROOF:

    Let's take a large semi-prime N==n
    F1*F2 == n
    C^2-D^2 == n

    >>> from rsch import reducer as rude
    >>> from rsch.reducer import Reducer as red
    >>> f1_red, d_red, c_red, f2_red, n_red = red(f1), red(d), red(c), red(f2), red(n)
    # And now you sit and wait for a few hours if n.bits ~ 1024
    >>> assert 0 == len(rude.MakeSane(n_red.Reduced+[-x for x in rude.MakeSane(rude._mulst2(f1_red.Reduced, f2_red.Reduced))]))
    >>> assert 0 == len(rude.MakeSane(n_red.Reduced+[-x for x in rude.MakeSane(rude._sq_lst(c_red.Reduced)+[-y for y in rude._sq_lst(d_red.Reduced)])]))
    >>> assert 0 == len(rude.MakeSane([-x for x in rude.MakeSane(rude._sq_lst(c_red.Reduced)+[-y for y in rude._sq_lst(d_red.Reduced)])]+[-x for x in rude.MakeSane(rude._mulst2(f1_red.Reduced, f2_red.Reduced))]))

    This is sort-of an asinine example, but it
    makes the point. The point is EQUIVALENCY.
    Tested, working.

    WARNING: Probably typos or something incorrect
        in the above documentation. The example
        above was thrown together by hand.

==================================================
==================================================

DISCLAIMER/WARRANTY:

    NONE.

    If things don't make sense, read the code.
    If things still don't make sense,
    and you're 100% sure that something is broken:

        SEND SPECIFIC DETAILS

    If you cannot, do not, or will not ...
        ... send specific details, ...
    you will be skewered (in public, if possible).

==================================================
==================================================

REQUIREMENTS:

    (1) gmpy2 # 3rd party library (Python 2 and 3)
    (2) sage  # mathematics package (Python 2)
    (3) PyTripTk (this library, Python 3)
    (4) psutil (Python 2 and 3)

'''

# pylint: disable=W0602,C0305

# Library
from subprocess import TimeoutExpired

# 3rd party
import gmpy2 as gmp

# Local
import PyTripTk as pttk
from PyTripTk.util.OfSquares import DifferenceOfSquares as dos
from PyTripTk.util.OfSquares import SumOfSquares as sos
from PyTripTk.util.factor import factor

_2POW = [gmp.mpz(1), gmp.mpz(2), gmp.mpz(4), gmp.mpz(8), gmp.mpz(16),
         gmp.mpz(32), gmp.mpz(64), gmp.mpz(128), gmp.mpz(256),
         gmp.mpz(512), gmp.mpz(1024), gmp.mpz(2048), gmp.mpz(4096)]
_DB = {}
_MDDB = {'MaxDepth':{},}
_ACDB = {}

class FactorsTuple(tuple):
    '''A tuple of factors.
    '''

    def __init__(self, tup):
        assert isinstance(tup, tuple) or isinstance(tup, list)
        super(tuple, tuple(tup))

def _p3_dos():
    '''Returns the difference of squares if a number is 3.
    '''
    return [gmp.mpz(4), -gmp.mpz(1)]

def _p5_sos():
    '''Returns the sum of squares if a number is 5.
    '''
    return [gmp.mpz(4), gmp.mpz(1)]

def _2pow(val):
    '''Is the input a power of two?
    '''
    while val > max(_2POW):
        _2POW.append(2*max(_2POW))
    return val in _2POW

def _mulst1(val, lst):
    assert isinstance(val, int) or isinstance(val, type(gmp.mpz()))
    assert isinstance(lst, list) or isinstance(lst, FactorsTuple)
    tmp = []
    for x in lst:
        tmp.append(val*x)
    return tmp

def _mulst2(lst1, lst2):
    '''Multiplies 2 lists together.
    '''
    try:
        assert isinstance(lst1, list) or isinstance(lst1, FactorsTuple)
        assert isinstance(lst2, list) or isinstance(lst2, FactorsTuple)
    except AssertionError as aerr:
        print("List 1 Type: {}".format(type(lst1)))
        print("List 2 Type: {}".format(type(lst2)))
        raise aerr
    if len(lst1)*len(lst2) > 10000:
        print("WARN:\n\tlen(lst1)*len(lst2) > 1000:\n\t\tThis could take some time!")
    if len(lst1) == 1:
        return _mulst1(lst1[0], lst2)
    elif len(lst2) == 1:
        return _mulst1(lst2[0], lst1)
    else:
        #print("Multiplying Lists:\n\t{}\n\t{}".format(lst1, lst2))
        tmp = []
        for x in lst1:
            for y in lst2:
                val = x*y
                if -val in tmp:
                    tmp.remove(-val)
                elif val in tmp:
                    tmp[tmp.index(val)] *= 2
                else:
                    tmp.append(val)
        #print("\tRETURNING:\n\t\t{}".format(tmp))
        return tmp

def _sq_lst(lst):
    '''Square a list.
    '''
    try:
        assert isinstance(lst, list) or isinstance(lst, FactorsTuple)
    except AssertionError as aerr:
        print("{}, {}".format(type(lst), lst))
        raise aerr
    return _mulst2(lst[::], lst[::])

def _mulst_single(lst):
    '''Multiplies a single list together.
    '''
    try:
        assert isinstance(lst, list) or isinstance(lst, FactorsTuple)
    except AssertionError as aerr:
        print("List Type: {}".format(type(lst)))
        raise aerr
    tmp = gmp.mpz(1)
    for x in lst:
        tmp *= x
    return tmp

def MakeSane(lst):
    '''Makes an input list sane by removing and consolidating entries.

    This is designed, primarily, to work with numbers that are powers of 2.
    '''
    check_sanity = True
    tmp = lst[::]
    while check_sanity:
        check_sanity = False # Doesn't need to be checked again unless something changes.
        for x in list(set(tmp)):
            # Cancellation
            try:
                if x in tmp and -x in tmp:
                    tmp.remove(x)  # Remove both (1 of 2)
                    tmp.remove(-x) # Remove both (2 of 2)
                    check_sanity = True # Always check the sanity after modifying the list.
            except:
                print("Could not remove value: {} or {}".format(x, -x), flush=True)
                print("In: {}".format(tmp), flush=True)
                raise
            # Division by 2
            try:
                if x in tmp and -gmp.f_div(x, 2) in tmp:
                    tmp[tmp.index(x)] += -gmp.f_div(x, 2) # Modify one
                    tmp.remove(-gmp.f_div(x, 2))          # Remove the other
                    check_sanity = True # Always check the sanity after modifying the list.
            except:
                print("Could not remove value: {} or {}".format(x, -x), flush=True)
                print("In: {}".format(tmp), flush=True)
                raise
            # Multiplication by 2
            try:
                if tmp.count(x) >= 2:
                    tmp[tmp.index(x)] *= 2 # Modify one
                    tmp.remove(x)          # Remove the other.
                    check_sanity = True # Always check the sanity after modifying the list.
            except:
                print("Could not remove value: {} or {}".format(x, -x), flush=True)
                print("In: {}".format(tmp), flush=True)
                raise
    # Sort and return.
    tmp.sort(key=lambda x: abs(x))
    return tmp

class Reducer():
    '''Reduces a number or object to constituient components.
    '''

    global _2POW
    global _DB
    global _MDDB
    global _ACDB

    debug = True

    @property
    def Obj(self):
        '''The input object.
        '''
        return self._obj

    @property
    def Type(self):
        '''The type of input object.
        '''
        return self._typ

    @property
    def Reduced(self):
        '''The reduced form of the input.
        '''
        if not self._isSane:
            self._red = MakeSane(self._red)
            self._isSane = True
        return self._red

    def __init__(self, obj, depth=0, md=None, dsq_only=False):
        self._dsq_only = dsq_only
        self._depth = depth # The depth of the recursion; add 1 each time this process calls itself.
        if depth == 0 and (isinstance(obj, type(gmp.mpz())) or isinstance(obj, int)):
            _MDDB['MaxDepth'][obj] = 0
            self._md = obj
        else:
            self._md = md
            if not md is None and depth > 0:
                if md in _MDDB['MaxDepth']:
                    if depth > _MDDB['MaxDepth'][md]:
                        _MDDB['MaxDepth'][md] = depth
        self._red = [] # The sum(+/-) of residual reduction of elements.
        self._isSane = False # Has `red` been reduced?
        if self.debug:
            print("Depth {}, ID: {} \n\tReceived: {} of Value: {}".format(depth, id(self), type(obj), obj), flush=True)
        try:
            if isinstance(obj, int) or isinstance(obj, type(gmp.mpz())):
                self._obj = gmp.mpz(obj)
                self._typ = 'MPZ'
                if self._obj == 1:
                    self._red.append(gmp.mpz(1))
                elif self._obj == 2:
                    self._red.append(gmp.mpz(2))
                elif self._obj == 3:
                    self._red.extend(_p3_dos())
                elif self._obj == 4:
                    self._red.append(gmp.mpz(4))
                elif self._obj == 5:
                    self._red.extend(_p5_sos())
                else:
                    self._reduce_mpz()
            elif isinstance(obj, sos):
                if self._dsq_only:
                    self._obj = dos(gmp.dos(obj[0]**2+obj[1]**2))
                    self._typ = 'DOS'
                    self._reduce_dos()
                else:
                    self._obj = obj
                    self._typ = 'SOS'
                    self._reduce_sos()
            elif isinstance(obj, dos):
                self._obj = obj
                self._typ = 'DOS'
                self._reduce_dos()
            elif isinstance(obj, FactorsTuple):
                self._obj = obj
                self._typ = 'FTUP'
                self._reduce_ftup()
            else:
                raise Exception("Unhandled object type: {}".format(type(obj)))
        except:
            raise
        finally:
            if self.debug:
                print("Depth {}:\n\tFinal: {}".format(depth, self._red), flush=True)

    def _reduce_mpz(self):
        '''Reduce an integer.
        '''
        if self._obj == 3:
            self._red.extend(_p3_dos())
        elif self._obj == 5:
            self._red.extend(_p5_sos())
        elif _2pow(self._obj):
            self._red.extend(self._obj)
        elif self.InDb(self._obj):
            self._red.extend(self.GetDb(self._obj))
        elif gmp.is_prime(self._obj):
            if self._obj%4 == 1:
                tmp = Reducer(sos(gmp.sos(self._obj)), depth=self._depth+1, md=self._md, dsq_only=self._dsq_only)
                self._red.extend(tmp.Reduced)
                self.SetDb(self._obj, MakeSane(tmp.Reduced))
            else:
                tmp = Reducer(dos(gmp.dos(self._obj)), depth=self._depth+1, md=self._md, dsq_only=self._dsq_only)
                self._red.extend(tmp.Reduced)
                self.SetDb(self._obj, MakeSane(tmp.Reduced))
        elif self._obj%2 == 0:
            tmp = gmp.f_div(self._obj, 2)
            even = gmp.mpz(2)
            while tmp%2 == 0:
                tmp = gmp.f_div(tmp, 2)
                even *= 2
            self._red.extend(self.GetDb(tmp))
            for x in range(0, len(self._red)):
                self._red[x] *= even
        else:
            facts = None
            try:
                # if this succeeds, see 'else'
                facts = factor(self._obj, tout=10)
                if len(facts) == 1:
                    if facts[0] == self._obj:
                        assert not self._obj in facts
                        #raise TimeoutExpired(cmd='Intentional', timeout=10) # Intentional.
            except (TimeoutExpired, AssertionError) as err2:
                # if the above does not succeed, this should, no 'finally'
                if self.debug:
                    print("Caught factor timeout: {}".format(self._obj), flush=True)
                tmp, even = gmp.mpz(self._obj), gmp.mpz(1)
                while tmp%2 == 0:
                    even *= 2
                    tmp = gmp.f_div(tmp, 2)
                tmp2 = Reducer(dos(gmp.dos(tmp)), depth=self._depth+1, md=self._md, dsq_only=self._dsq_only)
                self._red.extend(tmp2.Reduced)
                if even != 1:
                    for x in range(0, len(self._red)):
                        self._red[x] *= even
                self.SetDb(self._obj, self.Reduced)
            except:
                raise
            else:
                # if factorization succeeds (does not time-out), do this.
                if self.debug:
                    print("Factorization succeeded!\n\t{}".format(self._obj))
                ftup = FactorsTuple(facts)
                tmp = Reducer(ftup, depth=self._depth+1, md=self._md, dsq_only=self._dsq_only)
                self._red.extend(tmp.Reduced)
                self.SetDb(self._obj, self.Reduced)
        assert sum(self._red) == self._obj

    def _reduce_ftup(self):
        '''Reduce a tuple of factors to its components
        '''
        tmp, even = [], gmp.mpz(1)
        for x in range(0, len(self._obj)):
            #if isinstance(self._obj[x], type(gmp.mpz())) or isinstance(self._obj[x], int) and self._obj[x] == 2:
            if self._obj[x] == 2:
                even *= self._obj[x]
            else:
                if self.debug:
                    print("Depth {}: Processing {}".format(self._depth, self._obj[x]))
                tmp.append(self.GetDb(self._obj[x]))
                if self.debug:
                    print("\tDepth {}: FTUP: {}".format(self._depth, tmp), flush=True)
        while len(tmp) >= 2:
            if self.debug:
                print("\tDepth {}: FTUP Multiply: {}{}".format(self._depth, tmp[0], tmp[1]))
            tmp.append(_mulst2(tmp[0], tmp[1]))
            if self.debug:
                print("\tDepth {}: FTUP: {}".format(self._depth, tmp), flush=True)
            del tmp[1], tmp[0]
            if self.debug:
                print("\tDepth {}: FTUP: {}".format(self._depth, tmp), flush=True)
        if self.debug:
            print("\tEv: {}, Tmp: {}".format(even, tmp), flush=True)
        if even > 1:
            self._red.extend(_mulst1(even, tmp[0]))
        else:
            self._red.extend(tmp[0])
        try:
            assert sum(self._red) == _mulst_single(self._obj)
        except AssertionError as aerr:
            if self.debug:
                print("Reduced: {}".format(self._red))
                print("Input: {}".format(self._obj), flush=True)
            raise aerr
        except:
            raise

    def _reduce_sos(self):
        '''Reduce a sum of squares.
        '''
        for val in [self._obj[0], self._obj[1]]:
            if val == 3:
                tmp = _p3_dos()
                tmp = _sq_lst(tmp)
                self._red.extend(tmp)
            elif val == 5:
                tmp = _p5_sos()
                tmp = _sq_lst(tmp)
                self._red.extend(tmp)
            elif _2pow(val):
                self._red.append(val**2)
            else:
                tmp = self.GetDb(val)
                tmp = _sq_lst(tmp)
                self._red.extend(tmp)
                #self.SetDb(val**2, self.Reduced) # should be sane in the membrane (database)
        try:
            assert sum(self._red) == self._obj[0]**2+self._obj[1]**2
        except AssertionError as aerr:
            if self.debug:
                print("Reduced: {} {}".format(sum(self._red), self._red))
                print("Input, SOS: {}, {}".format(self._obj[0], self._obj[1]))
            raise aerr

    def _reduce_dos(self):
        '''Reduce a difference of squares.
        '''
        #raise Exception("ToDo: Finish this, implemented GetDb, same as SOS.")
        crsq, drsq = None, None
        if self._obj.C == 3:
            crsq = _sq_lst(_p3_dos())
        elif self._obj.C == 5:
            crsq = _sq_lst(_p5_sos())
        elif _2pow(self._obj.C):
            crsq = [self._obj.C**2,]
        else:
            tmp = self.GetDb(self._obj.C)
            #Reducer(self._obj.C, depth=self._depth+1, par=self).Reduced
            crsq = _sq_lst(tmp)
            #self.SetDb(self._obj.C**2, crsq)
        if self._obj.D == 3:
            drsq = _sq_lst(_p3_dos())
        elif self._obj.D == 5:
            drsq = _sq_lst(_p5_sos())
        elif _2pow(self._obj.D):
            drsq = [self._obj.D**2,]
        else:
            tmp = self.GetDb(self._obj.D)
            drsq = _sq_lst(tmp)
            #Reducer(self._obj.D, depth=self._depth+1, par=self).Reduced
            #self.SetDb(self._obj.D**2, drsq)
        if self.debug:
            print("Depth {}: Csq={}".format(self._depth, crsq))
            print("Depth {}: Dsq={}".format(self._depth, crsq))
        tmp = crsq[::]
        for x in drsq:
            if x in tmp:
                tmp.remove(x)
            elif -x in tmp:
                tmp[tmp.index(-x)] *= 2
            else:
                tmp.append(-x)
        self._red.extend(tmp)
        try:
            assert sum(self._red) == self._obj.C**2-self._obj.D**2
        except AssertionError as aerr:
            if self.debug:
                print("Reduced: {} {}".format(sum(self._red), self._red))
                print("Input, DOS: C={}, D={}".format(self._obj.C, self._obj.D))
            raise aerr

    def InDb(self, key):
        '''Check if an entry exists in the database.
        '''
        return key in _DB.keys()

    def GetDb(self, key):
        '''Get an entry from the database.
        Recursively gets from the parent object.
        '''
        if isinstance(key, type(gmp.mpz())) or isinstance(key, int):
            _key = key
        elif isinstance(key, sos):
            _key = key[0]**2+key[1]**2
        elif isinstance(key, dos):
            _key = key.C**2-key.D**2
        else:
            raise Exception("Unrecognized/Unhandled key object: {}".format(type(key)))
        if not self.InDb(_key):
            tmp = Reducer(key, depth=self._depth+1, md=self._md, dsq_only=self._dsq_only)
            self.SetDb(_key, val=tmp.Reduced) # should be sane in the membrane (database)
            _ACDB[_key] = 0
        _ACDB[_key] += 1
        return _DB[_key]

    def SetDb(self, key, val):
        '''Set a new entry from the database.
        Recursively sets the entry in the top-level parent object.
        '''
        if key in _DB:
            assert _DB[key] == val
        else:
            assert isinstance(val, list)
            try:
                assert sum(val) == key
            except AssertionError as aerr:
                print("Sum: {}".format(sum(val)))
                print("Val: {}".format(val))
                print("Sane: {}".format(sum(MakeSane(val))))
                print("Key: {}".format(key))
                raise aerr
            _DB[key] = MakeSane(val)



