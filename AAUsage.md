## Additional, or Advanced Usage

Following the basic usage in the [Readme](./README.md), this covers things like [Euler Bricks](https://en.wikipedia.org/wiki/Euler_brick) and [Arithmetic Progression of Squares (APS)](http://math.ucr.edu/~res/math153/history07b.pdf).

### Basics from Readme...

```python
>>> import PyTripTk.PyTrip as pt
>>> import PyTripTk.NextGen as ng
>>> import PyTripTk.PrevGen as pg
>>> import PyTripTk.ReduceRegen as rr
>>> import PyTripTk.EulerBrick as eb
>>> import PyTripTk.APS as aps
>>> rtpt = pt.ROOT
>>> rtpt
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> _
```

### Fibonacci Boxes (FB, or FibBox)

I was introduced to FibBoxes by [Price's 2008 paper, "The Pythagorean Tree: A New Species"](https://arxiv.org/pdf/0809.4324.pdf). It is a simple term to represent the [Sum of Squares](https://en.wikipedia.org/wiki/Sum_of_two_squares_theorem), and [Difference of Squares](https://en.wikipedia.org/wiki/Difference_of_two_squares), in a simple, succinct "Box".

**For example:**

```text
Descriptive
Box             FROM          TO
---------     --------     --------
| D  F1 |     | 1  1 |     | 1  3 |    (3 shifts up,
|       | --> |      | --> |      |     recalc D, C, F2)
| C  F2 |     | 2  3 |     | 4  5 |
---------     --------     --------
(A, B, C)     (3, 4, 5)    (15, 8, 17)

              --------     --------
              | 1  1 |     | 2  1 |    (3 shifts left,
              |      | --> |      |     recalc F1, D, F2)
              | 2  3 |     | 3  5 |
              --------     --------
              (3, 4, 5)    (5, 12, 13)

              --------     --------
              | 1  1 |     | 3  1 |    (3 shifts diag,
              |      | --> |      |     recalc F1, C, F2)
              | 2  3 |     | 4  7 |
              --------     --------
              (3, 4, 5)    (7, 24, 25)

              --------     --------
              | 1  1 |     | 2  3 |    (2, 3 shift up,
              |      | --> |      |     recalc C, F2)
              | 2  3 |     | 5  7 |
              --------     --------
              (3, 4, 5)    (21, 20, 29)

```

This can _(very simply)_ be done for each of the three (3) transitions in either of the Price or Classic _(Primitive)_ Pythagorean Triple Trees. It is a quick and simple visual diagram of what the algebraic algorithm is doing behind the scenes. _(Trivial, but Pretty Nifty.)_

Similarly, knowing the members of a Pythagorean Triple, it can be reduced to a Fibonacci Box.

**The descriptive box is as follows:**

* `D` == Difference
* `C` == Center
* `F1` == Factor 1 (smaller)
* `F2` == Factor 2 (larger)
* `C == F1+D`
* `F2` == C+D == F1+2D
* `F1` == F2-2D == C-D
* _whatever you like, algebraically_

**And, the `A`, `B`, and `C` calculations are as follows:**

* `A` == F1&middot;F2 == C<sup>2</sup>-D<sup>2</sup>
* `B` == 2&middot;C&middot;D
* `C` == C<sup>2</sup>+D<sup>2</sup> == (C&middot;F2)-(D&middot;F1) == (C&middot;F1)+(D&middot;F2)

Interestingly enough, if you start with the standard Fibonacci sequence of `1, 1, 2, 3` &mdash; every 3rd sequential rotatioin to the next Fibonacci Number is non-primitive, the other _(first)_ 2/3 are primitive. _(And, when reduced to primitive, the non-primitive rotations of the sequence yield other members of the tree to be rotated the same and reduced the same, building-out a full ternary tree.)_

```python
>>> import PyTripTk.PyTrip as pt
>>> import PyTripTk.FibBox as fb
>>> rtpt = pt.ROOT
>>> rtpt
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> fibbox = fb.FB_FromPT(rtpt)
>>> fibbox
FibBox(F1=mpz(1), D=mpz(1), C=mpz(2), F2=mpz(3), abc=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
>>> ptviafb = fb.PT_FromFB(fibbox)
>>> ptviafb
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
```

---

### Euler Bricks

Euler Bricks are another interesting beast _(unto themselves)_. Simply put, every Pythagorean Triple can be used to generate an Euler Brick _(which, itself, has constituient Pythagorean Triples)_, but not every Pythagorean Triple is a member of an Euler Brick. If the input for an Euler Brick is a Primitive Pythagorean Triple, then, the `abd` triple will be primitive to start, but the `ace` and `bcf` triples will not be. This software automatically reduces `ace` and `bcf` triples to their Primitive _(PT)_ form. Reversal is calculated directly, even though the non-primitive values are saved in a `namedtuple`.

There is a concept of a "twin" brick, that can be formed from permutations of the calculated input variables. However, this "twin" will always reduce to the primitive form of the brick _(using basic GCD checks)_. Thus, non-primitive forms are practically useless, other than as a novelty. You could use non-primitive Euler Brick "twin" formation to generate some very large numbers, very rapidly, but, they would all eventually have a lot of very common multiples; thus reducing back to the primitive form. But, again, still interesting _(though a novelty)_.

```python
>>> import PyTripTk.PyTrip as pt
>>> import PyTripTk.EulerBrick as eb
>>> rtpt = pt.ROOT
>>> rtpt
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> ebrick = eb.EB_FromPT(rtpt)
>>> ebrick
EulerBrick(a=117, b=44, c=240, d=125, e=267, f=244, abd=PyTrip(a=mpz(117), b=mpz(44), c=mpz(125)), ace=PyTrip(a=mpz(39), b=mpz(80), c=mpz(89)), bcf=PyTrip(a=mpz(11), b=mpz(60), c=mpz(61)), uvw=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
>>> ptviaeb = eb.PT_FromEB(ebrick)
>>> ptviaeb
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
```

**Notes:** The `PT_FromEB` function requires an input formed from `pt.PyTrip(#, #, #)`. Ideally _(or, optimally required)_, it descends from a PRIMITIVE Pythagorean Triple.

---

### Arithmetic Progression of Squares

As stated in the above [link](http://math.ucr.edu/~res/math153/history07b.pdf) _(Dr. Schultz, UC Riverside, Math 153, supplementary course material, 2012)_, there is a 1:1 _(one-to-one)_ relationship among Pythagorean Triples, and [Arithmetic Progression](https://en.wikipedia.org/wiki/Arithmetic_progression) of Squares &mdash; which holds for multiplicative identities of any primitive Pythagorean Triple _(the ratio is of the same multiplicative value above PI+/-1)_. This is to say, the APS _(ratio)_ for `(3, 4, 5)` will be the same as `(6, 8, 10)` _(each of `(3, 4, 5)` multiplied by 2, thus a valid, but not primitive, Pythagorean Triple)_. All of this via Leonardo Pisano's (Fibonacci's) _The Book of Squares_. Dr. Schultz did an excellent job of summarizing this for his Math History course, and I would recommend you read the above link if this interests you.

In addition, while the author prefers `(a=1%2, b=0%2, c=1%2)` for the order of Pythagorean Triples, APS requires `a < b < c`, thus APS conversions ensure this before converting back and forth. Restatement: In APS, the `a` and `b` values of a Pythagorean Triple need to be ordered. APS is delivered in sequential order, such that `APS=(p, q, r) | p < q < r`. _(Compared to using a%2==1 and b%2==0 for PT.)_

Also, note, there are often instances where `p < 0` in `APS(p, q, r)` &mdash; this is expected behavior. _(Read the paper, do the math, understand the why behind the ordered aspect of APS versus the unordered aspect of PT representations. Technically it's all just math. But some representations are better for certain things, and others for different things. For example, if `abs(p) < q` and `p < 0`, is it still a representative APS if the sign of `p` is changed. Answering this question helps understand the maths and specs and why this software was written the way it was.)_

```python
>>> import PyTripTk.PyTrip as pt
>>> import PyTripTk.APS as aps
>>> rtpt = pt.ROOT
>>> rtpt
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> apsq = aps.APS_FromPT(rtpt)
>>> apsq
PyTripAPS(p=mpz(1), q=mpz(5), r=mpz(7), abc=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
>>> ptviaaps = aps.PT_FromAPS(apsq)
>>> ptviaaps
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
```

---

See Also:

* [Basic Usage](./README.md)
* [Advanced Usage 2, PyTripExt](./AAUsage2.md)


