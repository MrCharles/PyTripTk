
# (Docs) License: Copyleft

The documentation in this directory is nothing special. Anyone can work-out the math on their own. This might help some people, but confuse others. Thus, what is here is meant to be helpful, but is provided without warranty or guarantee _(i.e. that it suits your personal educational or publication needs)_. As-is good practice: Cite Your Sources. If something here is helpful, please cite it accordingly. Otherwise, please consider these documents Open Source, or Copyleft, or GNU Documentation License or whatnot. _(I care not to split the difference over the obvious. There are things I have learned, such that I have forgotten where I learned them. Reciting passages verbatim without attribution is prohibited plagerism practice. Common terms, phrases, expressions, and maths need not apply for license where otherwise learned and forgotten. If anything significant, not obvious otherwise or elsewhere just-so-happens to be published here, such is a case you should cite the original source, and not just-so-happen to recite it from memory for your own personal benefit, etc.)_

* [GNU Free Documentation License, v1.3](https://www.gnu.org/licenses/fdl-1.3.en.html)
* [Copyleft](https://en.wikipedia.org/wiki/Copyleft)

