
\title{Pythagorean Triples:\\First Form Observations}
\author{Mr. Charles\\PyTripTk}
\date{2018-Jan-20}

\documentclass[11pt]{article}

% Packages: GeoFrame
    %\usepackage{showframe}
    %\usepackage[showframe]{geometry}
    \usepackage{geometry}

% Packages: Text Formatting
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}

% Packages: Row Formatting
    \usepackage{multirow}
    \usepackage[table,x11names]{xcolor}

% Packages: Bib
    \usepackage[backend=biber]{biblatex}

% Packages: Linking
    \usepackage{hyperref}
    \usepackage{url}

% Packages: Misc (req/needed)
    \usepackage{xpatch}

% Packages: Maths
    \usepackage{mathtools}
    \usepackage{xfrac}

% Packages: Unused
    %\usepackage{pdfcomment}
    %\usepackage{pdflscape}
    %\usepackage{changepage}
    %\usepackage{csquotes}
    %\usepackage{caption}

% Configuration: Bib
    \addbibresource{Rsch_WikiPedia.bib}
    \addbibresource{Rsch_Papers.bib}
    %\addbibresource{Rsch_OEIS.bib}
    %\addbibresource{Rsch_Misc.bib}

% Configuration: Unused
    \hypersetup{
        colorlinks,
        linkcolor={red!50!black},
        citecolor={blue!50!black},
        urlcolor={blue!35!black}
    }

% ==================================================
% Setup Notes:
% Use `hyperref` and `url` packages to get clickable
% links. Configure with `hypersetup` to remove boxes
% around the links and replace with color. Add the 
% `xcolor` package to use named colors.
% ==================================================

\begin{document}

\maketitle

\noindent
\textbf{Fibonacci Boxing,\\or, FibBox} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
A Fibonacci Box, or FibBox, or FibBox'ing is a visio-spatial representation of \
the \textbf{Sum and Difference of Squares} \cite{WikiPedia:Congruum} \
\cite{WikiPedia:DSQ}, as it pertains to Pythagorean Triples \
\cite{WikiPedia:PyTrip}. \\
\\

\noindent
\textbf{Fibonacci Boxes} were first noticed in Price's 2008 paper covering \
``\textit{The Pythagorean Tree: A New Species}" \cite{HLeePrice:08}. Such that \
Price's discovery of a new variation of the tree of primitive Pythagorean \
Triples contained this representation. \\
\\

\noindent
Since reading that paper in 2017, it has become a personal, internal mnemonic \
representation -- a different way of seeing or looking at the \textbf{Sum and \
Difference of Squares}, as it relates to Pythagorean Triples. \\
\\

%\noindent
%In other terms: I know not where the term ``Fibonacci Box" came from -- it \
%seems core and rudimentary; but no other known \textit{(or, better)} references. \\
%\\

\noindent
\textbf{Expository:} If you, the reader, don't find visio-spatial representations \
useful for understanding algorithms, then this document may not be of any use. \
Other resources include ``\textit{Tree of primitive Pythagorean triples}" \
\cite{WikiPedia:PTree} and ``\textit{Forumulas for generating Pythagorean triples}" \
\cite{WikiPedia:PTFormulas}.\\
\\

\newpage

\noindent
\textbf{Core Terms} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
Fibonacci Numbers \cite{WikiPedia:FibNum} are defined as \
\(F_{n} = F_{n-1} + F_{n-2}\), starting at \(F_{0}=1\) and \(F_{1}=1\), such \
that \(F_{2}=2\) and \(F_{3}=3\) and \(F_{4}=5\) and \(F_{5}=8\), etc \ldots{}

\[
    \begin{array}{l}
        1+1 = 2 \\
        1+2 = 3 \\
        2+3 = 5 \\
        3+5 = 8 \\
        5+8 = 13 
    \end{array}
\]

\noindent
However, if we re-organize these terms, sequentially, ... 

\[
    \begin{array}{|l|cccc|rcl|l|} \hline
                     & \multicolumn{4}{c|}{\text{Fib Terms}} & \multicolumn{3}{c|}{\text{PyTrip}} & \\
        \text{Index} & F_{1\gets} & F_{2\gets} & F_{3\gets} & F_{4} & a & b & c & \text{Notes} \\ \hline
        0  &  1    &  1    &  2    &  3   &   3 &    4 &    5 & \texttt{Primitive}      \\
        1  &  1    &  2    &  3    &  5   &   5 &   12 &   13 & \texttt{Primitive}      \\
        2  &  2    &  3    &  5    &  8   &  16 &   30 &   34 & \texttt{NOT primitive}  \\ \hline
        2r &       &       &       &      &   8 &   15 &   17 & \texttt{Primitive Form} \\ \hline
        3  &  3    &  5    &  8    & 13   &  39 &   80 &   89 & \texttt{Primitive}      \\
        4  &  5    &  8    & 13    & 21   & 105 &  208 &  233 & \texttt{Primitive}      \\
        5  &  8    & 13    & 21    & 34   & 272 &  546 &  610 & \texttt{NOT primitive}  \\ \hline
        5r &       &       &       &      & 136 &  273 &  305 & \texttt{Primitive Form} \\ \hline
        6  & 13    & 21    & 34    & 55   & 715 & 1428 & 1597 & \texttt{Primitive}      \\
        7  & \gets & \gets & \gets & calc &   a &    b &    c & \texttt{Primitive}      \\ \hline
    \end{array}
\]

%2-----------------21------------------41------------------61------------------81%
\noindent
The rotation of terms becomes obvious, one to the next, and every 3rd is not \
primitive, but has a primitive \textit{(Pythagorean Triple)} form when the GCD \
\textit{(greatest common denominator)} divides all of \((a, b, c)\). \\
\\

\noindent
\textbf{Restatement:} Whenever the outside terms are both even, the calculation \
is non-primitive, such that the two \textbf{EVEN} outside terms lead to a \
Pythagorean Triple with \(GCD(a, b, c) \ne 1\); and division by 2 leads to a \
\textit{(more desirable)} primitive form. \textit{(At least for this software.)}\\
\\

\newpage

\noindent
\textbf{Fibonacci Boxing \textit{of terms}} \\
\\

\[
    \left[
        \begin{array}{cc}
            1 & 1 \\
            2 & 3
        \end{array}
    \right]
\]

\noindent
The box above, 2x2, columns and rows, could be interpreted a couple of ways. \
For clarification ...

\[
    \begin{array}{|l|cccc|rcl|l|} \hline
                     & \multicolumn{4}{c|}{\text{Fib Terms}} & \multicolumn{3}{c|}{\text{PyTrip}} & \\
        \text{Index} & T_{1} & T_{2} & T_{3} & T_{4} & a & b & c & \text{Notes} \\ \hline
        0  &  1    &  1    &  2    &  3   &   3 &    4 &    5 & \texttt{Primitive}      \\
        1  &  1    &  2    &  3    &  5   &   5 &   12 &   13 & \texttt{Primitive}      \\
        2  &  2    &  3    &  5    &  8   &  16 &   30 &   34 & \texttt{NOT primitive}  \\ \hline
    \end{array}
\]

\noindent
Becomes ...

\[
    \left[
        \begin{array}{cc}
            T_{2} & T_{1} \\
            T_{3} & T_{4}
        \end{array}
    \right]
\]

\noindent
Such that ... 

\[
    \left[
        \begin{array}{cc}
            1 & 1 \\
            2 & 3
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            2 & 1 \\
            3 & 5
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            3 & 2 \\
            5 & 8
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            5 & 3 \\
            8 & 13
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            8  & 5 \\
            13 & 21
        \end{array}
    \right] \to \text{like clockwork}
\]

\noindent
Another way of explaining it is ...

\[
    \begin{array}{c}
        T_{1}+T_{2}=T_{3} \\
        T_{2}+T_{3}=T_{4}
    \end{array}
\]

%2-----------------21------------------41------------------61------------------81%
\noindent
From one generation \(\to\) to the next rotates the boxed variables clockwise. \
When a generation is used to create a Pythagorean Triple from the terms, the \
effective \textbf{Sum and Difference of Squares} is read counter-clockwise. \\
\\

\noindent
Again, this is a personal mnemonic representation, absorbed from Price's 2008 \
paper. However, the reader may find a different representation more useful. \
\textit{(I.e. You might prefer the clockwise and counter-clockwise term \
rotation be inverted ... the same observations hold; to each their own.)}

%\noindent\hrulefill \\
%\\

\newpage
\noindent
\textbf{Sum and Difference of Squares} \\

%2-----------------21------------------41------------------61------------------81%
\noindent
The \textbf{Sum and Difference of Squares} is another \textit{(personal)} \
mnemonic representation, learned before encountering the Fibonacci Box concept. \
\textit{(This is to say, the Fibonacci Box is a nice visio-spatial representation \
of sum/diff squares algorithmic components.)}

\[
    \left[
        \begin{array}{ c | c }
            T_{2} & T_{1} \\ \hline 
            T_{3} & T_{4} \\
        \end{array}
    \right]
    \equiv
    \left[
        \begin{array}{ c | c }
            D & F_{1} \\ \hline
            C & F_{2}
        \end{array}
    \right]
\]

\[
    \begin{array}{rcl} \hline
        & \text{Relations} & \\ \hline
        T_{1} & \to & F_{1} \\
        T_{2} & \to & D \\
        T_{3} & \to & C \\
        T_{4} & \to & F_{2} \\ \hline
        (F_{1}, D, C, F_{2}) & & \text{Fibonacci Box} \\ \hline
        F_{1} & \to & \texttt{Factor}_{1} \\
        D & \to & \texttt{Difference} \\
        C & \to & \texttt{Center} \\
        F_{2} & \to & \texttt{Factor}_{2} \\
        F_{1} & < & F_{2} \\
        D & < & C \\
        1 & \equiv & F_{1} \bmod 2 \equiv F_{2} \bmod 2 \\
        D \bmod 2 & \ne & C \bmod 2 \\ \hline
        F_{1} & = & C-D      \\
        F_{1} & = & F_{2}-2D \\
        F_{2} & = & C+D      \\
        F_{2} & = & F_{1}+2D \\ 
        C & = & F_{1}+D        \\
        D & = & F_{2}-C        \\
        & & \\ \hline
        (a, b, c) & & \text{Pythagorean Triple} \\ \hline
        a & = & C^2-D^2 \\
        a & = & F_{1}*F_{2} \\
        b & = & 2CD \\
        c & = & C^2+D^2 \\
        c & = & (D*F_{2})+(C*F_{1}) \\
        c & = & (C*F_{2})-(D*F_{1}) \\
    \end{array}
\]

\noindent
In regards to \(c\), as the \textbf{Sum of Squares}, such that \
\(c \equiv 1 \bmod 4\) \ldots{} See Also: the Brahmagupta-Fibonacci Identity \
\cite{WikiPedia:BragFibId}. \textit{(However, note, the multiplicative identity \
may not always hold when the two multiples either \(\pm\) have many composites \
instead of singular primes or many multiplicative values.)}

\newpage

\noindent
\textbf{Transitions \textit{(or, Lambdas)}} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
Previous examples showed that there is a transition, or lambda, from one state \
\(\to\) to the next. This example was of the sequence of Fibonacci Numbers.

\[
    \left[
        \begin{array}{cc}
            1 & 1 \\
            2 & 3
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            2 & 1 \\
            3 & 5
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            3 & 2 \\
            5 & 8
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            5 & 3 \\
            8 & 13
        \end{array}
    \right] \to
    \left[
        \begin{array}{cc}
            8  & 5 \\
            13 & 21
        \end{array}
    \right] \to \text{like clockwork}
\]

\noindent
However, there are different state transitions, depending on Pythagorean Tree, \
and algorithms to build the trees \textit{(Price, or Classic)}. \\
\\

\noindent
In \textit{(ternary)} Pythagorean Trees, each parent node has 3 children, or \
branches, \(A\), \(B\) and \(C\). Each of these must have a different algorithmic \
state transition. \\
\\

\noindent
\hrulefill
\\

\noindent
\textbf{Classic Tree: A Branch} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
The Classic tree \textit{(B.Berggren, 1934)} \textbf{A} branch shifts the \
\(F_{2}\) value to the \(F_{1}\) position, maintaining \(D\), and re-calculating \
\(C\) and \(F_{2}\). As such ...

\[
    \begin{array}{ccccccccccc}
        \left[
            \begin{array}{cc}
                D & F_{1} \\
                C & F_{2}
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 1 \\
                2 & 3
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 3 \\
                4 & 5
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 5 \\
                6 & 7
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 7 \\
                8 & 9
            \end{array}
        \right] & \to & ... \\
        (a, b, c) & & 
        (3, 4, 5) & & 
        (15, 8, 17) & &
        (35, 12, 37) & &
        (63, 16, 65) & & \\
    \end{array}
\]

\noindent
Notice that \(D=1\) is a constnant value throughout the repeating transition. \
And, all members of the \textbf{Classic A branch} are of Form 2, such that \
\(c=a+2\). \\
\\

\newpage

\noindent
\textbf{Classic Tree: B Branch} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
The Classic tree \textit{(B.Berggren, 1934)} \textbf{B} branch shifts the \
\(F_{2}\) value to the \(F_{1}\) position, and the \(C\) value to the \(D\) \
position, then re-calculate \(C\) and \(F_{2}\). As such ...

\[
    \begin{array}{ccccccccccc}
        \left[
            \begin{array}{cc}
                D & F_{1} \\
                C & F_{2}
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 1 \\
                2 & 3
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                2 & 3 \\
                5 & 7
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                5  & 7 \\
                12 & 17
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                12 & 17 \\
                29 & 41
            \end{array}
        \right] & \to & ... \\
        (a, b, c) & & 
        (3, 4, 5) & & 
        (21, 20, 29) & &
        (119, 120, 169) & &
        (697, 696, 985) & & \\
    \end{array}
\]

\noindent
Notice that all members of the \textbf{Classic B branch} are of Form 3, such that \
\(b=a \pm 1\). If there are \(3^{Odd}\) members across a child branch, \(b=a-1\), \
and if there are \(3^{Even}\) members across a child branch, \(b=a+1\). \\
\\

\noindent
\hrulefill
\\

\noindent
\textbf{Classic Tree: C Branch} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
The Classic tree \textit{(B.Berggren, 1934)} \textbf{C} branch shifts the \
\(C\) value to the \(D\) position, then re-calculate \(C\) and \(F_{2}\). \
As such ...

\[
    \begin{array}{ccccccccccc}
        \left[
            \begin{array}{cc}
                D & F_{1} \\
                C & F_{2}
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 1 \\
                2 & 3
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                2 & 1 \\
                3 & 5
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                3 & 1 \\
                4 & 7
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                4 & 1 \\
                5 & 9
            \end{array}
        \right] & \to & ... \\
        (a, b, c) & & 
        (3, 4, 5) & & 
        (5, 12, 13) & &
        (7, 24, 25) & &
        (9, 40, 41) & & \\
    \end{array}
\]

\noindent
Notice that \(F_{1}=1\) is a constnant value throughout the repeating \
transition. And, all members of the \textbf{Classic C branch} are of Form 1, \
such that \(c=b+1\). Also take note that after \(a_{0}=3\), the \(a_{n}\) \
values are sequential \texttt{Odd} numbers; \texttt{3, 5, 7, 9, 11, 13, ...} \\
\\

\newpage

\noindent
\textbf{Price Tree: A Branch} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
The Price tree \textit{(Price, H. Lee, 2008)} \textbf{A} branch shifts the \
\(F_{2}\) value to the \(C\) position, then re-calculate \(D\) and \(F_{2}\). \
As such ...

\[
    \begin{array}{ccccccccccc}
        \left[
            \begin{array}{cc}
                D & F_{1} \\
                C & F_{2}
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 1 \\
                2 & 3
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                2 & 1 \\
                3 & 5
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                4 & 1 \\
                5 & 9
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                8 & 1 \\
                9 & 17
            \end{array}
        \right] & \to & ... \\
        (a, b, c) & & 
        (3, 4, 5) & & 
        (5, 12, 13) & &
        (9, 40, 41) & &
        (17, 144, 145) & & \\
    \end{array}
\]

%2-----------------21------------------41------------------61------------------81%
\noindent
Notice that \(F_{1}=1\) is a constnant value throughout the repeating \
transition. And, all members of the \textbf{Price A branch} are of Form 1, \
such that \(c=b+1\). \(D\) is always Even, and \(C\) is always odd, and \
\(C-D=1\). \\
\\

\noindent
\hrulefill
\\

\noindent
\textbf{Price Tree: C Branch} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
The Price tree \textit{(Price, H. Lee, 2008)} \textbf{C} branch shifts the \
\(F_{2}\) value to the \(D\) position, then re-calculate \(C\) and \(F_{2}\). \
As such ...

\[
    \begin{array}{ccccccccccc}
        \left[
            \begin{array}{cc}
                D & F_{1} \\
                C & F_{2}
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 1 \\
                2 & 3
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                3 & 1 \\
                4 & 7
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                7 & 1 \\
                8 & 15
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                15 & 1 \\
                16 & 31
            \end{array}
        \right] & \to & ... \\
        (a, b, c) & & 
        (3, 4, 5) & & 
        (7, 24, 25) & &
        (15, 112, 113) & &
        (31, 480, 481) & & \\
    \end{array}
\]

%2-----------------21------------------41------------------61------------------81%
\noindent
Notice that \(F_{1}=1\) is a constnant value throughout the repeating \
transition. And, all members of the \textbf{Price C branch} are of Form 1, the \
same as the \textbf{Price A branch}, such that \(c=b+1\). \(D\) is always Odd, \
and \(C\) is always Even, and \(C-D=1\). This represents the same as the \
\textbf{Classic C branch}, however, distributed across the \textbf{Price tree} \
in a binary fashion; \textbf{A-C branching}.
\\

\newpage

\noindent
\textbf{Price Tree: B Branch} \\
\\

%2-----------------21------------------41------------------61------------------81%
\noindent
The Price tree \textit{(Price, H. Lee, 2008)} \textbf{B} branch shifts the \
\(F_{1}\) value to the \(D\) position, and the \(F_{2}\) value to the \(F_{1}\) \
position, then re-calculate \(C\) and \(F_{2}\). \
As such ...

\[
    \begin{array}{ccccccccccc}
        \left[
            \begin{array}{cc}
                D & F_{1} \\
                C & F_{2}
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 1 \\
                2 & 3
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                1 & 3 \\
                4 & 5
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                3 & 5 \\
                8 & 11
            \end{array}
        \right] & \to &
        \left[
            \begin{array}{cc}
                5  & 11 \\
                16 & 21
            \end{array}
        \right] & \to & ... \\
        (a, b, c) & & 
        (3, 4, 5) & & 
        (15, 8, 17) & &
        (55, 48, 73) & &
        (231, 160, 281) & & \\
    \end{array}
\]

\newpage
    % Bibliography
    \printbibliography
    \addcontentsline{toc}{section}{Bibliography}

\newpage

\noindent
\textbf{Other References} \\
\\

\noindent
The author of this document has not had a chance to review the original source \
for the \textbf{Classic tree: B.Berggren, (1934)}. Thus, it is not listed in \
references, though cited with in-line observations. The various WikiPedia \
articles dealing with Pythagorean Triples are the source of this information, \
and the reference point; however, it originally comes from this unchecked \
reference. In addition, the Price paper \textit{(2008)} refers to the ``Classic" \
tree as ``Barning-Hall" -- however, it should be referred to as ``Classic." \\
\\

\end{document}



