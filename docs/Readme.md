
# Documentation Readme

Miscelaneous documentation is collected here as the package is formalized and built-out.

See the [Index](./Index.md)

---

## License

Copyleft, GNU Doc License v1.3, going _hand-in-hand_ with the code being GNU GPL v3.

[License Link](./License.md)

---

### Notes: Building LaTeX to PDF

The `.tex` files are `LaTeX`, and usually rely on a `.bib` file _(bibliography)_. And the aptly named `.pdf` files map back to the respective `.tex` files _(so named)_.

To build the `.tex` files into `.pdf`, I'm using the following:

```bash
$> latexmk -gg --quiet -bibtex -pdf {filename}.tex
```

The bibliography back-end is `Biber`; this is often a separate package; i.e. for Debian, I do the following:

```bash
$> sudo apt-get install tex-live latexmk biber
```

You may have to install other packages _(depending on your nix distro)_ to build the PDF docs.

---

### Notes: ToDo: Whatever else ...

Placeholder ...

