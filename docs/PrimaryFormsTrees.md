
# Pythagorean Triples: Primary Forms, Trees

There is plenty of information out there on Pythagorean Triples. Typically the best place to start is a WikiPedia article ...

* https://en.wikipedia.org/wiki/Pythagorean_triple
* https://en.wikipedia.org/wiki/Tree_of_primitive_Pythagorean_triples
* https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples

The first is a long article, the other two are technical; and overall it is a broad topic, relating to many other numeric and computational domains. _(Or, in other words, a core topic in number theory, if not computation.)_

---

### 3 Primary Forms

* `Form 1: (a, b, c==b+1)`
* `Form 2: (a, b, c==a+2)`
* `Form 3: (a, b==a+1, c) for 3^n, 0 ≡ n mod 2 {or} n%2==0`
* `Form 3: (a, b==a-1, c) for 3^n, 1 ≡ n mod 2 {or} n%2==1`

Notice that the 3rd form is split into 2 parts. Where `b == a ± 1` depends on the level or tier of the _(Classic)_ ternary tree.

---

### 3 Branches

Each Pythagorean Triple can be a parent of 3 additional Pythagorean Triples. Both trees (below) are ternary trees, such that each Parent Node has 3 Child Nodes. From Left-to-Right ...

* `A` &mdash; Left Child Node
* `B` &mdash; Middle Child Node
* `C` &mdash; Right Child Node

And, `P` will designate a Parent Node (having all of `A`, `B`, and `C` children).

Take note: Ternary Trees have a Base-3 representation `[0, 1, 2]`.

---

### 2 Primary Trees

* `Classic -- B.Berggren    (1934)`
* `Price   -- Price, H. Lee (2008)`

The two (2) trees have different orderings of the previous forms. Their respective `A`, `B`, and `C` branchings are formed differently.

All of `Form 1` constitute the `C` branch in the Classic Tree. Whereas, in Price, these values are distributed across the rows.

All of `Form 2` constitute the `A` branch in the Classic Tree. Whereas, in Price, these values are distributed between `A` and `C` child nodes as the `B`-child of Parent-`P`.

All of `Form 3` constitute the `B` branch in the Classic Tree. Whereas, in Price, their distribution is seemingly random.

> _Technically speaking, you could change these around. This organization makes the most sense to the author. Another user might have different preferences for organization of the algorithms behind ordering and interpretation._

The Price Tree has a binary organization. This might be better termed pseudo-binary organization. Left-to-Right, the `A` and `C` branches from the root, and Parent Nodes form counters, such that in the 2rd tier, or level _(below root)_, such that if you have 3<sup>2</sup> entries _(left-to-right)_, the Price tree starts with ...

`9 B 11 {3xB} 13 B 15`

... this is to say if you have 3<sup>2</sup> entries, then you have 2<sup>2</sup> sequential _(predictable position)_ odd numbers _left-to-right_, starting from `(2^3)+1==9`, and continuing to `(2^4)-1==15`. And the next level looks like this ...

`17 B 19 {3xB} 21 B 23 {9xB} 25 B 27 {3xB} 29 B 31`

Which has 3<sup>3</sup> entries, and started at `(2^4)+1==17`, with 2<sup>3</sup> sequential odd numbers `+2`, and ends at `(2^5)-1==31`.

Most importantly, distributed across the tree, opposed to the pure `C*` branch in the Classic Tree.

There are different uses for the two trees, respectively. This is just a brief overview of observations. _(Many other interesting connections and patterns. Thus the reason for `PyTripTk`)_

Note that in most instances _(not all)_, the Classic tree grows more rapidly than the Price tree. The `0{ABC}*` string representation of Price is often shorter than the Classic tree. And, entries in the Price tree that have random distribution of `0{ABC}*` are often represented by long strings of `A*`, `B*`, and `C*` strings in the Classic tree. &mdash; It depends purely upon the Pythagorean triple; and forms thereof.

---

## Summary

* Form 1 maps to the `C*` branch in the Classic tree.
* Form 2 maps to the `A*` branch in the Classic tree.
* Form 3 maps to the `B*` branch in the Classic tree.
* Form 1 and 2 are distributed across, _left-to-right_ in the Price tree.
* Form 3 is distributed randomly in the Price tree.
* Any number with a pure `A*C*` branching in the Price tree is a literal BINARY representation of the `a` valued number in `a^2+b^2=c^2`, and maps back to the Classic `C*` branch.
* Any number starting with `A*C*` and ending with `B` in the price tree is of Form 2, and maps back to the Classic `A*` branch.




