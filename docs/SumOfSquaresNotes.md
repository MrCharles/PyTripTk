
# Minor Note on SUM Of Squares

The _**SUM** of Squares_ is sometimes called Congruum. Not to be confused with statistical methods, this is a number theory concept _(core maths)_.

* [Congruum &mdash; WikiPedia](https://en.wikipedia.org/wiki/Congruum) &mdash; _"the difference between successive square numbers in an arithmetic progression of three squares"_
* [Sum of two squares theorem &mdash; WikiPedia](https://en.wikipedia.org/wiki/Sum_of_two_squares_theorem)
    * Both of the **See also** articles are useful concepts...
    * [Brahmagupta–Fibonacci identity](https://en.wikipedia.org/wiki/Brahmagupta%E2%80%93Fibonacci_identity)
    * [Landau–Ramanujan constant](https://en.wikipedia.org/wiki/Landau%E2%80%93Ramanujan_constant)

This is somewhat important for identifying which numbers can be `c` in a Pythagorean Triple `(a, b, c)`.

There is a relatively efficient [SageMath](https://www.sagemath.org/) script, developed by [William Stein](https://wstein.org/) _(of SageMath, a core developer and maths professor)_.

* [Sums of Two Squares](https://wstein.org/edu/2007/spring/ent/ent-html/node75.html)

```python
sage: def sum_of_two_squares(p):
...    p = Integer(p)
...    assert p%4 == 1, "p must be 1 modulo 4"
...    r = Mod(-1,p).sqrt().lift()
...    v = continued_fraction(-r/p)
...    n = floor(sqrt(p))
...    for x in v.convergents():
...        c = r*x.denominator() + p*x.numerator()
...        if -n <= c and c <= n:
...            return (abs(x.denominator()),abs(c))
```

