
# PyTripTk Docs Index

### Markdown Docs

* [Motivation &mdash; Skip this](./Motivation.md)
* [Primary Forms, Trees, Branches](./PrimaryFormsTrees.md)
* [Note: Sum of Squares](./SumOfSquaresNotes.md)
* ... more to come ...

### PDF Docs

* [Form 1 Observations](./Form1.pdf) _(pointless placeholder)_
* [Form 2 Observations](./Form2.pdf) _(pointless placeholder)_
* [Form 3 Observations](./Form3.pdf)
* [Fibonacci Boxes](./FibBox.pdf)
* [Euler Bricks](./EulerBricks.pdf)
* [FibBox j, k, i Modulo Identities](./FibBox_jki_modulo.pdf)
* ... more to come ...

---

If something is not yet here, and you'd like the package author's observations documented, ask and ye shall receive. _(**WIP**: Work-In-Progress can be prioritized upon request.)_


