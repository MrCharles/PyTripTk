
# Motivation

If it is not obvious from the code and the direction I'm going, one of my personal hobbies or interests is [Integer Factorization](https://en.wikipedia.org/wiki/Integer_factorization).

It's not like I'm going to change the world with my minimal math skills &mdash; [Quantum Computing](https://en.wikipedia.org/wiki/Quantum_algorithm) and [Shor's Algorithm](https://en.wikipedia.org/wiki/Shor%27s_algorithm) will come long before I contribute anything significant. _(For that matter, I'll probably be forever sleep by the time such problems are solved.)_

In any case, this little package is because I'm interested in variations on a theme of Integer Factorization.

There are a lot of interesting observations on the nature of [Pythagorean Triples](https://en.wikipedia.org/wiki/Pythagorean_triple) _(which are ternary trees)_, including [Sum](https://en.wikipedia.org/wiki/Sum_of_two_squares_theorem) and [Difference of Squares](https://en.wikipedia.org/wiki/Difference_of_two_squares) _(or, Fibonacci Boxes)_, and [Arithmetic Progression of Squares](https://en.wikipedia.org/wiki/Congruum).

---

### Secondary

Sometimes I feel like writing _(this)_. Sometimes I feel like coding. This little space solves both those problems. Sometimes I dig into documenting my thoughts _(formally)_, and sometimes I write code _(hopefully clean code for publication)_.

---

### Tertiary

I have a very bad habit of writing something, and then re-writing it from scratch. I've started a GitLab account in hopes of focusing my personal interests. _(I've been a programmer since long before it was trendy and became the next mini-mart cashier job; but, alas, I'm just a lowly code janitor in my day-to-day job; not everybody can be a big-name nobody; someone needs to clean and scrub the toilets and know not to mix cholorine and bleach.)_

Once I can write something from scratch, I'm happy to re-format my computer, not re-load everything I've backed-up before doing so _(digital pack-rat)_, and then ... well ... start from scratch again _(because often queued from memory is faster than digging in the dirt to find the shovel I misplaced but can replace with a trip to the store; in this case, my store is the internet)_.

Measures of inefficiency, and measures of learning and growth.

Or, in simpler terms, I'm tried of digging in my personal archives for something I've done &mdash; I'm going to publish things I'm working on and see if this makes a difference in terms of retreival of older assets &mdash; given almost everything I do is online, including researching code, algorithms, etc.

> _It'll be a funny day ... the day I'm doing some research and run across something I've done before because it's published, and I'll be like: "Oh, yeah, okay, now I remember..."_

Just a small bit of humor to amuse myself at some future point.

