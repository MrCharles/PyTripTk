
## Additional, or Advanced Usage, part 2

Following the [Readme](./README.md), and [Part 1](./AAUsage.md), this section will focus on one particular import, `PyTripTk.PyTripExt`

Opposed to the `PyTripTk.PyTrip.PyTrip` namedtuple _(core, or basis)_, the `PyTripExt` class brings all of the previous components together.

```python
>>> import PyTripTk.PyTrip as pt
>>> import PyTripTk.PyTripExt as ext
>>> var = pte.PyTripExt(pt.ROOT)
>>> var
<PyTripTk.PyTripExt.PyTripExt object at 0x7ff5d1e6bb70>
>>> var.PyTrip
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> _
```

`PyTripExt` **has the following methods and aliases:**

* `EulerBrick` or `EB` <br />&mdash; Euler Brick
* `FibBox` or `FB` <br />&mdash; Fibonacci Box
* `APS` <br />&mdash; Arithmetic Progression of Squares
* `PriceParent` or `PriceP` <br />&mdash; Generates the parent object, as a `PyTripExt` object. _(uses `PrevGen`)_
* `PricePBranch` <br />&mdash; Gets which branch this is from the Price parent `PyTripExt` object. _(`A`, `B`, or `C`; uses `PrevGen`)_
* `PriceABranch` or `PriceA` <br />&mdash; Generates the A branch child in the Price tree, as a `PyTripExt` object. _(uses `NextGen`)_
* `PriceBBranch` or `PriceB` <br />&mdash; Generates the B branch child in the Price tree, as a `PyTripExt` object. _(uses `NextGen`)_
* `PriceCBranch` or `PriceC` <br />&mdash; Generates the C branch child in the Price tree, as a `PyTripExt` object. _(uses `NextGen`)_
* `PriceString` or `PriceS` <br />&mdash; Generates the `0{ABC}*` string representation of the PT in the Price Tree. _(uses `ReduceRegen`)_
* `ClassicParent` or `ClassicP` <br />&mdash; Generates the parent object, as a `PyTripExt` object. _(uses `PrevGen`)_
* `ClassicPBranch` <br />&mdash; Gets which branch this is from the Classic parent `PyTripExt` object. _(`A`, `B`, or `C`; uses `PrevGen`)_
* `ClassicABranch` or `ClassicA` <br />&mdash; Generates the A branch child in the Classic tree, as a `PyTripExt` object. _(uses `NextGen`)_
* `ClassicBBranch` or `ClassicB` <br />&mdash; Generates the B branch child in the Classic tree, as a `PyTripExt` object. _(uses `NextGen`)_
* `ClassicCBranch` or `ClassicC` <br />&mdash; Generates the C branch child in the Classic tree, as a `PyTripExt` object. _(uses `NextGen`)_
* `ClassicString` or `ClassicS` <br />&mdash; Generates the `0{ABC}*` string representation of the PT in the Classic Tree. _(uses `ReduceRegen`)_

Each of these has a not-yet-assigned private variable. Upon requesting, the value(s) are generated; only once. Thereafter, they're stored _(not regenerated on each call)_. The same holds for the tree hierarchy, but, in addition, memory is managed a little bit, much like a pure `C`-struct.

**Point being...**

```python
>>> var.ClassicA.ClassicB.ClassicC.ClassicA
<PyTripTk.PyTripExt.PyTripExt object at 0x7ff5d0dee7b8>
>>> var.ClassicA.ClassicB.ClassicC.ClassicA.PyTrip
PyTrip(a=mpz(943), b=mpz(576), c=mpz(1105))
>>> hex(id(var.ClassicA.ClassicB.ClassicC))
'0x7ff5d0dee780'
>>> hex(id(var.ClassicA.ClassicB.ClassicC.ClassicA.ClassicP))
'0x7ff5d0dee780'
>>> hex(id(var.ClassicA.ClassicB.ClassicC.ClassicA.ClassicB.ClassicP))
'0x7ff5d0dee7b8'
>>> hex(id(var.ClassicA.ClassicB.ClassicC.ClassicA))
'0x7ff5d0dee7b8'
```

In simpler terms, opposed to generating each `PyTrip` and/or `PyTripExt` manually, and needing to manage the tree ... the tree will build itself after calling the various methods against a member of the tree. Thus, if you want to operate from the next generation ...

```python
>>> hex(id(var))
'0x7fb0532cecc0'
>>> var = var.PriceA
>>> hex(id(var))
'0x7fb052253400'
>>> hex(id(var.PriceP))
'0x7fb0532cecc0'
>>> hex(id(var.PriceP.PriceA))
'0x7fb052253400'
>>> hex(id(var))
'0x7fb052253400'
```

So, you can crawl the tree the same way as you might run over a `C`-struct. _(So to speak; or at least this is the design intent not to eat a ton of memory or have to manage this manually.)_

Where the latter example says `var = var.PriceA` &mdash; it generates the A-branch for in the Price tree, if not yet done, then var is assigned to that object. That object has tracked which object created it _(the parent object)_, and has forward-and-backward memory of the tree structure.

---

**Otherwise ...**

```python
>>> var._eb
>>> print("Should be nothing, none, no output.")
>>> var.EB
EulerBrick(a=mpz(117), b=mpz(44), c=mpz(240), d=mpz(125), e=mpz(267), f=mpz(244), abd=PyTrip(a=mpz(117), b=mpz(44), c=mpz(125)), ace=PyTrip(a=mpz(39), b=mpz(80), c=mpz(89)), bcf=PyTrip(a=mpz(11), b=mpz(60), c=mpz(61)), uvw=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
>>> var._eb
EulerBrick(a=mpz(117), b=mpz(44), c=mpz(240), d=mpz(125), e=mpz(267), f=mpz(244), abd=PyTrip(a=mpz(117), b=mpz(44), c=mpz(125)), ace=PyTrip(a=mpz(39), b=mpz(80), c=mpz(89)), bcf=PyTrip(a=mpz(11), b=mpz(60), c=mpz(61)), uvw=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
>>> print("Because the EulerBrick has now been generated.")
```

```python
>>> var.FB
FibBox(F1=mpz(1), D=mpz(1), C=mpz(2), F2=mpz(3), abc=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
```

```python
>>> var.APS
PyTripAPS(p=mpz(1), q=mpz(5), r=mpz(7), abc=PyTrip(a=mpz(3), b=mpz(4), c=mpz(5)))
```

```python
>>> var.PyTrip
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> var.ClassicS
'0'
>>> var = var.ClassicA.ClassicB.ClassicC.ClassicA
>>> var.ClassicS
'0ABCA'
```

---

### ToDo ...

* In the future, connecting two branches is planned.
* Many other things are planned, you can see the code for some research notes.

---

### Links

* [Home / Readme (Basics)](./README.md)
* [Advanced Usage, pt 1](./AAUsage.md)

