#!/usr/bin/env python3

'''
Reduce and Regenerate Pythagorean Triples to/from strings.

This file contains the following:
    Regen_Price           -- (def, tuple) -- Regenerates a PT from a string.
    Regen2_Price          -- (def, tuple) -- Regenerates a PT from a string and starting PT.
    Reduce_Price          -- (def, tuple) -- Reduces a PT to a string of A, B, C characters.
    Regen_Classic         -- (def, tuple) -- Regenerates a PT from a string.
    Regen2_Classic        -- (def, tuple) -- Regenerates a PT from a string and starting PT.
    Reduce_Classic        -- (def, str)   -- Reduces a PT to a string of A, B, C characters.
    PyTripRegenError      -- (def, None)  -- Formats the Regeneration Exception Message.
    PyTripReduceError     -- (def, None)  -- Formats the Reduction Exception Message.
    PyTripRegenException  -- (exception)  -- Exception to be called when regeneration fails.
    PyTripReduceException -- (exception)  -- Exception to be called when reduction fails.

'''

# 3rd Party
import gmpy2 as gmp

# Local (package)
import PyTripTk.PyTrip as pt
import PyTripTk.NextGen as ng
import PyTripTk.PrevGen as pg

class PyTripReduceException(pt.PyTripException):
    '''A specialized PyTripException for reduction.
    '''
    pass

class PyTripRegenException(pt.PyTripException):
    '''A specialized PyTripException for regeneration.
    '''
    pass

def PyTripReduceError(pytrip, tree_type, itr_var, sout_var):
    '''Formats the error message if reducing a tree to a string,
    fails. Raises `PyTripReduceException` with the message.
    '''
    msg = "Error reducing Pythagorean Triple\n"
    msg += "Input: {}\n".format(repr(pytrip))
    msg += "Tree: {}\n".format(tree_type)
    msg += "State: {}\n".format(repr(itr_var))
    msg += "State: {}\n".format(sout_var)
    raise PyTripReduceException(msg)

def PyTripRegenError(imsg, c, s):
    msg = imsg[::]
    msg += " '{}' at {} in {}".format(c, s.index(c), s)
    raise PyTripRegenException(msg)

def Reduce_Price(pytrip):
    '''Attempts to reduce a Pythagorean Triple, using the Price tree,
    to a string of "A" "B" and "C" characters (effective Base-3).
    '''
    assert isinstance(pytrip, pt.PyTrip)
    assert pt.IsPrimitivePyTrip(pytrip)
    sout = ""
    itr = pt.MakePyTrip(pytrip[0], pytrip[1], pytrip[2])
    while itr != pt.ROOT:
        tmp = pg.PrevGen_Price_A(itr)
        if not tmp is None:
            sout += "A"
            itr = tmp
        else:
            tmp = pg.PrevGen_Price_B(itr)
            if not tmp is None:
                sout += "B"
                itr = tmp
            else:
                tmp = pg.PrevGen_Price_C(itr)
                if not tmp is None:
                    sout += "C"
                    itr = tmp
                else:
                    PyTripReduceError(pytrip, "Price", itr, sout)
    assert itr == pt.ROOT
    sout += "0"
    return sout[::-1]

def Reduce_Classic(pytrip):
    '''Attempts to reduce a Pythagorean Triple, using the Classic tree,
    to a string of "A" "B" and "C" characters (effective Base-3).
    '''
    assert isinstance(pytrip, pt.PyTrip)
    assert pt.IsPrimitivePyTrip(pytrip)
    sout = ""
    itr = pt.MakePyTrip(pytrip[0], pytrip[1], pytrip[2])
    while itr != pt.ROOT:
        tmp = pg.PrevGen_Classic_A(itr)
        if not tmp is None:
            sout += "A"
            itr = tmp
        else:
            tmp = pg.PrevGen_Classic_B(itr)
            if not tmp is None:
                sout += "B"
                itr = tmp
            else:
                tmp = pg.PrevGen_Classic_C(itr)
                if not tmp is None:
                    sout += "C"
                    itr = tmp
                else:
                    PyTripReduceError(pytrip, "Classic", itr, sout)
    assert itr == pt.ROOT
    sout += "0"
    return sout[::-1]

def Regen_Price(abcstr):
    '''Regenerates a string of "A" "B" and "C" characters
    into a Pythagorean Triple, using the Price tree.
    (Must start with root "0".)
    '''
    assert isinstance(abcstr, str)
    assert abcstr[0] == '0'
    pytrip = pt.MakePyTrip(3, 4, 5)
    tmpstr = abcstr[1:]
    while len(tmpstr) > 0:
        if tmpstr[0] in ['a', 'A']:
            pytrip = ng.NextGen_Price_A(pytrip)
        elif tmpstr[0] in ['b', 'B']:
            pytrip = ng.NextGen_Price_B(pytrip)
        elif tmpstr[0] in ['c', 'C']:
            pytrip = ng.NextGen_Price_C(pytrip)
        else:
            PyTripRegenError("Invalid character", tmpstr[0], abcstr)
        if len(tmpstr) == 1:
            break
        else:
            tmpstr = tmpstr[1:]
    return pytrip

def Regen_Price2(pytrip, abcstr):
    '''Regenerates a string of "A" "B" and "C" characters
    into a Pythagorean Triple, using the Price tree,
    starting with a given Pythagorean Triple.

    The input pythagorean triple does not need to be valid!

    '''
    assert isinstance(pytrip, pt.PyTrip)
    assert pt.IsPrimitivePyTrip(pytrip)
    assert isinstance(abcstr, str)
    assert not abcstr[0] == '0'
    ptout = pt.MakePyTrip(pytrip[0], pytrip[1], pytrip[2])
    tmpstr = abcstr[::]
    while len(tmpstr) > 0:
        if tmpstr[0] in ['a', 'A']:
            ptout = ng.NextGen_Price_A(ptout)
        elif tmpstr[0] in ['b', 'B']:
            ptout = ng.NextGen_Price_B(ptout)
        elif tmpstr[0] in ['c', 'C']:
            ptout = ng.NextGen_Price_C(ptout)
        else:
            PyTripRegenError("Invalid character", tmpstr[0], abcstr)
        if len(tmpstr) == 1:
            break
        else:
            tmpstr = tmpstr[1:]
    return ptout

def Regen_Classic(abcstr):
    '''Regenerates a string of "A" "B" and "C" characters
    into a Pythagorean Triple, using the Classic tree.
    (Must start with root "0".)
    '''
    assert isinstance(abcstr, str)
    assert abcstr[0] == '0'
    pytrip = pt.MakePyTrip(3, 4, 5)
    tmpstr = abcstr[1:]
    while len(tmpstr) > 0:
        if tmpstr[0] in ['a', 'A']:
            pytrip = ng.NextGen_Classic_A(pytrip)
        elif tmpstr[0] in ['b', 'B']:
            pytrip = ng.NextGen_Classic_B(pytrip)
        elif tmpstr[0] in ['c', 'C']:
            pytrip = ng.NextGen_Classic_C(pytrip)
        else:
            PyTripRegenError("Invalid character", tmpstr[0], abcstr)
        if len(tmpstr) == 1:
            break
        else:
            tmpstr = tmpstr[1:]
    return pytrip

def Regen_Classic2(pytrip, abcstr):
    '''Regenerates a string of "A" "B" and "C" characters
    into a Pythagorean Triple, using the Classic tree,
    starting with a given Pythagorean Triple.

    The input pythagorean triple does not need to be valid!

    '''
    assert isinstance(pytrip, pt.PyTrip)
    assert pt.IsPrimitivePyTrip(pytrip)
    assert isinstance(abcstr, str)
    assert not abcstr[0] == '0'
    ptout = pt.MakePyTrip(pytrip[0], pytrip[1], pytrip[2])
    tmpstr = abcstr[::]
    while len(tmpstr) > 0:
        if tmpstr[0] in ['a', 'A']:
            ptout = ng.NextGen_Classic_A(ptout)
        elif tmpstr[0] in ['b', 'B']:
            ptout = ng.NextGen_Classic_B(ptout)
        elif tmpstr[0] in ['c', 'C']:
            ptout = ng.NextGen_Classic_C(ptout)
        else:
            PyTripRegenError("Invalid character", tmpstr[0], abcstr)
        if len(tmpstr) == 1:
            break
        else:
            tmpstr = tmpstr[1:]
    return ptout

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



