#!/usr/bin/env python3

'''
Contains the following methods to check if a
Pythagorean Triple maches a particular form...
    IsForm1 -- (def, bool) -- Is it of the first form?
    IsForm2 -- (def, bool) -- Is it of the second form?
    IsForm3 -- (def, bool) -- Is it of the third form?

ToDo: Dynamic form detection (additional form types).

The three basic forms (1, 2, 3) are not meant to check
non-primitive forms, such that a Pythagorean Triple may
be a multiplicative member of a primary form.

'''

# 3rd party
import gmpy2 as gmp

# Local package
import PyTripTk.PyTrip as pt

def IsForm1(pytrip=None):
    '''Attempts to detect if an input PyTrip
    is of the First form.
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = pytrip[0], pytrip[1], pytrip[2]
    if c != b+1:
        return False
    n = gmp.f_div(a-1, 2)
    x, y, z = (2*n)+1, 2*n*(n+1), (2*n*(n+1))+1
    if x == a and y == b and z == c:
        return True
    return False

def IsForm2(pytrip=None):
    '''Attempts to detect if an input PyTrip
    is of the Second form.
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = pytrip[0], pytrip[1], pytrip[2]
    if c != a+2:
        return False
    n = gmp.f_div(b, 4)
    x, y, z = 4*(n**2)-1, 4*n, 4*(n**2)+1
    if x == a and y == b and z == c:
        return True
    return False

def IsForm3(pytrip=None):
    '''Attempts to detect if an input PyTrip
    is of the Third form.

    ToDo: Implement pell number algo checking (???) Maybe.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = pytrip[0], pytrip[1], pytrip[2]
    if a == b+1 or a == b-1 and a**2+b**2==c**2:
        return True
    return False

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



