#!/usr/bin/env python3

'''
FibBox is a representation of Sum and Difference of Squares,
as it pertains to factorization; but for Pythagorean Triples.

The concept of a "Fibonacci Box" was introduced to the author
in Price's (2008) paper, "The Pythagorean Tree: A New Species".

REF: https://arxiv.org/pdf/0809.4324.pdf

The values of a Pythagorean Triple can be represented any
number of ways, for example:

    If a**2+b**2==c**2, and (a%2==1, b%2==0, c%4==1), then
    F1 = C-D, where F1 is the lower of 2 factors.
    F2 = C+D, where F2 is the greater of 2 factors.
    C is the Center, and D is the Difference, such that
    F2-F1 == 2*D

    Therein,
    A == C**2-D**2 == F1*F2
    B == 2*C*D
    C == C**2+D**2 == (D*F2)+(C*F1) == (C*F2)-(D*F1)

This could be so-named, "DiffSquares" or "SumDiffSquares" --
However, the author likes the concept of a FibBox, since
it goes hand-in-hand with seeing the lambda transitions
between the individual children of parent nodes in each of
the Pythagorean Triple tenary trees.

A "FibBox" looks roughly like this...

---------
| D  F1 |
|       |
| C  F2 |
---------

... counter-clockwise, F1+D==C, and D+C==F2

Depending on which tree you're using (Price or Classic),
the members of that box shift up/down/left/right/diagnol.
(Left to the reader to work-out.) Simply put, a very nice
representation of things already known.

'''

# Python Library
from collections import namedtuple as nt

# 3rd Party
import gmpy2 as gmp

# Local Package
import PyTripTk.PyTrip as pt

FibBox=nt('FibBox', ['F1', 'D', 'C', 'F2', 'abc'])

def PT_FromFB(fb=None):
    '''Returns a Pythagorean Triple from a FibBox.

    Does not uses the `FibBox.abc` variable for calculations!

    '''
    assert isinstance(fb, FibBox)
    assert fb.F1%2 == 1 and fb.F2%2 == 1 and fb.F2 > fb.F1
    assert (fb.C+fb.D)%2 == 1 and fb.C > fb.D
    assert fb.F2 == fb.F1+2*fb.D and fb.F1 == fb.C-fb.D and fb.F2 == fb.C+fb.D
    ret = pt.PyTrip(fb.F1*fb.F2, 2*fb.C*fb.D, fb.C**2+fb.D**2)
    assert pt.IsPrimitivePyTrip(ret)
    return ret

def FB_ToPT(fb=None):
    '''Proxy method for PT_FromFB.
    '''
    return PT_FromFB(fb)

def PT_ToFB(pytrip=None):
    '''Proxy method for FB_FromPT.
    '''
    return FB_FromPT(pytrip)

def FB_FromPT(pytrip=None):
    '''Returns a FibBox for the primitive form of a Pythagorean Triple.
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    if not pt.IsPrimitivePyTrip(pytrip):
        pytrip = pt.MakePrimitive(pytrip)
    a, b, c = pytrip[0], pytrip[1], pytrip[2]
    if a%2 == 0 and b%2 == 1:
        a, b = b, a
    assert c%4 == 1
    assert c > a and (c-a)%2 == 0
    assert gmp.iroot_rem(gmp.f_div(c-a, 2), 2)[1] == 0
    Diff = gmp.iroot_rem(gmp.f_div(c-a, 2), 2)[0]
    assert gmp.iroot_rem(a+Diff**2, 2)[1] == 0
    Ctr = gmp.iroot_rem(a+Diff**2, 2)[0]
    F1, F2 = Ctr-Diff, Ctr+Diff
    assert F1%2 == 1 and F2%2 == 1
    assert (Ctr+Diff)%2 == 1
    assert F1*F2 == a and a == Ctr**2-Diff**2
    assert b == 2*Ctr*Diff
    assert c == Ctr**2+Diff**2 and c == (Diff*F2)+(Ctr*F1) and c == (Ctr*F2)-(Diff*F1)
    return FibBox(F1, Diff, Ctr, F2, pt.PyTrip(a, b, c))

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



