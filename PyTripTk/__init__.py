
if not 'gmp' in dir():
    try:
        import gmpy2 as gmp
    except ImportError as ie:
        print("\n\tPyTripTk requires GMP.\n")
        print("\n\tPyTripTk requires GMP.\n")
        print("\n\tPyTripTk requires GMP.\n")
        raise ie

if 'setprec' in dir(gmp):
    del gmp.setprec

if not 'setprec' in dir(gmp):
    try:
        import PyTripTk.util.setprec
    except ImportError as ie:
        print("\n\tCould not import setprec extension to gmp.\n")
        raise ie

if '__gmp__setprec2' in dir():
    del __gmp__setprec2

if 'mpq2' in dir(gmp):
    del gmp.mpq2

if not 'mpq2' in dir(gmp):
    try:
        import PyTripTk.util.mpq2
    except ImportError as ie:
        print("\n\tCould not import mpq2 extension to gmp.\n")
        raise ie

if '__gmp__mpq2' in dir():
    del __gmp__mpq2

if not 'is_triangular' in dir(gmp):
    try:
        import PyTripTk.util.Triangular
    except ImportError as ie:
        print("\n\tCould not import Triangular extension to gmp.\n")
        raise ie

if not 'prev_prime' in dir(gmp):
    try:
        import PyTripTk.util.previous_prime as pp
        gmp.prev_prime = pp.previous_prime
        del pp
    except ImportError as ie:
        print("\n\tCould not import previous_prime extension to gmp.\n")
        raise ie

if not 'sos' in dir(gmp):
    try:
        from PyTripTk.util.sage_stuff import __gmp__ext__sum_of_squares
        gmp.sos = __gmp__ext__sum_of_squares
        del __gmp__ext__sum_of_squares
    except ImportError as ie:
        print("\n\tCould not import sum of squares extension to gmp.\n")
        raise ie

if not 'dos' in dir(gmp):
    try:
        from PyTripTk.util.OfSquares import __gmp__ext__difference_of_squares
        gmp.dos = __gmp__ext__difference_of_squares
        del __gmp__ext__difference_of_squares
    except ImportError as ie:
        print("\n\tCould not import (prime) difference of squares extension to gmp.\n")
        raise ie

if not 'dos2' in dir(gmp):
    try:
        from PyTripTk.util.OfSquares import __gmp__ext__difference_of_squares2
        gmp.dos2 = __gmp__ext__difference_of_squares2
        del __gmp__ext__difference_of_squares2
    except ImportError as ie:
        print("\n\tCould not import (composite) difference of squares extension to gmp.\n")
        raise ie

if not 'const_silver' in dir(gmp):
    try:
        import PyTripTk.util.silver_ratio
    except ImportError as ie:
        print("\n\tCould not import silver_ratio into gmp as const_silver.\n")
        raise ie

if not 'const_golden' in dir(gmp):
    try:
        import PyTripTk.util.golden_ratio
    except ImportError as ie:
        print("\n\tCould not import golden_ratio into gmp as const_golden.\n")
        raise ie

