#!/usr/bin/env python3

'''
Extension classes for `PyTripTk.PyTrip.PyTrip` (namedtuple)

Contains the following:
    PyTripExt            -- Core Extension
    PyTripConnectTheDots -- Not Implemented (Research, WIP, ToDo)

'''

# Python Library
from enum import Enum

# 3rd Party
import gmpy2 as gmp

# Local Package
import PyTripTk.PyTrip as pt
import PyTripTk.Forms as ptf
import PyTripTk.APS as aps
import PyTripTk.FibBox as fb
import PyTripTk.EulerBrick as eb
import PyTripTk.PrevGen as pg
import PyTripTk.NextGen as ng
import PyTripTk.ReduceRegen as rr

class PyTripExt:
    '''A class holding all of the various
    interpretations of a Pythagorean Triple:

        * Fibonacci Box, (FB)
        * Euler Brick,   (EB)
        * Arithmetic Progression of Squares (APS)
        * Parent (descendent node)
        * Children (child nodes; 3)

    The only requirement is the initial input,
    an instance of `PyTripTk.PyTrip.PyTrip` is
    a valid (Primitive) Pythagorean Triple.

    When something is called, such as `EulerBrick` or `EB` it is
    generated on demand (then stored until garbage collected).

    This class is designed to operate, or act, somewhat like a
    basic C-struct, but, in pure Python. (Should or could probably
    be better or more efficiently implemented in Cython, if not
    pure C; however, that takes a bit more time, and this was
    initially built (over time) in a Python environment.

    NOTES:
        * This can eat a lot of memory.

    When in doubt:

        $> python3
        # Blah, Blah, Blah
        >>> import PyTripTk.PyTripExt as ptext
        >>> help(ptext.PyTripExt)

    '''

    # These variables (below) are global for a reason.
    # Please do not re-locate them into the __init__ method.
    # They are declared outside the __init__ method for a reason!
    # (Memory management reasons.)

    # Classic Children
    _cA, _cB, _cC = None, None, None
    # Price Children
    _pA, _pB, _pC = None, None, None
    # Parent Nodes
    _cP, _pP = None, None
    # What child of the parent nodes?
    _pbr, _cbr = None, None

    # The variables (above) are global for a reason.
    # Please do not re-locate them into the __init__ method.
    # They are declared outside the __init__ method for a reason!

    @property
    def IsRoot(self):
        if self._pt == pt.PyTrip(3, 4, 5):
            return True
        return False

    @property
    def PyTrip(self):
        '''Get the input Pythagorean Triple
        '''
        return self._pt

    @property
    def EulerBrick(self):
        '''This Pythagorean Triple as an EulerBrick.
        '''
        if self._eb is None:
            self._eb = eb.EB_FromPT(self._pt)
        return self._eb

    @property
    def EB(self):
        '''Proxy method for EulerBrick.
        '''
        return self.EulerBrick

    @property
    def FibBox(self):
        '''This Pythagorean Triple as a Fibonacci Box.
        '''
        if self._fb is None:
            self._fb = fb.FB_FromPT(self._pt)
        return self._fb

    @property
    def FB(self):
        '''Proxy method for FibBox.
        '''
        return self.FibBox

    @property
    def APS(self):
        '''This Pythagorean Triple as an Arithmethic Progression of Squares.
        '''
        if self._aps is None:
            self._aps = aps.APS_FromPT(self._pt)
        return self._aps

    @property
    def APS2(self):
        '''APS, v2
        Uses the APS class to represent the APS (versus v1, a namedtuple).
        Same as `APSC`
        '''
        if self._aps2 is None:
            self._aps2 = aps.APS(x=self.PyTrip.a, y=self.PyTrip.b, z=self.PyTrip.c)
        return self._aps2

    @property
    def APSC(self):
        '''Alias for APS2 (v2)
        '''
        return self.APS2

    @property
    def PriceA(self):
        '''Gets the child node A-branch in the Price Tree.
        '''
        # If it hasn't been generated, do so...
        if self._pA is None:
            self._pA = PyTripExt(ng.NextGen_Price_A(self._pt))
        # If the child is not tracking the parent, make it so...
        if self._pA._pP is None:
            self._pA._pP = self # This object is the parent of the child.
        return self._pA

    @property
    def PriceABranch(self):
        '''Proxy method for `self.PriceA`
        '''
        return self.PriceA

    @property
    def PriceB(self):
        '''Gets the child node B-branch in the Price Tree.
        '''
        # If it hasn't been generated, do so...
        if self._pB is None:
            self._pB = PyTripExt(ng.NextGen_Price_B(self._pt))
        # If the child is not tracking the parent, make it so...
        if self._pB._pP is None:
            self._pB._pP = self # This object is the parent of the child.
        return self._pB

    @property
    def PriceBBranch(self):
        '''Proxy method for `self.PriceB`
        '''
        return self.PriceB

    @property
    def PriceC(self):
        '''Gets the child node C-branch in the Price Tree.
        '''
        # If it hasn't been generated, do so...
        if self._pC is None:
            self._pC = PyTripExt(ng.NextGen_Price_C(self._pt))
        # If the child is not tracking the parent, make it so...
        if self._pC._pP is None:
            self._pC._pP = self # This object is the parent of the child.
        return self._pC

    @property
    def PriceCBranch(self):
        '''Proxy method for `self.PriceC`
        '''
        return self.PriceC

    @property
    def PriceParent(self):
        '''Returns the parent Pythagorean Triple in the Price Tree.
        '''
        if not self.IsRoot:
            if self._pP is None:
                self._pP = PyTripExt(pg.PrevGen_Price(self._pt))
                if self._pbr is None and not self.__norecurse:
                    self.__norecurse = True
                    tmp = self.PricePBranch
                    self.__norecurse = False
            return self._pP
        else:
            raise pt.PyTripRootException()

    @property
    def PriceP(self):
        '''Proxy method for `self.PriceParent`
        '''
        return self.PriceParent

    @property
    def IsPriceA(self):
        '''Is this a Price tree A branch?
        '''
        if not self._pbr:
            tmp = self.PricePBranch
            assert not self._pbr is None
        return self._pbr == pt.PyTripBranch.A

    @property
    def IsPriceB(self):
        '''Is this a Price tree B branch?
        '''
        if not self._pbr:
            tmp = self.PricePBranch
            assert not self._pbr is None
        return self._pbr == pt.PyTripBranch.B

    @property
    def IsPriceC(self):
        '''Is this a Price tree C branch?
        '''
        if not self._pbr:
            tmp = self.PricePBranch
            assert not self._pbr is None
        return self._pbr == pt.PyTripBranch.C

    @property
    def PricePBranch(self):
        if not self.IsRoot:
            if self._pbr is None:
                if self._pP is None and not self.__norecurse:
                    self.__norecurse = True
                    tmp = self.PriceParent
                    self.__norecurse = False
                if pg.PrevGen_IsPriceA(self._pt):
                    self._pbr = pt.PyTripBranch.A
                    if not self._pP is None:
                        if self._pP._pA is None:
                            self._pP._pA = self
                elif pg.PrevGen_IsPriceB(self._pt):
                    self._pbr = pt.PyTripBranch.B
                    if not self._pP is None:
                        if self._pP._pB is None:
                            self._pP._pB = self
                elif pg.PrevGen_IsPriceC(self._pt):
                    self._pbr = pt.PyTripBranch.C
                    if not self._pP is None:
                        if self._pP._pC is None:
                            self._pP._pC = self
                else:
                    pt.InvalidBranchException()
            return self._pbr
        else:
            raise pt.PyTripRootException()

    @property
    def PriceString(self):
        '''Gets the string representation of a node
        in the Price Primitive Pythagorean Tree.
        '''
        if self._srp is None:
            self._srp = rr.Reduce_Price(self._pt)
        return self._srp

    @property
    def PriceS(self):
        '''Proxy method for `self.PriceString`
        '''
        return self.PriceString

    @property
    def ClassicA(self):
        '''Gets the child node A-branch in the Classic Tree.
        '''
        # If it hasn't been generated, do so...
        if self._cA is None:
            self._cA = PyTripExt(ng.NextGen_Classic_A(self._pt))
        # If the child is not tracking the parent, make it so...
        if self._cA._cP is None:
            self._cA._cP = self # This object is the parent of the child.
        return self._cA

    @property
    def ClassicABranch(self):
        '''Proxy method for `self.ClassicA`
        '''
        return self.ClassicA

    @property
    def ClassicB(self):
        '''Gets the child node B-branch in the Classic Tree.
        '''
        # If it hasn't been generated, do so...
        if self._cB is None:
            self._cB = PyTripExt(ng.NextGen_Classic_B(self._pt))
        # If the child is not tracking the parent, make it so...
        if self._cB._cP is None:
            self._cB._cP = self # This object is the parent of the child.
        return self._cB

    @property
    def ClassicBBranch(self):
        '''Proxy method for `self.ClassicB`
        '''
        return self.ClassicB

    @property
    def ClassicC(self):
        '''Gets the child node C-branch in the Classic Tree.
        '''
        # If it hasn't been generated, do so...
        if self._cC is None:
            self._cC = PyTripExt(ng.NextGen_Classic_C(self._pt))
        # If the child is not tracking the parent, make it so...
        if self._cC._cP is None:
            self._cC._cP = self # This object is the parent of the child.
        return self._cC

    @property
    def ClassicCBranch(self):
        '''Proxy method for `self.ClassicC`
        '''
        return self.ClassicC

    @property
    def ClassicParent(self):
        '''Returns the parent Pythagorean Triple in the Classic Tree.
        '''
        if not self.IsRoot:
            if self._cP is None:
                self._cP = PyTripExt(pg.PrevGen_Classic(self._pt))
                if not self._cP is None and not self.__norecurse:
                    self.__norecurse = True
                    tmp = self.ClassicPBranch
                    self.__norecurse = False
            return self._cP
        else:
            raise pt.PyTripRootException()

    @property
    def ClassicP(self):
        '''Proxy method for `self.ClassicParent`
        '''
        return self.ClassicParent

    @property
    def IsClassicA(self):
        '''Is this a Classic tree A branch?
        '''
        if self._cbr is None:
            tmp = self.ClassicPBranch
            assert not self._cbr is None
        return self._cbr == pt.PyTripBranch.A

    @property
    def IsClassicB(self):
        '''Is this a Classic tree B branch?
        '''
        if self._cbr is None:
            tmp = self.ClassicPBranch
            assert not self._cbr is None
        return self._cbr == pt.PyTripBranch.B

    @property
    def IsClassicC(self):
        '''Is this a Classic tree C branch?
        '''
        if self._cbr is None:
            tmp = self.ClassicPBranch
            assert not self._cbr is None
        return self._cbr == pt.PyTripBranch.C

    @property
    def ClassicPBranch(self):
        if not self.IsRoot:
            if self._cbr is None:
                if self._cP is None and not self.__norecurse:
                    self.__norecurse = True
                    tmp = self.ClassicParent
                    self.__norecurse = False
                if pg.PrevGen_IsClassicA(self._pt):
                    self._cbr = pt.PyTripBranch.A
                    if not self._cP is None:
                        if self._cP._cA is None:
                            self._cP._cA = self
                elif pg.PrevGen_IsClassicB(self._pt):
                    self._cbr = pt.PyTripBranch.B
                    if not self._cP is None:
                        if self._cP._cB is None:
                            self._cP._cB = self
                elif pg.PrevGen_IsClassicC(self._pt):
                    self._cbr = pt.PyTripBranch.C
                    if not self._cP is None:
                        if self._cP._cC is None:
                            self._cP._cC = self
                else:
                    pt.InvalidBranchException()
            return self._cbr
        else:
            raise pt.PyTripRootException()

    @property
    def ClassicString(self):
        if self._src is None:
            self._src = rr.Reduce_Classic(self._pt)
        return self._src

    @property
    def ClassicS(self):
        '''Proxy method for `self.ClassicString`
        '''
        return self.ClassicString

    @property
    def IsSemiPrime(self):
        '''Attempts to detect if this PyTrip object
        is semiprime. (Two prime factors of [0]).
        '''
        return gmp.is_prime(self.FB.F1) and gmp.is_prime(self.FB.F2)

    @property
    def IsSSP(self):
        '''Attempts to detect if this PyTrip object
        is a Strong SemiPrime (Three prime factors of [0], [1]).

        This code is ... IN THEORY ... (instances found should be logged)!
        '''
        if self.IsSemiPrime:
            assert self.FB.C%2 != self.FB.D%2, "Bad FibBox!"
            if self.FB.C%2 == 1 and self.FB.D%4 == 0:
                if gmp.is_prime(self.FB.C):
                    return True
                elif gmp.f_div(self.FB.D, 4)%2 == 1:
                    if gmp.is_prime(gmp.f_div(self.FB.D, 4)):
                        return True
            elif self.FB.D%2 == 1 and self.FB.C%4 == 0:
                if gmp.is_prime(self.FB.D):
                    return True
                elif gmp.f_div(self.FB.C, 4)%2 == 1:
                    if gmp.is_prime(gmp.f_div(self.FB.C, 4)):
                        return True
            #else:
            #    raise pt.PyTripRootException("Bad FibBox!")
        return False

    @property
    def IsVSSP(self):
        '''Attempts to detect if this PyTrip object
        is a VERY Strong SemiPrime (Four prime factors of [0], [1], [3]).

        This code is ... IN THEORY ... (instances found should be logged)!
        '''
        if self.IsSemiPrime and self.IsSSP:
            assert self.FB.C%2 != self.FB.D%2, "Bad FibBox!"
            if self.FB.C%2 == 1 and self.FB.D%4 == 0:
                if gmp.is_prime(self.FB.C) and gmp.is_prime(gmp.f_div(self.FB.D, 4)):
                    return True
            elif self.FB.D%2 == 1 and self.FB.C%4 == 0:
                if gmp.is_prime(self.FB.D) and gmp.is_prime(gmp.f_div(self.FB.C, 4)):
                    return True
            #else:
            #    raise pt.PyTripRootException("Bad FibBox!")
        return False

    @property
    def IsForm1(self):
        '''Is this PT Form 1?
        '''
        if self._if1 is None:
            self._if1 = ptf.IsForm1(self._pt)
        return self._if1

    @property
    def IsForm2(self):
        '''Is this PT Form 2?
        '''
        if self._if2 is None:
            self._if2 = ptf.IsForm2(self._pt)
        return self._if2

    @property
    def IsForm3(self):
        '''Is this PT Form 3?
        '''
        if self._if3 is None:
            self._if3 = ptf.IsForm3(self._pt)
        return self._if3

    @property
    def DepthPrice(self):
        '''Gets the depth of the Price tree.
        '''
        if self._pd is None:
            self._pd = len(self.PriceString)-1
        return self._pd

    @property
    def PDepth(self):
        '''Proxy method for `self.DepthPrice`
        '''
        return self.DepthPrice

    @property
    def DepthP(self):
        '''Proxy method for `self.DepthPrice`
        '''
        return self.DepthPrice

    @property
    def DepthClassic(self):
        '''Gets the depth of the Classic tree.
        '''
        if self._cd is None:
            self._cd = len(self.ClassicString)-1
        return self._cd

    @property
    def CDepth(self):
        '''Proxy method for `self.DepthClassic`
        '''
        return self.DepthClassic

    @property
    def DepthC(self):
        '''Proxy method for `self.DepthClassic`
        '''
        return self.DepthClassic

    @property
    def PriceLRI(self):
        '''Gets the left-to-right index
        of this node in the Price tree.
        '''
        if self._plri is None:
            self._plri = PyTripExt.LRIndex(self.PriceS)
        return self._plri

    @property
    def PriceLRIndex(self):
        '''Proxy method for `self.PriceLRI`
        '''
        return self.PriceLRI

    @property
    def ClassicLRI(self):
        '''Gets the left-to-right index
        of this node in the Classic tree.
        '''
        if self._clri is None:
            self._clri = PyTripExt.LRIndex(self.ClassicS)
        return self._clri

    @property
    def ClassicLRIndex(self):
        '''Proxy method for `self.ClassicLRI`
        '''
        return self.ClassicLRI

    @property
    def PxCy(self):
        if self._pxcy is None:
            #if self.IsRoot or self.PriceP.IsRoot or self.ClassicP.IsRoot:
            if self.IsRoot:
                self._pxcy = pt.PyTripPxCy.P0C0
            else:
                if self.IsPriceA and self.IsClassicA:
                    self._pxcy = pt.PyTripPxCy.PaCa
                elif self.IsPriceA and self.IsClassicB:
                    self._pxcy = pt.PyTripPxCy.PaCb
                elif self.IsPriceA and self.IsClassicC:
                    self._pxcy = pt.PyTripPxCy.PaCc
                elif self.IsPriceB and self.IsClassicA:
                    self._pxcy = pt.PyTripPxCy.PbCa
                elif self.IsPriceB and self.IsClassicB:
                    self._pxcy = pt.PyTripPxCy.PbCb
                elif self.IsPriceB and self.IsClassicC:
                    self._pxcy = pt.PyTripPxCy.PbCc
                elif self.IsPriceC and self.IsClassicA:
                    self._pxcy = pt.PyTripPxCy.PcCa
                elif self.IsPriceC and self.IsClassicB:
                    self._pxcy = pt.PyTripPxCy.PcCb
                elif self.IsPriceC and self.IsClassicC:
                    self._pxcy = pt.PyTripPxCy.PcCc
        return self._pxcy

    @property
    def IsABD(self):
        '''Can this be an ABD member of an Euler Brick?
        '''
        if self._isABD is None:
            if not gmp.iroot(self.PyTrip.c, 3)[1]:
                self._isABD = (False, None)
            else:
                if self._isABD is None:
                    self._isABD_form1()
                if self._isABD is None:
                    self._isABD_form2()
                if self._isABD is None:
                    self._isABD_form3()
                if self._isABD is None:
                    self._isABD_uvw_f1_eq1()
                if self._isABD is None:
                    self._isABD_ace_f1_eq1()
                if self._isABD is None:
                    self._isABD_bcf_f1_eq1()
                if self._isABD is None:
                    self._isABD_failOver()
                if self._isABD is None:
                    self._isABD = (False, None)
        return self._isABD[0]

    def _isABD_uvw_f1_eq1(self):
        '''Test supporting `IsABD`

        Sets ABD as an Euler Brick member when uvw[f1] == 1.
        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            if self.FB.F1 > w:
                v = gmp.f_div(self.FB.F1-w, 2)
                if w > v:
                    u = gmp.iroot(w**2-v**2, 2)
                    if u[1]:
                        u = u[0]
                        tmp = pt.MakePyTrip(u, v, w)
                        tst = eb.EB_FromPT(tmp)
                        if tst.abd == self.PyTrip:
                            #print("ABD uvw[f1]==1") # Debug
                            self._isABD = (True, tmp)

    def _isABD_ace_f1_eq1(self):
        '''Test supporting `IsABD`

        Sets ABD as an Euler Brick member when ace[f1] == 1.
        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            v = gmp.f_div(w-1, 2)
            u = gmp.iroot(w**2-v**2, 2)
            if u[1]:
                u = u[0]
                tmp = pt.MakePyTrip(u, v, w)
                tst = eb.EB_FromPT(tmp)
                if tst.abd == self.PyTrip:
                    #print("ABD ace[f1]==1") # Debug
                    self._isABD = (True, tmp)

    def _isABD_bcf_f1_eq1(self):
        '''Test supporting `IsABD`

        Sets ABD as an Euler Brick member when bcf[f1] == 1.

        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            u = gmp.f_div(w+1, 2)
            v = gmp.iroot(w**2-u**2, 2)
            if v[1]:
                v = v[0]
                tmp = pt.MakePyTrip(u, v, w)
                tst = eb.EB_FromPT(tmp)
                if tst.abd == self.PyTrip:
                    #print("ABD bcf[f1]==1") #Debug
                    self._isABD = (True, tmp)

    def _isABD_form1(self):
        '''Test supporting `IsABD`

        uvw is of the 1'st form
        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            v = w-1
            if gmp.is_square(w**2-v**2):
                u = gmp.iroot(w**2-v**2, 2)[0]
                tmp = pt.MakePyTrip(u, v, w)
                if eb.EB_FromPT(tmp).abd == self.PyTrip:
                    #print("ABD is Form 1") # Debug
                    self._isABD = (True, tmp)

    def _isABD_form2(self):
        '''Test supporting `IsABD`

        uvw is of the 2'nd form
        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            u = w-2
            if gmp.is_square(w**2-u**2):
                v = gmp.iroot(w**2-u**2, 2)[0]
                tmp = pt.MakePyTrip(u, v, w)
                if eb.EB_FromPT(tmp).abd == self.PyTrip:
                    #print("ABD is Form 2") # Debug
                    self._isABD = (True, tmp)

    def _isABD_form3(self):
        '''Test supporting `IsABD`

        uvw is of the 3'rd form
        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            tmp = gmp.sqrt((w**2-1)/2)
            u, v = gmp.mpz(gmp.floor(tmp)), gmp.mpz(gmp.ceil(tmp))
            if u**2+v**2 == w**2:
                if u%2 == 0 and v%2 == 1:
                    u, v = v, u
                tmp = pt.MakePyTrip(u, v, w)
                if eb.EB_FromPT(tmp).abd == self.PyTrip:
                    #print("ABD is Form 3") # Debug
                    self._isABD = (True, tmp)
            if self._isABD is None:
                # Somewhat redundant
                tmp = gmp.sqrt((w**2+1)/2)
                u, v = gmp.mpz(gmp.floor(tmp)), gmp.mpz(gmp.ceil(tmp))
                if u**2+v**2 == w**2:
                    if u%2 == 0 and v%2 == 1:
                        u, v = v, u
                    tmp = pt.MakePyTrip(u, v, w)
                    if eb.EB_FromPT(tmp).abd == self.PyTrip:
                        #print("ABD is Form 3") # Debug
                        self._isABD = (True, tmp)

    def _isABD_failOver(self):
        '''Test supporting `IsABD`

        Inefficiently hackish. But, workable.

        The failOver instance or scenario works off of the w/2
        critical point of the (positive) cubic equation:
               Red: -4x^2+3(w^2)x-a (tested)
             Green: -4x^2+3(w^2)x+a (implement if desired)
            Purple:  4x^2-3(w^2)x+a (implement if desired)
              Blue:  4x^2-3(w^2)x-a (implement if desired)

        Finds a zero point, `zpt > w/2`
        then calculates the other two x-axis intercept points and tests them.

        Inefficiently Hackish:
            This is done with iterators instead of a formal cubic equation solver.
            A formal cubic equation solver would likely be more efficient.
            The iterators operate on 10**i, decimal, and bit-length limiters.

        If `zpt > w/2` tests to 0, then a solution is found (or, not an ABD member).
        Else, the fractional parts of solving, algebraicly, for the other two points,
            should be very close. Where, in reality, one of these is a whole number,
            and the fractional part would naturally belong to `zpt`, such that
            `zpt.frac` is exactly 0.

        Finally, if neither of these scenarios (conditions) is (or, are) valid,
        then the Pythagorean Triple behind this class is not an ABD member of
        and Euler Brick.

        There is additional documentation on this in the `EulerBricks.pdf` file
        associated with this software.

        '''
        if self._isABD is None:
            w = gmp.iroot(self.PyTrip.c, 3)[0]
            a = self.PyTrip.a
            try:
                assert -4*((w/2)**3)+3*(w**2)*(w/2)-a > 0, "Not an ABD member."
            except:
                self._isABD = (False, None)
                return None
            i, j = 1, 0                      # Counters
            zpt = gmp.ceil(w/2)              # Zero Point
            tst = -4*(zpt**3)+3*(w**2)*zpt-a # Cubic Equation Test (Red Equation)
            # Find Maximum zpt+10**i for Cubic where < 0
            while True:
                tmp = zpt+(10**i)
                if tmp >= w-2:
                    i -= 1
                    break
                tst = -4*(tmp**3)+3*(w**2)*tmp-a
                if tst > 0:
                    i += 1
                else:
                    i -= 1
                    break
            # Incrememt zpt to very close approximation using (10**i)
            zpt += (10**i)
            while i > 0:
                zpt += (10**i)
                if zpt >= w-2:
                    zpt -= (10**i)
                    i -= 1
                    continue
                tst = -4*(zpt**3)+3*(w**2)*zpt-a
                if tst < 0:
                    zpt -= (10**i)
                    i -= 1
                    j = 0
                else:
                    j += 1
                    if j == 9:
                        i -= 1
                        j = 0
            tst = -4*(zpt**3)+3*(w**2)*zpt-a
            try:
                assert tst >= 0, "Test of zpt should be >= 0."
            except:
                zpt -= 1
                tst = -4*(zpt**3)+3*(w**2)*zpt-a
                assert tst >= 0, "Test of zpt should be >= 0."
            j = 0
            while j < 9:
                zpt += 1
                tst = -4*(zpt**3)+3*(w**2)*zpt-a
                if tst <= 0:
                    zpt -= 1
                    break
                else:
                    j += 1
                    if j == 9:
                        break
            tst = -4*(zpt**3)+3*(w**2)*zpt-a
            tst2 = -4*((zpt+1)**3)+3*(w**2)*(zpt+1)-a
            assert tst >= 0 and tst2 <= 0, "Test and Test2 not aligned!"
            if tst == 0 or tst2 == 0:
                u = abs(gmp.mpz(zpt)) if tst == 0 else abs(gmp.mpz(zpt+1))
                if gmp.is_square(w**2-u**2):
                    v = gmp.iroot(w**2-u**2, 2)[0]
                    tmp = pt.MakePyTrip(u, v, w)
                    if eb.EB_FromPT(tmp).abd == self.PyTrip:
                        self._isABD = (True, tmp)
                        return None
            if self._isABD is None:
                lst = []
                # See class documentation; algebraic solver
                tst = (zpt+gmp.sqrt((3*(w**2))-(3*(zpt**2))))/(-2)
                lst.append(abs(gmp.mpz(gmp.floor(tst))))
                if lst[-1]%2 == 0:
                    lst.append(lst[-1]-1)
                    lst.append(lst[-2]+1)
                else:
                    lst.append(lst[-1]-2)
                    lst.append(lst[-2]+2)
                lst.append(abs(gmp.mpz(gmp.ceil(tst))))
                if lst[-1]%2 == 0:
                    lst.append(lst[-1]-1)
                    lst.append(lst[-2]+1)
                else:
                    lst.append(lst[-1]-2)
                    lst.append(lst[-2]+2)
                # See class documentation; algebraic solver
                tst = (zpt-gmp.sqrt((3*(w**2))-(3*(zpt**2))))/(-2)
                lst.append(abs(gmp.mpz(gmp.floor(tst))))
                if lst[-1]%2 == 0:
                    lst.append(lst[-1]-1)
                    lst.append(lst[-2]+1)
                else:
                    lst.append(lst[-1]-2)
                    lst.append(lst[-2]+2)
                lst.append(abs(gmp.mpz(gmp.ceil(tst))))
                if lst[-1]%2 == 0:
                    lst.append(lst[-1]-1)
                    lst.append(lst[-2]+1)
                else:
                    lst.append(lst[-1]-2)
                    lst.append(lst[-2]+2)
                lst = sorted(list(set([x for x in lst if x%2 == 1])))
                for u in lst:
                    if gmp.is_square(w**2-u**2):
                        v = gmp.iroot(w**2-u**2, 2)[0]
                        tmp = pt.MakePyTrip(u, v, w)
                        if eb.EB_FromPT(tmp).abd == self.PyTrip:
                            self._isABD = (True, tmp)
                            break

    @property
    def IsABDof(self):
        '''If this can be an ABD member of an Euler Brick,
        which PyTrip(u, v, w) forms the Euler Brick?
        '''
        if self.IsABD:
            return self._isABD[1]
        else:
            return None

    @property
    def IsACE(self):
        '''Can this be an ACE member of an Euler Brick?
        '''
        if self._isACE is None:
            v = gmp.f_div(self.FB.D, 2) if self.FB.D%2 == 0 else gmp.f_div(self.FB.C, 2)
            w = self.FB.C if self.FB.C%2 == 1 else self.FB.D
            try:
                assert v%2 == 0 and w%2 == 1 and w > v
            except AssertionError as ae:
                self._isACE = (False, None)
                return self._isACE[0]
            if gmp.is_square(w**2-v**2):
                u = gmp.iroot(w**2-v**2, 2)[0]
                try:
                    assert u**2+v**2==w**2
                except AssertionError as ae:
                    self._isACE = (False, None)
                    return self._isACE[0]
                else:
                    # TODO: Connect the dots ...
                    # TODO: ... instead of storing a separate memory object
                    self._isACE = (True, pt.MakePyTrip(u, v, w))
        return self._isACE[0]

    @property
    def IsACEof(self):
        '''If this can be an ACE member of an Euler Brick,
        which PyTrip(u, v, w) forms the Euler Brick?
        '''
        if self.IsACE:
            return self._isACE[1]
        else:
            return None

    @property
    def IsBCF(self):
        '''Can this be a BCF member of an Euler Brick?
        '''
        if self._isBCF is None:
            u = gmp.f_div(self.FB.D, 2) if self.FB.D%2 == 0 else gmp.f_div(self.FB.C, 2)
            w = self.FB.C if self.FB.C%2 == 1 else self.FB.D
            try:
                assert u%2 == 0 and w%2 == 1 and w > u
            except AssertionError as ae:
                self._isBCF = (False, None)
                return self._isBCF[0]
            if gmp.is_square(w**2-u**2):
                v = gmp.iroot(w**2-u**2, 2)[0]
                try:
                    assert u**2+v**2==w**2
                except AssertionError as ae:
                    self._isBCF = (False, None)
                    return self._isBCF[0]
                else:
                    # TODO: Connect the dots ...
                    # TODO: ... instead of storing a separate memory object
                    self._isBCF = (True, pt.MakePyTrip(u, v, w))
        return self._isBCF[0]

    @property
    def IsBCFof(self):
        '''If this can be a BCF member of an Euler Brick,
        which PyTrip(u, v, w) forms the Euler Brick?
        '''
        if self.IsBCF:
            return self._isBCF[1]
        else:
            return None

    def __init__(self, pytrip=None, **kwargs):
        assert isinstance(pytrip, pt.PyTrip)
        assert pt.IsPrimitivePyTrip(pytrip)
        # These variables (below) are local (not global) for a reason.
        # Please do not re-locate them outside of the __init__ method.
        # They are declared inside the __init__ method for a reason!
        # (Memory management reasons.)
        self._pt = pytrip
        # Precision is needed for mpfr calculations (division, sqrt, etc)
        if gmp.get_context().precision < self.PyTrip.c.bit_length()*8:
            gmp.get_context().precision = self.PyTrip.c.bit_length()*8
        # Euler Brick, Fibonacci Box, Arithmetic Progression of Squares
        self._eb, self._fb, self._aps, self._aps2 = None, None, None, None
        # IIF Form 1, 2, 3
        self._if1, self._if2, self._if3 = None, None, None
        # Depth
        self._pd, self._cd = None, None
        # L-to-R Index
        self._plri, self._clri = None, None
        # String Rep, Price & Classic
        self._srp, self._src = None, None
        # If Price or Classic, what is the other branch a descendent of?
        self._pxcy = None
        # EulerBrick Membership
        self._isABD, self._isACE, self._isBCF = None, None, None
        # attempt to prevent infinite recursion (hackish)
        self.__norecurse = False
        # options
        self._opts = {}
        if not kwargs is None:
            self._opts = kwargs

    @staticmethod
    def LRIndex(s):
        '''Gets the left-to-right index of a child node.
        '''
        assert isinstance(s, str)
        assert s[0] == "0"
        idx = 1
        for d in s[1:]:
            idx = 3*(idx-1)
            if d in ["A", "a"]: idx += 1
            elif d in ["B", "b"]: idx += 2
            elif d in ["C", "c"]: idx += 3
            else: raise Exception("Bad string character.")
        return idx

    def BFS(self, tree=None, depth=2, ret=None):
        '''Returns an iterator for a breadth-first (search)
        of a given tree at a given depth. Depth defaults to 2.
        Tree (type) must be given.
        '''
        if ret is None:
            return PyTripExtIter.BFS2(self, tree, depth)
        elif ret == type(pt.PyTrip):
            return PyTripExtIter.BFS(self.PyTrip, tree, depth)
        else:
            raise Exception("ret type must be '%s' or '%s'" % (type(self), type(pt.PyTrip)))

class PyTripExtIter:
    '''Iterators for PyTrip
    '''

    @staticmethod
    def BFS(obj=None, tree=None, depth=2):
        '''Breadth-First Iteration of a PT tree
        starting at the given object root.

        WARN: Inefficient!
        WARN: Inefficient!
        WARN: Inefficient!

        '''
        if isinstance(obj, PyTripExt):
            return PyTripExtIter.BFS2(obj, tree, depth)
        assert isinstance(obj, pt.PyTrip)
        assert isinstance(tree, pt.PyTripType)
        assert tree in [pt.PyTripType.Price, pt.PyTripType.Classic]
        assert depth >= 1
        if depth >= 13:
            print("WARN: Tenary Tree BFS >= 13")
            print("WARN: This could take some time.")
            if depth >= 15:
                print("\tWARN: Tenary Tree BFS >= 15")
                print("\tWARN: This could take A LOT of time.")
        a, d = [obj, []], 0
        while d < depth:
            for x in a[0:-1]:
                if d < depth-1:
                    if tree == pt.PyTripType.Classic:
                        a[-1].append(ng.NextGen_Classic_A(x))
                        a[-1].append(ng.NextGen_Classic_B(x))
                        a[-1].append(ng.NextGen_Classic_C(x))
                    elif tree == pt.PyTripType.Price:
                        a[-1].append(ng.NextGen_Price_A(x))
                        a[-1].append(ng.NextGen_Price_B(x))
                        a[-1].append(ng.NextGen_Price_C(x))
                yield x
            d += 1
            a = a[-1]
            a.append([])

    @staticmethod
    def BFS2(obj=None, tree=None, depth=2):
        '''Breadth-First Iteration of a PT tree
        starting at the given object root.

        WARN: Inefficient!
        WARN: Inefficient!
        WARN: Inefficient!

        '''
        if isinstance(obj, pt.PyTrip):
            return PyTripExtIter.BFS(obj, tree, depth)
        assert isinstance(obj, PyTripExt)
        assert isinstance(tree, pt.PyTripType)
        assert tree in [pt.PyTripType.Price, pt.PyTripType.Classic]
        assert depth >= 1
        if depth >= 13:
            print("WARN: Tenary Tree BFS >= 13")
            print("WARN: This could take some time.")
            if depth >= 15:
                print("\tWARN: Tenary Tree BFS >= 15")
                print("\tWARN: This could take A LOT of time.")
        a, d = [obj, []], 0
        while d < depth:
            for x in a[0:-1]:
                if d < depth-1:
                    if tree == pt.PyTripType.Classic:
                        a[-1].append(x.ClassicA)
                        a[-1].append(x.ClassicB)
                        a[-1].append(x.ClassicC)
                    elif tree == pt.PyTripType.Price:
                        a[-1].append(x.PriceA)
                        a[-1].append(x.PriceB)
                        a[-1].append(x.PriceC)
                #yield x.PyTrip
                yield x
            d += 1
            a = a[-1]
            a.append([])

    class DFS:
        '''DFS iterators for PyTripExt
        '''

        @staticmethod
        def PostOrder(obj=None, tree=None, depth=None):
            #assert isinstance(obj, pt.PyTrip)
            #assert isinstance(tree, pt.PyTripType)
            #assert tree in [pt.PyTripType.Price, pt.PyTripType.Classic]
            #assert depth >= 1
            raise NotImplementedError()

        @staticmethod
        def PreOrder(obj=None, tree=None, depth=None):
            #assert isinstance(obj, pt.PyTrip)
            #assert isinstance(tree, pt.PyTripType)
            #assert tree in [pt.PyTripType.Price, pt.PyTripType.Classic]
            #assert depth >= 1
            raise NotImplementedError()

        def InOrder(obj=None, tree=None, depth=None):
            raise NotImplementedError("Not implemented for ternary trees.")

class PyTripConnectTheDots(PyTripExt):
    '''This research code is as of yet unpublished.
    Should ever reach maturity or stability, only then
    will it be published.

    This is ultimate goal, or objective,
    of the PyTrip package, as a whole.
    '''
    pass

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



