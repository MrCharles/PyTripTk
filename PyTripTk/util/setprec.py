#!/usr/bin/env python3

#3rd Party
import gmpy2 as gmp

def __gmp__setprec2(n, mul=8, quiet=False):
    '''Sets the precision for gmp based on an input variable, and a multiple (of that variable's bits).
    '''
    assert isinstance(n, type(gmp.mpz())), "Input `n` must be of type gmp.mpz"
    assert isinstance(mul, int), "Multiple must be an integer."
    assert mul >= 1, "Multiple must be >= 1"
    assert isinstance(quiet, bool), "Quiet must be boolean: True|False"
    try:
        bl = n.bit_length()
        gmp.get_context().precision = 8*bl
    except:
        print("Not set.")
    else:
        if not quiet:
            print("GMP precision set to %s" % (8*bl))

if 'setprec' in dir(gmp):
    del gmp.setprec

gmp.setprec = __gmp__setprec2
gmp.setprec.__doc__ = __gmp__setprec2.__doc__
if '__gmp__setprec2' in dir():
    del __gmp__setprec2
    tmp_bits = gmp.get_context().precision
    tmp_val = gmp.mpz(2)**tmp_bits-1
    try:
        gmp.setprec(tmp_val, 1, True)
    except:
        print("Could not set precision!")
    finally:
        gmp.get_context().precision = tmp_bits

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



