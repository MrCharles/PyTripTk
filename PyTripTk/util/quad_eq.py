#!/usr/bin/env python3

import gmpy2 as gmp

def quad_eq(a, b, c, z=0):
    '''Quick & Dirty Quadratic equation solver.

    ax^2+bx+c==z

    x = (-b +/- sqrt(b^2-4ac))/(2a)

    If the sqrt portion is negative,
    before taking the sqrt, it is
    converted to positive (ensuring success).

    Returns:
        3-tuple of (x, y, neg_img)
        X and Y are the +/- (over 2a)
        neg_img:
            True indicates sqrt was imaginary (changed to positive).
            False indicates sqrt was already positive (not imaginary).
    '''
    if z != 0:
        c -= z
        z = 0
    bsqm4ac = (b**2)-4*a*c
    neg_img = True if bsqm4ac < 0 else False
    if neg_img:
        bsqm4ac *= -1
    sqrt = gmp.sqrt(bsqm4ac)
    x = (-b - sqrt)/(2*a)
    y = (-b + sqrt)/(2*a)
    return (x, y, neg_img)

def from_3points(a, b, c):
    '''Accepts 3 tuples, each an (x, y) point.
    Returns a 3-tuple of ax^2+bx+c as (a, b, c).

    Gives the variables in a parabolic equation from 3 points.
    '''
    assert isinstance(a, tuple), "Input `a` must be a tuple of 2 points."
    assert len(a) == 2, "Input `a` must be a tuple of 2 points."
    assert isinstance(b, tuple), "Input `b` must be a tuple of 2 points."
    assert len(b) == 2, "Input `b` must be a tuple of 2 points."
    assert isinstance(c, tuple), "Input `c` must be a tuple of 2 points."
    assert len(c) == 2, "Input `c` must be a tuple of 2 points."
    a = (gmp.mpfr(a[0]), gmp.mpfr(a[1]))
    b = (gmp.mpfr(b[0]), gmp.mpfr(b[1]))
    c = (gmp.mpfr(c[0]), gmp.mpfr(c[1]))
    div0, div1, div2 = (a[0]-b[0])*(a[0]-c[0]), (b[0]-a[0])*(b[0]-c[0]), (c[0]-a[0])*(c[0]-b[0])
    xsq = a[1]/div0 + b[1]/div1 + c[1]/div2
    x = (a[1]*(-b[0]-c[0]))/div0 + (b[1]*(-a[0]-c[0]))/div1 + (c[1]*(-a[0]-b[0]))/div2
    y = (a[1]*(-b[0]*(-c[0])))/div0 + (b[1]*(-a[0]*(-c[0])))/div1 + (c[1]*(-a[0]*(-b[0])))/div2
    return (xsq, x, y)

def from_Npoint_y2x(n):
    '''This routine will generate a curve from 3 points, based on ...
    Form 1: (n, y, z=y+1) <-- FROM/TO --> Form 2: (n*(n-2), 2*(n-1), n*(n-2)+1)
    '''
    assert isinstance(n, int) or isinstance(n, type(gmp.mpz()))
    assert n >= 3 and n%2 == 1
    pt1 = (n, 2*gmp.f_div(n+1, 2)*gmp.f_div(n-1, 2)) # d == (n-1)/2
    pt3 = (n*(n-2), 2*(n-1))                         # d == 1
    pt2 = None                                       # d == calculated midpoint
    if gmp.f_div(n-1, 2)%2 == 1:
        pt2 = (n*gmp.f_div(n-1, 2), 2*gmp.f_div(n+gmp.f_div(n-1, 2), 2)*gmp.f_div(n-gmp.f_div(n-1, 2), 2))
    else:
        assert gmp.f_div(n+1, 2)%2 == 1
        pt2 = (n*gmp.f_div(n+1, 2), 2*gmp.f_div(n+gmp.f_div(n+1, 2), 2)*gmp.f_div(n-gmp.f_div(n+1, 2), 2))
    return from_3points(pt1, pt2, pt3)

def from_Npoint_x2i(n):
    '''This routine will generate a curve from 3 points, based on ...
    Form 2: (n*(n-2), 2*(n-1), n*(n-2)+1) <-- FROM/TO --> Infinity.
    The upper point or limit is calculated as the y-axis-value from Form 1.
    The middle point is calculated as the closest of two between (midpoint).
    '''
    assert isinstance(n, int) or isinstance(n, type(gmp.mpz()))
    assert n >= 3 and n%2 == 1
    pt1 = (n*(n-2), 2*(n-1))
    yMax = 2*gmp.f_div(n+1, 2)*gmp.f_div(n-1, 2)
    c, d = n-1, 1
    # 2*(c+x)*(d+x) == yMax
    # 2*(cd+cx+dx+x^2) == yMax
    # 2x^2+2(c+d)x+2cd-yMax == 0
    # x == (-2(c+d)+gmp.sqrt(4(c+d)^2-8(2cd-yMax)))/(4)
    x = gmp.mpz(gmp.ceil(abs((-2*(c+d)+gmp.sqrt(4*(c+d)**2-8*(2*c*d-yMax)))/4)))
    pt3 = ((n-2)*((c+x)+(d+x)), 2*(c+x)*(d+x))
    hx = gmp.f_div(x, 2)
    pt2 = ((n-2)*((c+hx)+(d+hx)), 2*(c+hx)*(d+hx))
    return from_3points(pt1, pt2, pt3)

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



