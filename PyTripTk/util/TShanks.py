#!/usr/bin/env python3

'''
Tonelli-Shanks algorithm (using gmp).

Taken/Modified from:
    ### https://en.wikipedia.org/wiki/Tonelli%E2%80%93Shanks_algorithm#The_algorithm
    ### https://rosettacode.org/wiki/Tonelli-Shanks_algorithm#Python
'''

# 3rd party
import gmpy2 as gmp

def tonelli_shanks(n, p, dbg=False):
    '''Returns x^2 \equiv n (mod p)
    '''
    n, p = gmp.mpz(n), gmp.mpz(p)
    try:
        assert gmp.legendre(n, p) == 1, "not a square (mod p)"
    except AssertionError as ae:
        if dbg:
            raise ae
        else:
            return None
    q, s = gmp.mpz(p)-1, gmp.mpz(0)
    # DOC: remove powers of 2 as `s`, from p-1==`q`
    while q%2 == 0:
        q = gmp.f_div(q, 2)
        s += 1
    assert not s is 0, "s cannot be 0"
    # DEBUG
    #if dbg: print("q={qq}\ns={ss}".format(qq=q, ss=s))
    # DOC: Initial `q` and `s` values are set.
    if s == 1:
        # Not caught (on purpose)
        assert p%4 == 3, "s==1 and p%4 != 3 ; (TODO)"
        try:
            # Same as the end try/catch.
            r = gmp.powmod(n, gmp.f_div(p+1, 4), p)
            assert (r**2)%p == n%p, "(r**2)%p != n%p"
            return r
        except AssertionError as ae:
            if dbg:
                raise ae
            else:
                return None
    # DOC: set z (find a quadratic non residue)
    z, ph = gmp.mpz(2), gmp.f_div(p-1, 2)
    while True:
        if p-1 == gmp.powmod(z, ph, p):
            break
        z += 1
    # DEBUG
    #if dbg: print("z={zz}".format(zz=z))
    # `z` is set
    m, t2, c, r, t = s, gmp.mpz(0), gmp.powmod(z, q, p), gmp.powmod(n, gmp.f_div(q+1, 2), p), gmp.powmod(n, q, p)
    # DEBUG
    #if dbg: print(m, c, r, t)
    while (t-1)%p != 0:
        t2 = gmp.powmod(t, 2, p)
        for i in range(1, m):
            if (t2-1)%p == 0:
                break
            t2 = gmp.powmod(t2, 2, p)
        b = gmp.powmod(c, gmp.mpz(1)<<(m-i-1), p)
        c = gmp.powmod(b, 2, p)
        r = (r*b)%p
        t = (t*c)%p
        m = i
        # DEBUG
        #if dbg: print(m, c, r, t)
    try:
        assert (r**2)%p == n%p, "(r**2)%p != n%p"
        return r
    except AssertionError as ae:
        if dbg:
            raise ae
        else:
            return None

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



