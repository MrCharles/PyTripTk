#!/usr/bin/env python3

'''
This file contains Sage (maths, Python2) routines that can be called
from a Python3 VirtualEnv.

Index:
    SOS_WStein5m.py -- `sum_of_squares` -- Sum of Squares (limited to 5-minutes).
    sage_factor.py  -- `try_factor`     -- Calls the Sage factoring algorithm.

'''

# Builtin Libaries
import os, sys
from subprocess import TimeoutExpired

# 3rd party libary
import gmpy2 as gmp

# Local libs
from PyTripTk.util import sage_subpop as sspop

SAGE_CMD = ['sage',]
MYDIR = os.path.abspath(os.curdir)
SOS_SCRIPT = 'SOS_WStein5m.py'
FACTOR_SCRIPT = 'sage_factor.py'
FIND_FACTOR_SCRIPT = 'sage_find_factor.py'

def _path_check(script, dbg=False, verbose=False):
    '''Check if the scripts exist.
    If so, return the (absolute) path for the script to be used.
    '''
    a_path = os.path.join(MYDIR, 'PyTripTk', 'PyTripTk', 'util', script)
    b_path = os.path.join(MYDIR, 'PyTripTk', 'util', script)
    c_path = os.path.join(MYDIR, 'util', script)
    if verbose or dbg:
        print("(Path Check) Trying to locate one of...")
        for xpath in [a_path, b_path, c_path]:
            print("\t'{:5} -- {}'".format(os.path.exists(xpath), xpath))
    for xpath in [a_path, b_path, c_path]:
        if os.path.exists(xpath):
            try:
                assert os.path.isfile(xpath), "Script not a file?"
            except AssertionError as ae:
                raise ae
            else:
                return xpath
    return None

def sum_of_squares(n, tout=30, dbg=False, verbose=False):
    script_path = _path_check(SOS_SCRIPT)
    assert not script_path is None, "Could not detect script location!"
    assert os.path.isfile(script_path), "Script not a file?"
    cmd = SAGE_CMD[::]
    cmd.append('{}'.format(script_path))
    assert isinstance(n, int) or isinstance(n, type(gmp.mpz(1))), "Input `n` must be an integer."
    n = gmp.mpz(n)
    assert n > 3, "Input `n` must be greater than 3"
    assert n%4 == 1, "Input `n` must be 1 == n mod 4 ; 1==n%4"
    assert gmp.is_prime(n), "Input `n` must be prime!"
    cmd.append('{}'.format(n))
    data = sspop.sage_subpop(cmd, tout)
    if dbg:
        print("RAW:", repr(data))
    # Process data
    # (repr) format should be """ b'(x, y)' """
    # Decode b''
    data = data[0].decode('utf8')
    if dbg:
        print("Decoded:", repr(data))
    # Strip '(', ')', ' '
    data = data.replace('(', '').replace(')', '').replace(' ', '')
    if dbg:
        print("Clean:", repr(data))
    # Split ','
    data = data.split(',')
    if dbg:
        print("Split:", repr(data))
    # Convert to integer list
    data = [gmp.mpz(x) for x in data]
    # sort it
    data = sorted(data)
    return data

def try_factor(n, tout=30, dbg=False, verbose=False):
    #print("dbg=", dbg, "; verbose=", verbose)
    script_path = _path_check(FACTOR_SCRIPT)
    assert not script_path is None, "Could not detect script location!"
    assert os.path.isfile(script_path), "Script not a file?"
    cmd = SAGE_CMD[::]
    cmd.append('{}'.format(script_path))
    if dbg:
        print(repr(cmd))
    assert isinstance(n, int) or isinstance(n, type(gmp.mpz(1))), "Input `n` must be an integer."
    input_n = gmp.mpz(n)
    n = gmp.mpz(n)
    sign = -1 if n < 0 else 1
    n = abs(n)
    assert n > 3, "Input `n` must be an integer > 3."
    try:
        assert not gmp.is_prime(n), "Input `n` cannot be prime!"
    except AssertionError as ae:
        if dbg:
            print("{} is prime".format(n))
        return [gmp.mpz(sign), n]
    epow = 0
    while n%2 == 0 and n > 0:
        epow += 1
        n = gmp.f_div(n, 2)
        if n == 0 or n == 1: break
    try:
        assert n != 1 and n%2 == 1, "Nothing to factor, n=2^{}".format(epow) # Special case (dumb)
    except AssertionError as ae:
        ret = [gmp.mpz(2),]*epow
        if sign == -1:
            ret.append(gmp.mpz(sign))
        ret = sorted(ret)
        return ret
    cmd.append('{}'.format(n))
    if dbg:
        print("Command: {}".format(repr(cmd)))
    data = sspop.sage_subpop(cmd, tout)
    if dbg:
        print("RAW:", repr(data))
    # Process data
    # (repr) format should be """b'x * y * z * ...'"""
    # Decode b''
    data = data[0].decode('utf8')
    if dbg:
        print("Decoded:", repr(data))
    # Clean
    data = data.replace('\n', '')
    if dbg:
        print("Cleaned:", repr(data))
    # Split ' * '
    data = data.split(' * ')
    if dbg:
        print("Split:", repr(data))
    # Convert to integer list
    tmp = []
    for x in data:
        if '^' in x:
            pwr = x.split('^')
            tmp.extend([gmp.mpz(pwr[0]),]*int(pwr[1]))
        else:
            tmp.append(gmp.mpz(x))
    data = tmp[::]
    del tmp
    # Put powers of 2 back
    if epow > 0:
        data.extend([gmp.mpz(2),]*epow)
    # Put a -1 if the input was negative
    if sign == -1:
        data.append(gmp.mpz(sign))
    # Put it all in order (sort the newly minted list)
    data = sorted(data)
    # Verify all of the work above (list should multiply to the same as the input).
    tst = gmp.mpz(1)
    for x in data: tst *= x
    try:
        assert tst == input_n, "Output factorization not the same as the input!"
    except AssertionError as ae:
        raise ae
    finally:
        # Data is returned irrespective of the test (???)
        return data

def find_factor(n, tout=30, dbg=False, verbose=False):
    script_path = _path_check(FIND_FACTOR_SCRIPT)
    assert not script_path is None, "Could not detect script location!"
    assert os.path.isfile(script_path), "Script not a file?"
    cmd = SAGE_CMD[::]
    cmd.append('{}'.format(script_path))
    if dbg:
        print(repr(cmd))
    assert isinstance(n, int) or isinstance(n, type(gmp.mpz(1))), "Input `n` must be an integer."
    input_n = gmp.mpz(n)
    n = gmp.mpz(n)
    sign = -1 if n < 0 else 1
    n = abs(n)
    assert n > 3, "Input `n` must be an integer > 3."
    try:
        assert not gmp.is_prime(n), "Input `n` cannot be prime!"
    except AssertionError as ae:
        if dbg:
            print("{} is prime".format(n))
        return [gmp.mpz(sign), n]
    epow = 0
    while n%2 == 0 and n > 0:
        epow += 1
        n = gmp.f_div(n, 2)
        if n == 0 or n == 1: break
    try:
        assert n != 1 and n%2 == 1, "Nothing to factor, n=2^{}".format(epow) # Special case (dumb)
    except AssertionError as ae:
        ret = [gmp.mpz(2),]*epow
        if sign == -1:
            ret.append(gmp.mpz(sign))
        ret = sorted(ret)
        return ret
    cmd.append('{}'.format(n))
    if dbg:
        print("Command: {}".format(repr(cmd)))
    try:
        data = sspop.sage_subpop(cmd, tout)
    except TimeoutExpired as texp:
        if dbg:
            print("Timeout attempting to factor: {}".format(n))
        return [gmp.mpz(sign), n]
    if dbg:
        print("RAW:", repr(data))
    # Process data
    # (repr) format should be """b'[n1, n2]'"""
    # Decode b''
    data = data[0].decode('utf8')
    if dbg:
        print("Decoded:", repr(data))
    # Clean
    data = data.replace('\n', '').replace(' ', '').strip('[').strip(']')
    if dbg:
        print("Cleaned:", repr(data))
    if '^' in data:
        raise Exception("Circumflex exponential needs to handled here, too (???)")
    # Split ','
    data = data.split(',')
    if dbg:
        print("Split:", repr(data))
    # Convert to integer list
    try:
        data = [gmp.mpz(x) for x in data]
    except:
        print(repr(data))
    # Put powers of 2 back
    if epow > 0:
        data.extend([gmp.mpz(2),]*epow)
    # Put a -1 if the input was negative
    if sign == -1:
        data.append(gmp.mpz(sign))
    # Put it all in order (sort the newly minted list)
    data.sort()
    # Verify all of the work above (list should multiply to the same as the input).
    tst = gmp.mpz(1)
    for x in data: tst *= x
    try:
        assert tst == input_n, "Output factorization not the same as the input!"
    except AssertionError as ae:
        raise ae
    finally:
        # Data is returned irrespective of the test (???)
        return data

def __gmp__ext__sum_of_squares(mpz):
    '''Returns sum of squares for a prime integer (1 mod 4).
    '''
    if isinstance(mpz, int):
        return __gmp__ext__sum_of_squares(gmp.mpz(mpz))
    if isinstance(mpz, type(gmp.mpz())):
        if gmp.is_prime(mpz):
            return sum_of_squares(mpz)
    raise Exception("Input must prime, and of type mpz.")

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



