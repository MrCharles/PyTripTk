#!/usr/bin/env python3

'''
ToDo: Description
'''

# 3rd party libary
import gmpy2 as gmp

class SumOfSquares(tuple):
    '''Sum of Squares provides a standard Python list, of length 2,
    representing a sum of two squares.
    '''

    @property
    def N(self):
        '''a^2+b^2
        '''
        return sum([x**2 for x in self])

    @property
    def n(self):
        '''Alias for `N`
        '''
        return self.N

    def __init__(self, tup):
        assert isinstance(tup, tuple) or isinstance(tup, list), "Input must be a tuple or list."
        assert len(tup) == 2, "Input must be of length 2"
        assert tup[0] > 0 and tup[1] > 0, "Input must be integers greater than 0."
        assert tup[0] != tup[1], "Input must not be equal."
        assert tup[0]%2 != tup[1]%2, "Input must be different (mod 2) values."
        super(tuple, tuple(tup))

    def __eq__(self, val):
        if isinstance(val, type(gmp.mpz())) or isinstance(val, int):
            return self.n == val
        elif isinstance(val, list) or isinstance(val, tuple):
            if len(val) == 2:
                if val[0] in self and val[1] in self:
                    return True
            else:
                # this is inconsistent and might be problematic
                try:
                    return sum(val) == self.n
                except:
                    pass
        return False

    def __int__(self):
        return self.n

class DifferenceOfSquares(tuple):
    '''Difference of Squares provides a standard Python list, of length 2,
    representing a difference of two squares.
    '''

    @property
    def N(self):
        '''C^2-D^2
        '''
        return self.C**2-self.D**2

    @property
    def n(self):
        '''Alias for `N`
        '''
        return self.N

    @property
    def C(self):
        '''Center value in the center and difference of squares.
        '''
        return max(self)

    @property
    def c(self):
        '''Alias for `C`
        '''
        return self.C

    @property
    def D(self):
        '''Difference value in the center and difference of squares.
        '''
        return min(self)

    @property
    def d(self):
        '''Alias for `D`
        '''
        return self.D

    def __init__(self, tup):
        assert isinstance(tup, tuple) or isinstance(tup, list), "Input must be a tuple or list."
        assert len(tup) == 2, "Input must be of length 2"
        assert tup[0] > 0 and tup[1] > 0, "Input must be integers greater than 0."
        assert tup[0] != tup[1], "Input must not be equal."
        assert tup[0]%2 != tup[1]%2, "Input must be different (mod 2) values."
        super(tuple, tuple(tup))

    def __eq__(self, val):
        if isinstance(val, type(gmp.mpz())) or isinstance(val, int):
            return self.n == val
        elif isinstance(val, list) or isinstance(val, tuple):
            if len(val) == 2:
                if val[0] == self.C and val[1] == self.D:
                    return True
            else:
                # this is inconsistent and might be problematic
                try:
                    return sum(val) == self.n
                except:
                    pass
        return False

    def __int__(self):
        return self.n

def __gmp__ext__difference_of_squares(mpz):
    '''Returns difference of squares for a prime integer.
    '''
    if isinstance(mpz, int):
        return __gmp__ext__difference_of_squares(gmp.mpz(mpz))
    if isinstance(mpz, type(gmp.mpz())):
        if mpz > 2 and mpz%2 == 1:
            return [gmp.f_div(mpz+1, 2), gmp.f_div(mpz-1, 2)]
    raise Exception("Input of type mpz and 1 mod 2.")

def __gmp__ext__difference_of_squares2(f1, f2):
    '''Returns difference of squares for a non-prime integer, given factors.
    '''
    if isinstance(f1, int):
        if isinstance(f2, int):
            return __gmp__ext__difference_of_squares2(gmp.mpz(f1), gmp.mpz(f2))
        else:
            return __gmp__ext__difference_of_squares2(gmp.mpz(f1), f2)
    elif isinstance(f2, int):
        return __gmp__ext__difference_of_squares2(f1, gmp.mpz(f2))
    if isinstance(f1, type(gmp.mpz())) and isinstance(f2, type(gmp.mpz())):
        if (f1%2 == f2%2) and (f2 > f1 > 0):
            return [gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)]
    raise Exception("Input must be of type mpz, f1%2==f2%2, f2 > f1 > 0")

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



