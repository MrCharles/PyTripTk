#!/usr/bin/env python3

'''
A very hackish implementation of factoring through various means,
for Python3.
'''

# Standard Library
from subprocess import TimeoutExpired

# 3rd party library
import gmpy2 as gmp

# Local libraries
from PyTripTk.util import bash_factor
from PyTripTk.util import sage_stuff

def factor(n, tout=30, dbg=False, verbose=False):
    '''Attempts to factor an input `n`

    Timeout defaults to 30 seconds.
    '''
    assert isinstance(n, int) or isinstance(n, type(gmp.mpz(0)))
    ooo = gmp.mpz(n)
    n = gmp.mpz(n)
    sign = -1 if n < 0 else 1
    n = abs(n)
    assert n > 3, "Input `n` must be greater than 3!"
    epow = 0
    while n%2 == 0:
        epow += 1
        n = gmp.f_div(n, 2)
    bits = n.bit_length()
    n = int(n)
    ret = None
    if bits > 64:
        #if dbg or verbose:
        #    print("Using Sage factor")
        #ret = sage_stuff.try_factor(n, tout, dbg=dbg, verbose=verbose)
        if dbg or verbose:
            print("Using Sage ecm.find_factor(n), recursive.")
        facts = [n,]
        try:
            tmp_lst = [x for x in facts if not gmp.is_prime(x)]
            try:
                # Ex.Fix: gmp.mpz(73200691951502228519836993359748802647106862684736)
                if len(tmp_lst) == 0 and len(facts) > 0:
                    if all([True if gmp.is_prime(x) else False for x in facts]):
                        ret = [gmp.mpz(x) for x in facts]
                        if epow > 0: ret.extend([gmp.mpz(2),]*epow)
                        if sign == -1: ret.append(gmp.mpz(sign))
                        ret = sorted(ret)
                        return ret
                # Any other condition: Original code branch.
                tmp = min(tmp_lst)
            except ValueError as verr:
                print("Input: {}".format(ooo))
                print("Current: {}".format(n))
                print("2^{}, Sign: {}".format(epow, sign))
                print("Temp List: {}".format(tmp_lst))
                print("Facts: {}".format(facts))
                raise verr
            if tmp != n:
                raise Exception("Could not start removing factors!")
            while len(tmp_lst) > 0:
                if tmp.bit_length() < 64:
                    facts_tmp = bash_factor.try_factor(tmp, tout)
                else:
                    facts_tmp = sage_stuff.find_factor(tmp, tout, dbg=dbg, verbose=verbose)
                tst = gmp.mpz(1)
                for x in facts_tmp: tst *= x
                assert tst == tmp
                if not tmp in facts_tmp:
                    while tmp in facts:
                        facts.extend(facts_tmp)
                        facts.remove(tmp)
                tmp_lst = [x for x in facts if not gmp.is_prime(x) and x > 1]
                if len(tmp_lst) == 1:
                    if tmp_lst[0] == tmp:
                        break
                if len(tmp_lst) > 0:
                    tmp = min(tmp_lst)
                else:
                    break
        except TimeoutExpired as tex:
            if len(facts) == 1:
                if facts[0] == n:
                    raise tex
            ret = sorted(facts)
        else:
            ret = sorted(facts)
    else:
        if dbg or verbose:
            print("Using bash factor")
        ret = bash_factor.try_factor(n, tout)
    while gmp.mpz(1) in ret:
        del ret[ret.index(gmp.mpz(1))]
    if epow > 0: ret.extend([gmp.mpz(2),]*epow)
    if sign == -1: ret.append(gmp.mpz(sign))
    ret = sorted(ret)
    return ret



