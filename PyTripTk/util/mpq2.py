#!/usr/bin/env python3

#3rd Party
import gmpy2 as gmp

class __gmp__mpq2():
    '''Extends gmp.mpq with some abbreviations and additional info.

    This whole thing replicates and passes everything through gmp.mpq --
        Because gmp.mpq did not want to allow overloads.

    It's funky. It's hackish.

    Not Implemented:
        Round
        SizeOf
        Floor
        Ceil

    Changes:
        If the result of an operation is an mpq, then it returns itself, an mpq2 instance.

    Additions:
        You can use `n` instead of `numerator`
        You can use `d` instead of `denominator`
        You can use `q` to get the `quotient` in floored integer division.
        You can use `r` to get the `remainder` in floored integer division.
        You can use `f` to get an mpfr of the fractional representation.
            You must manually set your desired precision.
    '''

    @property
    def n(self):
        '''Numerator
        '''
        return self._mpq.numerator

    @property
    def numerator(self):
        '''Numerator, derived from the base mpq.
        '''
        return self._mpq.numerator

    @property
    def d(self):
        '''Denominator
        '''
        return self._mpq.denominator

    @property
    def denominator(self):
        '''Denominator, derived from the base mpq.
        '''
        return self._mpq.denominator

    @property
    def q(self):
        '''Quotient of floored division.
        '''
        if self._q is None:
            self._q, self._r = gmp.f_divmod(self.n, self.d)
        return self._q

    @property
    def r(self):
        '''Remainder of floored division.
        '''
        if self._r is None:
            self._q, self._r = gmp.f_divmod(self.n, self.d)
        return self._r

    @property
    def f(self):
        '''Fractional representation (gmp.mpfr)
        '''
        if self._f is None:
            self._f = self.n/self.d
        return self._f

    def __init__(self, mpq):
        assert isinstance(mpq, type(gmp.mpq())), "Input must be of type gmp.mpq()"
        self._mpq = mpq
        self._q, self._r, self._f = None, None, None

    # Misc
    def __get__(self, instance, owner):
        '''Get the base mpq object.
        '''
        return self._mpq

    # Math Functions
    def __add__(self, val):
        '''Addition.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__add__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __radd__(self, val):
        '''Addition.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__radd__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __sub__(self, val):
        '''Subtraction.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__sub__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rsub__(self, val):
        '''Subtraction.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__rsub__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __mul__(self, val):
        '''Multiplication.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__mul__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rmul__(self, val):
        '''Multiplication.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__rmul__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __floordiv__(self, val):
        '''Floored Integer Division.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__floordiv__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rfloordiv__(self, val):
        '''Floored Integer Division.

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__rfloordiv__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __truediv__(self, val):
        '''True Integer Division (tdiv).

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__truediv__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rtruediv__(self, val):
        '''True Integer Division (tdiv).

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__truediv__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp

    def __divmod__(self, val):
        '''Divmod, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__divmod__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rdivmod__(self, val):
        '''Divmod, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__rdivmod__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp

    def __mod__(self, val):
        '''Mod, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__mod__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rmod__(self, val):
        '''Mod, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__rmod__(val._mpq if isinstance(val, type(self)) else val)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp

    def __pow__(self, val, mod=None):
        '''Pow, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__pow__(val._mpq if isinstance(val, type(self)) else val, mod)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __rpow__(self, val, mod=None):
        '''Pow, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__rpow__(val._mpq if isinstance(val, type(self)) else val, mod)
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp

    # Python (Numeric) Type Conversions
    def __int__(self):
        '''Conversion to Integer, as returned by mpq...
        '''
        return int(self._mpq)
    def __float__(self):
        '''Conversion to Float, as returned by mpq...
        '''
        return float(self._mpq)

    # Equality / Comparison
    def __eq__(self, val):
        '''Equality, as returned by mpq...
        '''
        return self._mpq.__eq__(val._mpq if isinstance(val, type(self)) else val)
    def __ge__(self, val):
        '''Equality, greater than or equal to, as returned by mpq...
        '''
        return self._mpq.__ge__(val._mpq if isinstance(val, type(self)) else val)
    def __gt__(self, val):
        '''Equality, greater than, as returned by mpq...
        '''
        return self._mpq.__gt__(val._mpq if isinstance(val, type(self)) else val)
    def __le__(self, val):
        '''Equality, less than or equal to, as returned by mpq...
        '''
        return self._mpq.__le__(val._mpq if isinstance(val, type(self)) else val)
    def __lt__(self, val):
        '''Equality, less than, as returned by mpq...
        '''
        return self._mpq.__lt__(val._mpq if isinstance(val, type(self)) else val)
    def __ne__(self, val):
        '''Equality, negation, as returned by mpq...
        '''
        return self._mpq.__ne__(val._mpq if isinstance(val, type(self)) else val)

    # Positive / Negative
    def __neg__(self):
        '''The negative, or inverted value, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__neg__()
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp
    def __pos__(self):
        '''The positive, or inverted value, as returned by mpq...

        Returns mpq2 if result is mpq, else the object as returned by the base mpq class.
        '''
        tmp = self._mpq.__pos__()
        if isinstance(tmp, type(gmp.mpq())): return gmp.mpq2(tmp)
        else: return tmp

    # Abs, Bool & Hash
    def __abs__(self):
        '''Absolute Value, as returned by mpq...
        '''
        return self._mpq.__abs__()
    def __bool__(self):
        '''Boolean (true), as returned by mpq...
        If an object exists, then, unless exactly zero, should return 1 (true).
        '''
        return self._mpq.__bool__()
    def __hash__(self):
        '''Hash, as returned by self (not mpq)!
        '''
        return hash(self)

    # Repr & Str
    def __repr__(self):
        '''Get the representation of the object, as returned by mpq...
        '''
        return self._mpq.__repr__()
    def __str__(self):
        '''Get the string of the object, as returned by mpq...
        '''
        return self._mpq.__str__()

    # Base Conversion
    def digits(self, base=10):
        '''Get the digits for the given base, as returned by mpq...
        '''
        assert isinstance(base, int) or isinstance(base, type(gmp.mpz())), "Base must be an integer."
        assert base >= 2 and base <= 62, "Base must between 2 and 62 (inclusive)."
        try: return self._mpq.digits(base)
        except: raise NotImplementedError("Bad implementation: gmp.mpq2.digits()")

    # Not Implemented
    def __round__(self):
        '''Rounding, not implemented.
        '''
        raise NotImplementedError("Not Implemented: gmp.mpq2.round")
    def __sizeof__(self):
        '''SizeOf, not implemented.
        '''
        raise NotImplementedError("Not Implemented: gmp.mpq2.sizeof (this is bad)")
    def __floor__(self):
        '''Floor, not implemented.
        '''
        raise NotImplementedError("Not Implemented: gmp.mpq2.floor")
    def __ceil__(self):
        '''Ceiling, not implemented.
        '''
        raise NotImplementedError("Not Implemented: gmp.mpq2.ceil")

if not 'mpq2' in dir(gmp):
    gmp.mpq2 = __gmp__mpq2

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



