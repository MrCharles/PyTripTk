#!/usr/bin/env python3

import gmpy2 as gmp

def golden_ratio():
    return (1+gmp.sqrt(5))/2

if not 'const_golden' in dir(gmp):
    gmp.const_golden = golden_ratio

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



