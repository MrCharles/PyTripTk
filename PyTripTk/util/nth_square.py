#!/usr/bin/env python3

import gmpy2 as gmp

def next_square(base):
    assert gmp.iroot_rem(base, 2)[1] == 0, "The base number must be square!"
    return (gmp.iroot_rem(base, 2)[0]+1)**2

def nth_square(base, nth):
    '''N'th Square Algorithm
    Accepts a base number, and the N'th square from the base.

    n^2 + (d-1)*n + b

    Where n is the n'th square number after the base, b.
    `d` or delta is caluclated as:
        d = (b+1)^2-b^2

    `d` is the delta or difference between each segment, over the multiplied additive.
    '''
    assert gmp.iroot_rem(base, 2)[1] == 0, "The base number must be square!"
    assert nth >= 1, "The N'th square root from the base must be an integer!"
    bsqrt = gmp.iroot_rem(base, 2)[0]
    delta = (bsqrt+1)**2-bsqrt**2
    calc_algo = x**2 + (delta-1)*x + base
    assert gmp.iroot_rem(calc_algo, 2)[1] == 0, "Number did not work out to a square!"
    return calc_algo



