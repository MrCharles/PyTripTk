#!/usr/bin/env python3

import gmpy2 as gmp

def previous_prime(n):
    n = gmp.mpz(n)
    p = n - 256
    t = gmp.next_prime(p)
    while t >= n:
        p -= 256
        t = gmp.next_prime(p)
    p = gmp.mpz(t)
    assert gmp.is_prime(p) and p < n and t < n
    while p < n and t < n:
        t = gmp.next_prime(p)
        if t < n:
            p = gmp.mpz(t)
    try:
        assert t >= n and p < t
        assert p < n and gmp.is_prime(p)
    except AssertionError as ae:
        print("    N Input: %s" % (n))
        print(" Temp Value: %s" % (t))
        print("   At Prime: %s" % (p))
        raise ae
    else:
        return p

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



