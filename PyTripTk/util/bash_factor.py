#!/usr/bin/env python3

'''
Contains a function to call the `factor` command at the bash shell.

Only works for numbers 127-bits or less. (num < 2**127-1)
'''

# Python Library
import subprocess as subpop

# 3rd Party
import gmpy2 as gmp

def try_factor(num, tout=30, dbg=False, verbose=False):
    '''Contains a function to call the `factor` command at the bash shell.
    Only works for numbers 127-bits or less. (num < 2**127-1)

    Returns a list of gmp.mpz(int) objects.

        * If the list is empty, the number was > 127 bits.
        * If the list contains 2 entries, 1 and the input number, it is prime.
        * If the list starts with any number other than 1, the input number is fully factored.

    '''
    n = gmp.mpz(num)
    if n.bit_length() > 127:
        return []
    else:
        pipe = subpop.PIPE
        var = subpop.Popen(['factor', str(n)], stderr=pipe, stdout=pipe).communicate(timeout=tout)
        if var[0].decode('utf8') == '':
            raise Exception(var[1].decode('utf8'))
        else:
            data = var[0].decode('utf8')
            if data == '1:\n':
                return [1,]
            try:
                assert data.startswith(str(n)+": ")
            except AssertionError as ae:
                print(repr(data))
                raise
            if str(n)+": "+str(n) in data:
                return [gmp.mpz(1), n]
            facts = data.split(':')[1].rstrip('\n').rstrip(' ').lstrip(' ').split(' ')
            facts = [gmp.mpz(x) for x in facts]
            assert len(facts) > 1
            return facts

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



