#!/usr/bin/sage

'''

This (python) script is meant to be run at the command line.

It requires Sage (maths package) be installed.
It requires a Python 2 environment.

It accepts a single argument. One of the following:

    (1) Either a file in the `/tmp` directory containing a single number
    (2) A number.

It runs the 'ecm.find_factor(n)' command from the Sage (maths) package,
and returns the output (if available).

This has a 5-minute fork timeout and should be silent.

'''

import sys

#from sage.all import *
from sage.all import Integer, ecm, fork, is_prime

@fork(timeout=305, verbose=True)
def sage_factor(p):
    p = Integer(p)
    if is_prime(p):
        return ('1 * {}'.format(p)).encode('utf8')
    else:
        var = None
        try:
            var = ecm.find_factor(p)
            if var[0].is_perfect_power():
                base, exp = var[0].perfect_power()
                var.extend([base,]*exp)
                del var[0]
                var.sort()
            if var[-1].is_perfect_power():
                base, exp = var[-1].perfect_power()
                del var[-1]
                var.extend([base,]*exp)
                var.sort()
            return var
        except ValueError as ve:
            print("Err:\n\tVar: {}".format(var))
            return var

if __name__ == '__main__':
    if sys.argv[1].startswith('/tmp/'):
        import os
        if os.path.isfile(sys.argv[1]):
            if os.path.isfile(sys.argv[1]+".out"):
                ovar = None
                with open(sys.argv[1]+".out", 'rb') as _fh:
                    ovar = _fh.read()
                ovar = ovar.strip('\n').strip(' ')
                print(ovar)
            else:
                ivar = None
                with open(sys.argv[1], 'rb') as _fh:
                    ivar = _fh.read()
                ivar = ivar.decode('utf8').strip('\n').strip(' ')
                ovar = sage_factor(ivar)
                with open(sys.argv[1]+".out", 'wb') as _fh:
                    if isinstance(ovar, tuple):
                        _fh.write(str(ovar).encode('utf8'))
                    else:
                        _fh.write(repr(ovar).encode('utf8'))
                print(ovar)
        else:
            raise Exception("No such file %s" % (sys.argv[1]))
    else:
        ovar = sage_factor(sys.argv[1])
        print(ovar)



