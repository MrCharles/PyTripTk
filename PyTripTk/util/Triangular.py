#!/usr/bin/env python3

'''
PyTripTk/util/Triangular.py

Handles triangular numbers.

Secondary:
    -- Monkey-patches GMP with gmp.is_triangular = is_triangular
    -- Monkey-patches GMP with gmp.is_square_triangular = is_square_triangular
'''

#3rd party
import gmpy2 as gmp

# local (PyTripTk)
from PyTripTk.util import quad_eq as qeq

def is_triangular(val):
    '''Boolean test if a number is triangular.
    Ensures the |absolute value| of the (integer floored) input (non-negative).
    '''
    if gmp.frac(val) != 0: val = gmp.mpz(gmp.floor(abs(gmp.mpfr(val))))
    else: val = gmp.mpz(abs(val))
    return gmp.is_square(8*val+1)

def is_square_triangular(val):
    '''Boolean test if a number is both square and triangular.
    Ensures the |absolute value| of the (integer floored) input (non-negative).
    '''
    if gmp.frac(val) != 0: val = gmp.mpz(gmp.floor(abs(gmp.mpfr(val))))
    else: val = gmp.mpz(abs(val))
    return is_triangular(val) and gmp.is_square(val)

def get_triangular(val):
    '''Returns an integer `n`,
        such that the input value should be composed of (n*(n+1))/2
    if and only if the input value is triangular
    if the input value is not triangular, the return is `None`
    Ensures the |absolute value| of the (integer floored) input (non-negative).
    '''
    if gmp.frac(val) != 0: val = gmp.mpz(gmp.floor(abs(gmp.mpfr(val))))
    else: val = gmp.mpz(abs(val))
    if not is_triangular(val):
        return None
    arr = qeq.quad_eq(1, 1, -2*val)
    assert gmp.frac(arr[0]) == 0 and gmp.frac(arr[1]) == 0, "fractional return: input not triangular"
    # Overkill
    #assert arr[0] < 0 and arr[1] > 0 and abs(abs(arr[1])-abs(arr[0])) == 1, "absolute difference of return not 1: not triangular"
    #try:
    #    assert gmp.f_div(gmp.mpz(arr[1]*(arr[1]+1)), 2) == val
    #    return arr[1]
    #except AssertionError as ae:
    #    try:
    #        assert gmp.f_div(gmp.mpz(abs(arr[0])*(abs(arr[0])+1)), 2) == val
    #        return arr[0]
    #    except AssertionError as ae2:
    #        return None
    assert gmp.f_div(gmp.mpz(arr[1]*(arr[1]+1)), 2) == val
    return gmp.mpz(arr[1])

def next_triangular(val):
    '''Returns the next triangular number.
    Ensures the |absolute value| of the (integer floored) input (non-negative).
    Does not require the input be triangular.
    '''
    if gmp.frac(val) != 0: val = gmp.mpz(gmp.floor(abs(gmp.mpfr(val))))
    else: val = gmp.mpz(abs(val))
    arr = qeq.quad_eq(1, 1, -2*val)
    assert arr[0] < 0 and arr[1] > 0, "Failure: %s" % (repr(arr))
    assert abs(arr[0])-1 == arr[1], "Failure: %s" % (repr(arr))
    if gmp.frac(arr[1]) != 0:
        tst = gmp.mpz(gmp.floor(arr[1]))
        assert gmp.mpz((tst*(tst+1))/2) < val, "Failure: %s, Test=%s" % (repr(arr), tst)
        tst += 1
        assert gmp.mpz((tst*(tst+1))/2) > val, "Failure: %s, Test=%s" % (repr(arr), tst)
        return gmp.mpz((tst*(tst+1))/2)
    else:
        tst = gmp.mpz(arr[1])
        assert gmp.mpz((tst*(tst+1))/2) == val, "Failure: %s, Test=%s" % (repr(arr), tst)
        tst += 1
        assert gmp.mpz((tst*(tst+1))/2) > val, "Failure: %s, Test=%s" % (repr(arr), tst)
        return gmp.mpz((tst*(tst+1))/2)

def prev_triangular(val):
    '''Returns the previous triangular number.
    Ensures the |absolute value| of the (integer floored) input (non-negative).
    Does not require the input be triangular.
    '''
    if gmp.frac(val) != 0: val = gmp.mpz(gmp.floor(abs(gmp.mpfr(val))))
    else: val = gmp.mpz(abs(val))
    arr = qeq.quad_eq(1, 1, -2*val)
    assert arr[0] < 0 and arr[1] > 0, "Failure: %s" % (repr(arr))
    assert abs(arr[0])-1 == arr[1], "Failure: %s" % (repr(arr))
    if gmp.frac(arr[1]) != 0:
        tst = gmp.mpz(gmp.floor(arr[1]))+1
        assert gmp.mpz((tst*(tst+1))/2) > val, "Failure: %s, Test=%s" % (repr(arr), tst)
        tst -= 1
        assert gmp.mpz((tst*(tst+1))/2) < val, "Failure: %s, Test=%s" % (repr(arr), tst)
        return gmp.mpz((tst*(tst+1))/2)
    else:
        tst = gmp.mpz(arr[1])
        assert gmp.mpz((tst*(tst+1))/2) == val, "Failure: %s, Test=%s" % (repr(arr), tst)
        tst -= 1
        assert gmp.mpz((tst*(tst+1))/2) < val, "Failure: %s, Test=%s" % (repr(arr), tst)
        return gmp.mpz((tst*(tst+1))/2)

if not 'is_triangular' in dir(gmp):
    gmp.is_triangular = is_triangular

if not 'is_square_triangular' in dir(gmp):
    gmp.is_square_triangular = is_square_triangular

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



