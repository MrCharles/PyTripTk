#!/usr/bin/env python3

import gmpy2 as gmp

def silver_ratio():
    return 1+gmp.sqrt(2)

if not 'const_silver' in dir(gmp):
    gmp.const_silver = silver_ratio

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



