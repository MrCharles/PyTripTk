#!/usr/bin/env python3

'''
This file contains a routine to call a Sage (maths package)
subprocess script from within a Python3 virtualenv.

As for 2019 (yeah, bad joke), Sage is not yet Python3
compatible. Trying to call sage (requires Python2) from
a Python3 environment or shell leads to errors -- Py3 does
not know about Sage, or, trying to make a system call,
such as:

    os.system('sage script vars')
        ~~~~~ OR ~~~~~
    subprocess.Popen(['sage', 'script', 'vars'], stdout=subprocess.PIPE).communicate(timeout=30)

An error is thrown:
    ImportError: No module named 'sage'

The solution is simple enough:
    Activating VirtualEnv (temporarily) modifies the user's
    system path. When Subprocess is called, Python's
    `os.environ` is (effectively) passed wholesale in
    setting-up the subprocess environment. Thus, that path
    variable must be removed, as should the 'VIRTUAL_ENV'
    environment variable.
'''

import os
import subprocess as subpop
from subprocess import TimeoutExpired
import psutil
from psutil import NoSuchProcess

def sage_subpop(cmd, tout=30):
    '''Only meant to be used if calling a (Python2) Sage maths
    script from a Python3 virtual environment!!!

    Only meant to be used in a *nix environment, where
    the path separator is `:`

    The command should be a list.
    See subprocess documentation.

    The timeout should be no less than 5 seconds (default: 30).

    This function is rudimentary:
        After modifying local/temporary environment variables,
        the following is returned wholesale:
            >>> subprocess.Popen(cmd, stdout=subprocess.PIPE, env=tmp_env).communicate(timeout=timeout)

    Removal of offending environment variables is a ONE-SHOT deal.
    '''
    assert isinstance(cmd, list), "The command must be a list; see subprocess documentation!"
    assert cmd[0] == 'sage', "The first command must be 'sage'"
    assert len(cmd) >= 2
    assert 'VIRTUAL_ENV' in os.environ.keys(), "This only works inside a virtual environment!"
    assert 'PATH' in os.environ.keys(), "This only works if a (user) PATH variable is set!"
    assert isinstance(tout, int), "The `timeout` variable must be an integer"
    assert tout >= 5, "The `timeout` variable must be >= 5 seconds\nIt can take this long to initialize sage and a script."
    tmp_env = os.environ.copy()
    tmp_path = os.environ['PATH'].split(':')
    venv = os.environ['VIRTUAL_ENV']
    for x in range(0, len(tmp_path)-1):
        if venv in tmp_path[x]:
            del tmp_path[x]
    tmp_env['PATH'] = ':'.join(tmp_path)
    del tmp_env['VIRTUAL_ENV']
    proc = subpop.Popen(cmd, stdout=subpop.PIPE, env=tmp_env)
    pid = proc.pid
    try:
        outs, errs = proc.communicate(timeout=tout)
    except TimeoutExpired as texp:
        psp = psutil.Process(pid=proc.pid)
        try:
            for child in psp.children(recursive=True):
                if 'ecm' in child.cmdline():
                    child.kill()
            proc.kill()
            outs, errs = proc.communicate()
            #print(outs, "\n\n", errs)
        except NoSuchProcess:
            pass
        raise texp
    return (outs, errs)

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



