#!/usr/bin/env python3

'''
This little module is because Python's native statistics package tends to
complain about irrational numbers (continuous, fractional, or decimal) when
asking for Standard Deviation. (Whatever.)

Numpy might handle this, but, unchecked.

NOTE/WARN:
    This is UNTESTED other than if it throws an unexpected error.
    Some computations involve checking the bit-length, or floating-point
    precision; and calculating or computing thereabouts. It *SHOULD*
    work with GMP across the board. (Unlike Python's stats module.)

'''

# Python Library
from collections import namedtuple as nt

# 3rd Party
import gmpy2 as gmp

BSTATS = nt('Basic_Stats', ['avg', 'stdev', 'min', 'max', 'median', 'mode'])

def mode(lst):
    '''Returns the mode of a list if available.

    This routine is inefficient.
    For any list between 1000 and 5000 (or more)
    entries, will be very slow above a certain point.
    '''
    if len(set(lst)) < len(lst):
        dct = {}
        for x in set(lst):
            dct[x] = lst.count(x)
        ct_max = max(dct.values())
        return sorted([gmp.mpfr(x) for x in dct.keys() if dct[x] == ct_max])
    else:
        return None

def median(lst):
    '''Returns a median value of a list.

    If the list length is odd, returns the exact center.
    If the list length is even, returns the average of 1/2 +/- 1.
    '''
    mlst = sorted(lst)
    if len(mlst)%2 == 0:
        return gmp.mpfr((mlst[int(len(mlst)/2)]+mlst[int(len(mlst)/2)-1])/2)
    else:
        return gmp.mpfr(mlst[int((len(mlst)+1)/2)])

def standard_deviation(lst, avg):
    '''Computes the standard deviation of a list.
    '''
    sdlst = [(x-avg)**2 for x in lst]
    return gmp.sqrt(sum(sdlst)/len(sdlst))

def bstats(lst, fpp_bit_lock=False, smooth_center=0):
    '''Accepts a list and returns a namedtuple of the basics.

    TODO: `fpp_bit_lock` (false, but, if int, lock bit computations).
    TODO: `smooth_center` (regressively locate stdev up to 3 times).
    TODO: Sampling regression instead of population (after `smooth_center`).

    Both `bit_lock` and `smooth_center` variables are irrelevant,
        and meant to be custom statistic measure implementations.
        At a later point in time ...

    '''
    assert isinstance(lst, list), "input must be a list"
    assert len(lst) >= 2, "at least 2 data points needed"
    bs_avg, bs_min, bs_max = sum(lst)/len(lst), min(lst), max(lst)
    bs_stdev = standard_deviation(lst, bs_avg)
    bs_median, bs_mode = median(lst), mode(lst)
    return BSTATS(avg=gmp.mpfr(bs_avg), stdev=gmp.mpfr(bs_stdev),
            min=gmp.mpfr(bs_min), max=gmp.mpfr(bs_max),
            median=bs_median, mode=bs_mode)

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



