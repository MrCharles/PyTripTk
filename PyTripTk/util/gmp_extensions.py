#!/usr/bin/env python3

'''
PyTripTk has patches the GNU Multi-Precision library with several extensions.
'''

################################################################################
################################################################################
################################################################################

MESSAGE = '''
PyTripTk has patches the GNU Multi-Precision library with several extensions.

These extensions modify GMP and the integrated MPFR top-level import.

Most files in this project start with a specific import:

    >>> import gmpy2 as gmp
    >>> _

Patching something like mpq with additional functionality is painful. So, for
example, one of the patches is ...

    >>> import PyTripTk.util.mpq2
    >>> _

Inside of that file is a statement (at the end):

    >>> if not 'mpq2' in dir(gmp):
            gmp.mpq2 = __gmp__mpq2
    >>> _

And, at the top-level, typically the author starts
a python (console) session with the following:

    >>> import PyTripTk as pttk
    >>> _

Inside of the package designator, PyTripTk/__init__.py ...

    >>> 'mpq2' in dir(gmp)
    False
    >>> if not 'mpq2' in dir(gmp):
    ....    import PyTripTk.util.mpq2
    ....
    >>> 'mpq2' in dir(gmp)
    True
    >>> _

That's how the patching is done.

To find the location of the patches (beyond the basic import),
    See: PyTripTk/__init__.py
... and read through the list below.

!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! WARNING / DISCLAIMER !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!

If you're using these patches, and you have similar patching, these
patches should not interfere with your global or customized GMP
environment unless this is imported after (other customizations), or
similar nomenclature is in-play.

None of these patches are part of the official `gmpy2` distribution,
nor either of GMP or MPFR (proper).

Reiterating the license behind this project:
    NO WARRANTY

Oh, and, as always:
    RTFM!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!! END WARNING / DISCLAIMER !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#####################################
########## LIST OF PATCHES ##########
#####################################

gmp.mpq2 -- Provides short-hand access to:
    `numerator` as `n`
    `denominator` as `d`
    Accepts a standard mpq instance as its input.

    USAGE:
    >>> foo = gmp.mpq()
    >>> var = gmp.mpq2(foo)
    >>> assert foo.numerator == var.n
    >>> assert foo.denominator == var.d
    >>> help(var) # See also...
    >>> _

    LOCATION:
        PyTripTk/util/mpq2.py

~~~~~
~~~~~

gmp.setprec -- Set the precision based on an input integer.
    The default is 8*n.bits, or 8x the number of bits of the input value, N.
    Unless verbose is turned off, displays a message with the number of bits.
    This is GLOBAL.

    USAGE:
    >>> help(gmp.setprec)
    >>> n = gmp.mpz(1)
    >>> var = gmp.setprec(n)
    GMP precision set to 8
    >>> _
    >>> # Except this is bad, the default is something like 54-bits.

    EQUIVALENT:
    >>> n = gmp.mpz(1)
    >>> gmp.get_context().precision = 8*n
    >>> # Because "set_context" doesn't seem to work for some-odd reason...
    >>> _

    LOCATION:
        PyTripTk/util/setprec.py

~~~~~
~~~~~

gmp.is_triangular, gmp.is_square_triangular --
    Tests for triangular numbers.

    USAGE:
    >>> from PyTripTk import Triangular as tri
    >>> foo = tri.next_triangular(1)
    >>> gmp.is_triangular(foo)
    True
    >>> bar = foo
    >>> while not gmp.is_square_triangular(bar):
            bar = tri.next_triangular(bar)
    >>> bar
    36
    >>> gmp.is_square_triangular(bar)
    True
    >>> _

    LOCATION:
        PyTripTk/util/Triangular.py

        This file also contains the functions:
            -- `get_triangular`
            -- `next_triangular`
            -- `prev_triangular`

~~~~~
~~~~~

gmp.prev_prime -- GMP has a `next_prime` function, why shouldn't it have a
    `prev_prime` function? The answer to this question is beyond me. This
    is a quick hack based on subtracting a prime-gap and looking forward
    using `next_prime`

    USAGE:

    >>> gmp.prev_prime(19)
    mpz(17)
    >>> _

~~~~~
~~~~~

gmp.sos -- Sum of Squares (for prime numbers only)
    There's a lot of number theory behind "Sum of Squares" (theorem).
    See documentation otherwise for details.

    USAGE:
    >>> gmp.sos(13)
    [mpz(2), mpz(3)]
    >>> gmp.sos(17)
    [mpz(1), mpz(4)]
    >>> gmp.sos(19)
    AssertionError: Input `n` must be 1 == n mod 4 ; 1==n%4
    >>> gmp.sos(21)
    Exception: Input must prime, and of type mpz.
    >>> gmp.sos(23)
    # same assertion error as 19
    >>> gmp.sos(29)
    [mpz(1), mpz(4)]
    >>> _

    LOCATION:
        PyTripTk/util/sage_stuff.py

~~~~~
~~~~~

gmp.dos -- Difference of Squares
    Complimentary to Sum of Squares;
    simplistic since there is ONLY ONE representation.

    USAGE:
    >>> gmp.dos(17)
    [mpz(9), mpz(8)]
    >>> assert gmp.dos(17) == [gmp.f_div(17+1, 2), gmp.mpz(17-1, 2)]
    True
    >>> gmp.dos(19)
    [mpz(10), mpz(9)]

    LOCATION:
        PyTripTk/util/OfSquares.py

~~~~~
~~~~~

gmp.dos2 -- Difference of Squares (for compsite numbers; accepts 2 factors)
    Complimentary to Sum of Squares

    USAGE:
    >>> gmp.dos2(13, 19)
    [mpz(16), mpz(3)]

    LOCATION:
        PyTripTk/util/OfSquares.py

##############################
########## END LIST ##########
##############################

'''

################################################################################
################################################################################
################################################################################

#print(MESSAGE)

__doc__ = MESSAGE

def usage():
    msg = '''\n\tUSAGE:
        >>> from PyTripTk.util import gmp_extensions
        >>> help(gmp_extensions)
    '''
    print(msg)

README=usage()

