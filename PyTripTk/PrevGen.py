#!/usr/bin/env python3

'''
Get the previous generation of a Pythagorean Triple.
There are two (2) established trees:
    Classic (1934)
    Price   (2008)

See:
https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples
    Section: #Pythagorean_triples_by_use_of_matrices_and_linear_transformations

This file contains the following:
    PrevGen            -- (def, tuple) -- Returns a 3-tuple, or an Exception.
    PrevGen_Price      -- (def, tuple) -- Returns a 3-tuple, or an Exception.
    PrevGen_Price_A    -- (def, tuple) -- Returns None, or a 3-tuple (True).
    PrevGen_Price_B    -- (def, tuple) -- Returns None, or a 3-tuple (True).
    PrevGen_Price_C    -- (def, tuple) -- Returns None, or a 3-tuple (True).
    PrevGen_IsPriceA   -- (def, bool)  -- Returns True or Falce; is pytrip Price A.
    PrevGen_IsPriceB   -- (def, bool)  -- Returns True or Falce; is pytrip Price B.
    PrevGen_IsPriceC   -- (def, bool)  -- Returns True or Falce; is pytrip Price C.
    PrevGen_Classic    -- (def, tuple) -- Returns a 3-tuple, or an Exception.
    PrevGen_Classic_A  -- (def, tuple) -- Returns None, or a 3-tuple (True).
    PrevGen_Classic_B  -- (def, tuple) -- Returns None, or a 3-tuple (True).
    PrevGen_Classic_C  -- (def, tuple) -- Returns None, or a 3-tuple (True).
    PrevGen_IsClassicA -- (def, bool)  -- Returns True or Falce; is pytrip Classic A.
    PrevGen_IsClassicB -- (def, bool)  -- Returns True or Falce; is pytrip Classic B.
    PrevGen_IsClassicC -- (def, bool)  -- Returns True or Falce; is pytrip Classic C.

'''

# 3rd Party
import gmpy2 as gmp

# Local (package)
import PyTripTk.PyTrip as pt

def PrevGen_Classic(pytrip=None):
    '''Returns a tuple reprsenting the parent node of a
    Pythagorean Triple in the Classic tree.

    Throws an exceptioin is the branch is not detected.

    '''
    tup = PrevGen_Classic_A(pytrip)
    if not tup is None:
        return tup
    else:
        tup = PrevGen_Classic_B(pytrip)
        if not tup is None:
            return tup
        else:
            tup = PrevGen_Classic_C(pytrip)
            if not tup is None:
                return tup
    errmsg = "\n\t{}\n\tNot a valid Pythagorean Triple!\n"
    errmsg.format(repr(pytrip))
    raise pt.PyTripException(errmsg)

def PrevGen_Classic_A(pytrip=None):
    '''Returns a tuple of the parent node if the input
    is an A branch on the Classic tree. Else, None.

    raises an exception if the input is invalid.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    x, y, z = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a = gmp.mpz((-x)-(2*y)+(2*z))
    b = gmp.mpz((2*x)+y-(2*z))
    c = gmp.mpz((-2*x)-(2*y)+(3*z))
    if a > 0 and b > 0 and c > 0 and a**2+b**2==c**2:
        if a%2 == 0 and b%2 == 1:
            a, b = b, a
        assert c > a and c > b
        return pt.PyTrip(a, b, c)
    return None

def PrevGen_Classic_B(pytrip=None):
    '''Returns a tuple of the parent node if the input
    is a B branch on the Classic tree. Else, None.

    raises an exception if the input is invalid.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    x, y, z = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a = gmp.mpz(x+(2*y)-(2*z))
    b = gmp.mpz((2*x)+y-(2*z))
    c = gmp.mpz((-2*x)-(2*y)+(3*z))
    if a > 0 and b > 0 and c > 0 and a**2+b**2==c**2:
        if a%2 == 0 and b%2 == 1:
            a, b = b, a
        assert c > a and c > b
        return pt.PyTrip(a, b, c)
    return None

def PrevGen_Classic_C(pytrip=None):
    '''Returns a tuple of the parent node if the input
    is a C branch on the Classic tree. Else, None.

    raises an exception if the input is invalid.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    x, y, z = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a = gmp.mpz(x+(2*y)-(2*z))
    b = gmp.mpz((-2*x)-y+(2*z))
    c = gmp.mpz((-2*x)-(2*y)+(3*z))
    if a > 0 and b > 0 and c > 0 and a**2+b**2==c**2:
        if a%2 == 0 and b%2 == 1:
            a, b = b, a
        assert c > a and c > b
        return pt.PyTrip(a, b, c)
    return None

def PrevGen_Price(pytrip=None):
    '''Returns a tuple reprsenting the parent node of a
    Pythagorean Triple in the Price tree.

    Throws an exceptioin is the branch is not detected.

    '''
    tup = PrevGen_Price_A(pytrip)
    if not tup is None:
        return tup
    else:
        tup = PrevGen_Price_B(pytrip)
        if not tup is None:
            return tup
        else:
            tup = PrevGen_Price_C(pytrip)
            if not tup is None:
                return tup
    errmsg = "\n\t{}\n\tNot a valid Pythagorean Triple!\n"
    errmsg.format(repr(pytrip))
    raise pt.PyTripException(errmsg)

def PrevGen_Price_A(pytrip=None):
    '''Returns a tuple of the parent node if the input
    is an A branch on the Price tree. Else, None.

    raises an exception if the input is invalid.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    x, y, z = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a = gmp.f_div(x-y+z, 2)
    b = gmp.f_div(x+(2*y)-z, 4)
    c = gmp.f_div(x-(2*y)+(3*z), 4)
    if a > 0 and b > 0 and c > 0 and a**2+b**2==c**2:
        if a%2 == 0 and b%2 == 1:
            a, b = b, a
        assert c > a and c > b
        return pt.PyTrip(a, b, c)
    return None

def PrevGen_Price_B(pytrip=None):
    '''Returns a tuple of the parent node if the input
    is a B branch on the Price tree. Else, None.

    raises an exception if the input is invalid.

    '''
    assert not pytrip is None
    x, y, z = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a = gmp.f_div(x+y-z, 2)
    b = gmp.f_div(x-(2*y)+z, 4)
    c = gmp.f_div((-x)-(2*y)+(3*z), 4)
    if a > 0 and b > 0 and c > 0 and a**2+b**2==c**2:
        if a%2 == 0 and b%2 == 1:
            a, b = b, a
        assert c > a and c > b
        return pt.PyTrip(a, b, c)
    return None

def PrevGen_Price_C(pytrip=None):
    '''Returns a tuple of the parent node if the input
    is a C branch on the Price tree. Else, None.

    raises an exception if the input is invalid.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    x, y, z = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a = gmp.f_div(x+y-z, 2)
    b = gmp.f_div((-x)+2*y-z, 4)
    c = gmp.f_div((-x)-(2*y)+(3*z), 4)
    if a > 0 and b > 0 and c > 0 and a**2+b**2==c**2:
        if a%2 == 0 and b%2 == 1:
            a, b = b, a
        assert c > a and c > b
        return pt.PyTrip(a, b, c)
    return None

def PrevGen(pytrip=None, which=None):
    '''Returns the parent of a Pythagorean Triple (3-tuple variable).

    Retquires the tree be specified; one of Classic or Price.

    '''
    assert not which is None
    if which == pt.PyTripType.Classic:
        return PrevGen_Classic(pytrip)
    elif which == pt.PyTripType.Price:
        return PrevGen_Price(pytrip)
    else:
        pt.InvalidTreeTypeException()

def PrevGen_IsClassicA(pytrip):
    '''Is the given `pytrip` variable a Classic A branch?
    '''
    return PrevGen_IsClassicABranch(pytrip)

def PrevGen_IsClassicABranch(pytrip):
    '''Is the given `pytrip` variable a Classic A branch?
    '''
    return False if PrevGen_Classic_A(pytrip) is None else True

def PrevGen_IsClassicB(pytrip):
    '''Is the given `pytrip` variable a Classic B branch?
    '''
    return PrevGen_IsClassicBBranch(pytrip)

def PrevGen_IsClassicBBranch(pytrip):
    '''Is the given `pytrip` variable a Classic B branch?
    '''
    return False if PrevGen_Classic_B(pytrip) is None else True

def PrevGen_IsClassicC(pytrip):
    '''Is the given `pytrip` variable a Classic C branch?
    '''
    return PrevGen_IsClassicCBranch(pytrip)

def PrevGen_IsClassicCBranch(pytrip):
    '''Is the given `pytrip` variable a Classic C branch?
    '''
    return False if PrevGen_Classic_C(pytrip) is None else True

def PrevGen_IsPriceA(pytrip):
    '''Is the given `pytrip` variable a Price A branch?
    '''
    return PrevGen_IsPriceABranch(pytrip)

def PrevGen_IsPriceABranch(pytrip):
    '''Is the given `pytrip` variable a Price A branch?
    '''
    return False if PrevGen_Price_A(pytrip) is None else True

def PrevGen_IsPriceB(pytrip):
    '''Is the given `pytrip` variable a Price B branch?
    '''
    return PrevGen_IsPriceBBranch(pytrip)

def PrevGen_IsPriceBBranch(pytrip):
    '''Is the given `pytrip` variable a Price B branch?
    '''
    return False if PrevGen_Price_B(pytrip) is None else True

def PrevGen_IsPriceC(pytrip):
    '''Is the given `pytrip` variable a Price C branch?
    '''
    return PrevGen_IsPriceCBranch(pytrip)

def PrevGen_IsPriceCBranch(pytrip):
    '''Is the given `pytrip` variable a Price C branch?
    '''
    return False if PrevGen_Price_C(pytrip) is None else True

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



