#!/usr/bin/env python3

'''
Get the next generation of a Pythagorean Triple.
There are two (2) established trees:
    Classic (1934)
    Price   (2008)

See:
https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples
    Section: #Pythagorean_triples_by_use_of_matrices_and_linear_transformations

According to the above WikiPedia entry...:
    The Right (C) branch of the Classic (1934) tree increments 3, 5, 7, 9, ...
    This distribution is binary across (left-to-right) in the Price tree ...:
        2**1 --                 3                 -- 2**2
        2**2 --              5 ... 7              -- 2**3
        2**3 --          9, 11 ... 13, 15         -- 2**4
        2**4 -- 17, 19, 21, 23 ... 25, 27, 29, 31 -- 2**5

... the above observations among many very interesting observations.

This file contains the following:
    NextGen            -- (def, tuple) -- Returns a 3-tuple, or 3x3-tuple val.
    NextGen_Price      -- (def, tuple) -- Returns a 3-tuple, or 3x3-tuple val.
    NextGen_Price_A    -- (def, tuple) -- Returns a 3-tuple value.
    NextGen_Price_B    -- (def, tuple) -- Returns a 3-tuple value.
    NextGen_Price_C    -- (def, tuple) -- Returns a 3-tuple value.
    NextGen_Classic    -- (def, tuple) -- Returns a 3-tuple, or 3x3-tuple val.
    NextGen_Classic_A  -- (def, tuple) -- Returns a 3-tuple value.
    NextGen_Classic_B  -- (def, tuple) -- Returns a 3-tuple value.
    NextGen_Classic_C  -- (def, tuple) -- Returns a 3-tuple value.
    PyTripNextGenTup   -- (namedtuple) -- A tuple of tuples (3x3).

'''

from collections import namedtuple as nt

# 3rd Party
import gmpy2 as gmp

# Local (package)
import PyTripTk.PyTrip as pt

PyTripNextGenTup=nt('PyTripNextGenTup', ['A', 'B', 'C'])

def NextGen_Classic(pytrip=None, branch=None):
    '''Returns the next generation of a primitive \
    Pythagorean Triple, using the Classic tree (1934).

    If one of A, B, or C branches are specified \
    that branch is returned as a 3-tuple variable.

    Else, a 3-tuple of ordered A, B, and C are \
    returned, each having 3-tuple variables.

    `pytrip` must be of type PyTrip.PyTrip (namedtuple).
    `branch` must be of type PyTrip.PyTripBranch (enum).

    '''
    if branch is None:
        return PyTripNextGenTup(
                NextGen_Classic_A(pytrip),
                NextGen_Classic_B(pytrip),
                NextGen_Classic_C(pytrip))
    elif branch == pt.PyTripBranch.A:
        return NextGen_Classic_A(pytrip)
    elif branch == pt.PyTripBranch.B:
        return NextGen_Classic_B(pytrip)
    elif branch == pt.PyTripBranch.C:
        return NextGen_Classic_C(pytrip)
    else:
        pt.InvalidBranchException()

def NextGen_Classic_A(pytrip=None):
    '''Returns a 3-tuple variable of the Left (A) branch, \
    of a Pythagorean Triple using the Classic tree (1934).
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    x = gmp.mpz((-a)+(2*b)+(2*c))
    y = gmp.mpz((-2*a)+b+(2*c))
    z = gmp.mpz((-2*a)+(2*b)+(3*c))
    if x**2 + y**2 == z**2:
        if x%2 == 0 and y%2 == 1:
            x, y = y, x
        assert z > x and z > y
        return pt.PyTrip(x, y, z)
    return None

def NextGen_Classic_B(pytrip=None):
    '''Returns a 3-tuple variable of the Middle (B) branch, \
    of a Pythagorean Triple using the Classic tree (1934).
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    x = gmp.mpz(a+(2*b)+(2*c))
    y = gmp.mpz((2*a)+b+(2*c))
    z = gmp.mpz((2*a)+(2*b)+(3*c))
    if x**2 + y**2 == z**2:
        if x%2 == 0 and y%2 == 1:
            x, y = y, x
        assert z > x and z > y
        return pt.PyTrip(x, y, z)
    return None

def NextGen_Classic_C(pytrip=None):
    '''Returns a 3-tuple variable of the Right (C) branch, \
    of a Pythagorean Triple using the Classic tree (1934).
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    x = gmp.mpz(a-(2*b)+(2*c))
    y = gmp.mpz((2*a)-b+(2*c))
    z = gmp.mpz((2*a)-(2*b)+(3*c))
    if x**2 + y**2 == z**2:
        if x%2 == 0 and y%2 == 1:
            x, y = y, x
        assert z > x and z > y
        return pt.PyTrip(x, y, z)
    return None

def NextGen_Price(pytrip=None, branch=None):
    '''Returns the next generation of a primitive \
    Pythagorean Triple, using Price (2008).

    If one of A, B, or C branches are specified \
    that branch is returned as a 3-tuple variable.

    Else, a 3-tuple of ordered A, B, and C are \
    returned, each having 3-tuple variables.

    `pytrip` must be of type PyTrip.PyTrip (namedtuple).
    `branch` must be of type PyTrip.PyTripBranch (enum).

    '''
    assert not pytrip is None
    if branch is None:
        return PyTripNextGenTup(
                NextGen_Price_A(pytrip),
                NextGen_Price_B(pytrip),
                NextGen_Price_C(pytrip))
    elif branch == pt.PyTripBranch.A:
        return NextGen_Price_A(pytrip)
    elif branch == pt.PyTripBranch.B:
        return NextGen_Price_B(pytrip)
    elif branch == pt.PyTripBranch.C:
        return NextGen_Price_C(pytrip)
    else:
        pt.InvalidBranchException()

def NextGen_Price_A(pytrip=None):
    '''Returns a 3-tuple variable of the Left (A) branch, \
    of a Pythagorean Triple using the Price tree (2008).
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    x = gmp.mpz((2*a)+b-c)
    y = gmp.mpz((-2*a)+(2*b)+(2*c))
    z = gmp.mpz((-2*a)+b+(3*c))
    if x**2 + y**2 == z**2:
        if x%2 == 0 and y%2 == 1:
            x, y = y, x
        assert z > x and z > y
        return pt.PyTrip(x, y, z)
    return None

def NextGen_Price_B(pytrip=None):
    '''Returns a 3-tuple variable of the Middle (B) branch, \
    of a Pythagorean Triple using the Price tree (2008).
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    x = gmp.mpz((2*a)+b+c)
    y = gmp.mpz((2*a)-2*b+(2*c))
    z = gmp.mpz((2*a)-b+(3*c))
    if x**2 + y**2 == z**2:
        if x%2 == 0 and y%2 == 1:
            x, y = y, x
        assert z > x and z > y
        return pt.PyTrip(x, y, z)
    return None

def NextGen_Price_C(pytrip=None):
    '''Returns a 3-tuple variable of the Right (C) branch, \
    of a Pythagorean Triple using the Price tree (2008).
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    a, b, c = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    x = gmp.mpz((2*a)-b+c)
    y = gmp.mpz((2*a)+(2*b)+(2*c))
    z = gmp.mpz((2*a)+b+(3*c))
    if x**2 + y**2 == z**2:
        if x%2 == 0 and y%2 == 1:
            x, y = y, x
        assert z > x and z > y
        return pt.PyTrip(x, y, z)
    return None

def NextGen(pytrip=None, which=None, branch=None):
    '''Returns the next generation of a Pythagorean Triple,
    given which tree to use, and the branch of that tree.

    The branch may be excluded (None). If the branch is not specified,
    all 3 of the children are returned, each being 3-tuple values.

    Which tree to use is required, else an exception is thrown.

    '''
    assert not which is None
    assert not pytrip is None
    if which == pt.PyTripType.Classic:
        return NextGen_Classic(pytrip, branch)
    elif which == pt.PyTripType.Price:
        return NextGen_Price(pytrip, branch)
    else:
        pt.InvalidTreeTypeException()

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



