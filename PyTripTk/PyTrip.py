#!/usr/bin/env python3

'''
This file contains rudimentary basics for the rest of the package.
See: https://en.wikipedia.org/wiki/Pythagorean_triple

This file contains the following:
    PyTrip                   -- (namedtuple)  --
    ROOT                     -- (global var)  -- PyTrip(mpz(3), mpz(4), mpz(5))
    Root                     -- (def, pytrip) -- PyTrip(mpz(3), mpz(4), mpz(5))
    PyTripType               -- (enum)        -- Classic or Price
    PyTripBranch             -- (enum)        -- A, B or C (by Type, or Tree)
    MakePyTrip               -- (def, pytrip) -- Makes a (primitive) PyTrip of 3 input vars.
    MpzPyTrip                -- (def, pytrip) -- Ensures vars are of type MPZ `gmp.mpz(var)`.
    MakePrimitive            -- (def, pytrip) -- Makes a PyTrip Primitive if it is not.
    IsPyTrip                 -- (def, bool)   -- Is Pythagorean Triple?
    IsPrimitivePyTrip        -- (def, bool)   -- Is Primitive Pythagorean Triple?
    PyTripException          -- (exception)   -- General, package-level.
    PyTripRootException      -- (exception)   -- Specific: If Parent is called on Root.
    InvalidBranchException   -- (exception)   -- A, B or C Enum
    InvalidTreeTypeException -- (exception)   -- Price or Classic Enum

===============================
========== PRIMITIVE ==========
===============================
-
 | This package is intended to work primarily with PRIMITIVE Pythagorean
 | Triples. This package does not guarantee that any non-primitive will not
 | otherwise be reduced to primitive form. For example, you could manually
 | specify a `PyTrip` variable which is a valid Pythagorean Triple, but not
 | primitive. However, it may be turned into its primitive form.
-
 | The `PrevGen` (algorithms) and Reduce part of `ReduceRegen` portions of
 | this package will simply not work if an input PyTrip is not primitive.
-
 | Further along, such as using `EulerBrick` the `abd` triple is always
 | primitive if the input is primitive. If the input is not primitive, `abd`
 | will be reduced to primitive, as will `ace` and `bcf` triples, and the
 | input will be reduced to primitive form as well.
-
 | If you need to work exclusively or primarily with non-primitive PT, then,
 | as in the GPL license for this software, you are free to modify it for
 | your needs, and good documentation standards (such as this) should clearly
 | outline the differences. Or you could force this software (though a lot of
 | effort an manual work-arounds) to handle non-primitive forms.
-
 | Pull requests modifying the source to handle non-primitive forms easier
 | will not be considered (branch, fork, have fun).
-
===============================

===========================
========== ORDER ==========
===========================
-
 | The general definition of a Pythagorean Triple is `a**2+b**2==c**2`
-
 | Some algorithmic definitions call for `a < b < c`
-
 | This software, however, maps the following PRIMITIVE form instead:
 |     a%2 == 1
 |     b%2 == 0
 |     c%4 == 1
 |     c   >  a
 |     c   >  b
 | Thus, `a` can be greater than or less than `b`, and
 | `c` is always `1 mod 4`, and greater than `a` or `b`.
-
 | In simpler terms: Do Not Assume `a < b < c`
 | No requests to change this will be considered.
 | The reason for this is algorithmic simplicity.
 | Detecting which of {a|b}%2 == {0|1} adds a lot of unnecessary code.
-
 | If you feed a manual PyTrip(a < b < c), such that a%2==0 and b%2==1,
 | and something such as `PrevGen` does not work, or `NextGen` outputs
 | something funky -- this is expected behavior.
 | Fix the PyTrip and try again.
-
 | Additional Note:
 | The `APS`, or Arithmetic Progression of Squares, part of this package,
 | with the abbreviations `p` `q` and `r` follow the `p < q < r` convention.
-
===========================

==============================
========== FAILURES ==========
==============================
-
 | This package raises assertion errors without being caught. This behavior
 | is intended and expected. Assertion errors should explain themselves, via
 | basic maths. Feedback is welcome if an unexpected failure happens which
 | should be caught and handled opposed to failing; or where a simple
 | workaround may suffice to ensure consistent (basic mathematical) working.
-
==============================

=============================
========== LICENSE ==========
=============================
-
 | This software is licensed GNU GPL v3.
 | It is provided WITHOUT WARRANTY.
 | You may modify this software.
 | You may redistribute this software.
 | Redistribution requires inclusion of this copyright notice,
 |     and a copy of the original source code.
 | This license (GNU GPL v3) should accompany
 |     any modifications or redistributions.
 |     https://www.gnu.org/licenses/gpl-3.0.en.html
-
 | Since this software is based on basic math, you
 | may consider any open-source license compatible.
 | For example: WTFPL
-
 | This software relies upon Python.
 |     Take into account the Python License.
 |     https://docs.python.org/3/license.html
 |     https://python.org
-
 | This software relies on GNU MP (or, GMP).
 |     Take into account the GNU MP Licenses (dual).
 |     https://www.gnu.org/licenses/lgpl.html
 |     https://www.gnu.org/licenses/gpl-2.0.html
 |     https://gmplib.org/
-
 | This software relies upon Gmpy2 (python module)
 |     Take into account the Gmpy2 License.
 |     https://www.gnu.org/licenses/lgpl-3.0.en.html
 |     https://pypi.python.org/pypi/gmpy2/2.0.0
 |     https://github.com/aleaxit/gmpy
-
=============================

'''

# Python Library
from collections import namedtuple as nt
from enum import Enum

# 3rd Party (gmp is required)
import gmpy2 as gmp

MPZT=type(gmp.mpz(1))

PyTrip=nt('PyTrip', ['a', 'b', 'c'])
ROOT=PyTrip(gmp.mpz(3), gmp.mpz(4), gmp.mpz(5))

def Root():
    '''The root of the Primitive Pythagoran Triple Tree
    is the same for both the Classic and Price Trees,
    at (3, 4, 5).
    '''
    return PyTrip(gmp.mpz(3), gmp.mpz(4), gmp.mpz(5))

class PyTripType(Enum):
    '''There are two known Pythagorean Triple trees:

    Classic (Berggren, B   -- 1934)
    Price   (Price, H. Lee -- 2008)

    Branches should be specified BY TREE to avoid ambiguity.

    The `PyTripType.Both` is a special application, whereby
    two branches reference the exact same location in either tree.
    Such as, or, for example: (39, 80, 89) which is "0AC" in
    both the Classic and Price trees (same location with
    different algorithmic interpretations).

    '''
    Classic = 0
    Price = 1
    Both = 2

class PyTripBranch(Enum):
    '''Branches are:

    Left   (A) 0
    Middle (B) 1
    Right  (C) 2

    These branches can be different PER TREE.
    To avoid ambiguity, a branch should always be associated
    with a specific tree; either "Classic" or "Price"

    '''
    A = 0
    B = 1
    C = 2

class PyTripForms(Enum):
    '''Primary forms

    Form1: (Classic C branch)
        a = 2n+1
        b = 2n(n+1)
        c = 2n(n+1)+1

    Form2: (Classic A branch)
        a = 4(n**2)-1
        b = 4n
        c = 4(n**2)+1

    Form3: (Classic B branch)
        If 3^Even Children:
            a = b-1
            b = a+1
            c = sqrt(a^2+b^2)
        If 3^Odd Children:
            a = b+1
            b = a-1
            c = sqrt(a^2+b^2)
    '''
    Form1 = 1
    Form2 = 2
    Form3 = 3

class PyTripPxCy(Enum):
    P0C0 = 0 # Special instance @ ROOT
    PaCa = 1
    PaCb = 2
    PaCc = 3
    PbCa = 4
    PbCb = 5
    PbCc = 6 # None
    PcCa = 7 # None
    PcCb = 8 # None
    PcCc = 9

class PyTripVar(Enum):
    '''
    '''
    ToDo = -1 # raise NotImplementedError
    Neg = 0   # Standard -- False
    Pos = 1   # Standard -- True
    UNK = 2   # raise|return PyTripFormalException(info)

class PyTripException(Exception):
    '''A basic exception class for the package (top-level).
    '''
    pass

class PyTripRootException(PyTripException):
    '''A specialized exception to be called when Parent is
    called or requested from the Root at (3, 4, 5).
    '''
    def __init__(self):
        super(PyTripException, self).__init__("(3, 4, 5) is Root (no parent)")

class PyTripFormalException(PyTripException):
    '''A specialized exception to be called when formal validation
    of a routine based on heuristic observations or arguments is
    incomplete, unavailable or otherwise possibly unsound.
    '''
    def __init__(self, info=None):
        msg = "Unhandled Heuristic Code. "
        msg += "\nTerms are not formally validated. "
        msg += "\nToDo: Expand upon this custom error/exception "
        msg += "\n\t ... to handle a [data] component."
        if info is None:
            super(PyTripException, self).__init__(msg)
        else:
            super(PyTripException, self).__init__(msg, info)

def InvalidBranchException():
    '''Method to raise a generalized next-gen branch exception.
    This is a method, not an exception class.
    It raises the base package exception, `PyTripException`
    '''
    msg = "Invalid branch, must be one of:\n"
    msg += "\tLeft (A),\n"
    msg += "\tMiddle (B),\n"
    msg += "\tRight (C)"
    raise PyTripException(msg)

def InvalidTreeTypeException():
    '''Method to raise a generalized tree exception.
    This is a method, not an exception class.
    It raises the base package exception, `PyTripException`
    '''
    msg = "You must specify which tree to use"
    msg += "\n\t(Price or Classic)."
    raise PyTripException(msg)

def MakePyTrip(a, b, c):
    '''Makes a Primitive Pythagorean Triple.
    A and B inputs may be unordered. However,
    the output will map a%2==1 and b%2==0.
    The C input should always be greater than A or B.
    If C is not greater than A or B, or A == B, an exception is thrown.
    '''
    try:
        a, b, c = gmp.mpz(a), gmp.mpz(b), gmp.mpz(c)
        assert not a is None and not b is None and not c is None, "Variables cannot be None."
        assert int(a) >= 3 and int(b) >= 4 and int(c) >= 5, "Minimum: (3, 4, 5), Given ({}, {}, {})".format(a, b, c)
        assert a < c and b < c and a != b, "Invalid Pythagorean Triple components; a<c, b<c, a!=b, a(mod)2!=b(mod)2"
        assert a**2+b**2 == c**2, "Invalid Pythagorean Triple components; a**2+b**2 != c**2"
        if a**2 + b**2 == c**2:
            if a%2 == 0 and b%2 == 1:
                a, b = b, a
            ret = MakePrimitive(PyTrip(gmp.mpz(a), gmp.mpz(b), gmp.mpz(c)))
            return ret
        else:
            msg = "Inputs, ({}, {}, {}) ".format(a, b, c)
            msg += "do not form a valid Pythagorean triple."
            raise PyTripException(msg)
    except AssertionError as ae:
        print("Given values:\n\ta={}\n\tb={}\n\tc={}".format(a, b, c))
        raise ae
    except:
        msg = "Could not make Pythagorean Triple from "
        msg += "({}, {}, {})".format(a, b, c)
        raise PyTripException(msg)

def PyTrip_FromN(N):
    '''Returns a Form 1 PyTrip from a given odd N >= 3.

    Form 1:
        a = N (input)
        a = 2n+1
        b = 2n(n+1)
        c = 2n(n+1)+1
    '''
    assert N%2 == 1 and N >= 3
    n = gmp.f_div(N-1, 2)
    a, b, c = 2*n+1, 2*n*(n+1), 2*n*(n+1)+1
    t = PyTrip(a, b, c)
    assert IsPrimitivePyTrip(t)
    return t

def PyTrip_FromFacts(f1, f2, make_primitive=True, not_primitive=False):
    '''Returns a Pythagorean Triple from 2 odd factors.

    Roughly equivalent to using a FibBox.
    '''
    assert f1%2 == 1 and f2%2 == 1 and f1 != f2
    if f1 > f2:
        f1, f2 = f2, f1
    assert f1 >= 1 and f2 >= 3
    ctr, dif = gmp.f_div(f2+f1, 2), gmp.f_div(f2-f1, 2)
    a, b, c = gmp.mpz(f1*f2), 2*ctr*dif, ctr**2+dif**2
    t = PyTrip(a, b, c)
    try:
        assert IsPrimitivePyTrip(t)
    except AssertionError as ae:
        if make_primitive:
            t = MakePrimitive(t)
            assert IsPrimitivePyTrip(t)
        elif not_primitive:
            return t
        else:
            gcd = gmp.gcd(t[0], gmp.gcd(t[1], t[2]))
            print("%s is not primitive, gcd=%s" % (repr(t), gcd))
    return t

def MpzPyTrip(pytrip=None):
    '''Ensures the variables are of type gmp.mpz.
    If the input variables are not of type gmp.mpz,
    returns a new instance as such.
    '''
    assert not pytrip is None
    assert isinstance(pytrip, PyTrip)
    if not isinstance(pytrip[0], MPZT) or not isinstance(pytrip[1], MPZT) or not isinstance(pytrip[2], MPZT):
        return MakePyTrip(pytrip[0], pytrip[1], pytrip[2])
    else:
        return pytrip

def IsPyTrip(pytrip=None):
    '''Returns True if a**2 + b**2 == c**2,
    and isinstance(`PyTripTk.PyTrip.PyTrip`).

    Else False.

    '''
    try:
        assert not pytrip is None
        assert isinstance(pytrip, PyTrip)
        assert pytrip[0]**2 + pytrip[1]**2 == pytrip[2]**2
        return True
    except AssertionError as ae:
        return False
    except Exception as ex:
        raise ex

def IsPrimitivePyTrip(pytrip=None):
    '''Returns True if a**2 + b**2 == c**2 and gcd(a,b,c) == 1,
    among other checks and assurances.

    Else False.

    '''
    try:
        assert not pytrip is None
        assert isinstance(pytrip, PyTrip)
        if IsPyTrip(pytrip):
            a, b, c = pytrip[0], pytrip[1], pytrip[2]
            gcd = gmp.gcd(a, gmp.gcd(b, c))
            # A primitive PT always has GCD==1
            assert gcd == 1
            # c must be 1 mod 4
            assert c%4 == 1
            # Only one of a or b can be 0 mod 3
            assert a%3 != b%3 and (a%3 == 0 or b%3 == 0)
            # We want b to always be 0 mod 4
            assert b%4 == 0
            # Only one of a, b, c can be 0 mod 5
            assert sum([a%5==0, b%5==0, c%5==0]) == 1
            # if all of the basic checks pass, return true.
            return True
        # fail-over if it is not even a PT
        return False
    except AssertionError as ae:
        # if any of the basic checks fail, return false.
        return False
    except Exception as ex:
        raise ex

def MakePrimitive(pytrip=None):
    assert not pytrip is None
    assert isinstance(pytrip, PyTrip)
    gcd = gmp.gcd(pytrip[0], gmp.gcd(pytrip[1], pytrip[2]))
    if gcd != 1:
        a = gmp.f_div(pytrip[0], gcd)
        b = gmp.f_div(pytrip[1], gcd)
        c = gmp.f_div(pytrip[2], gcd)
        if a**2+b**2 == c**2:
            if a%2 == 0 and b%2 == 1:
                a, b = b, a
            assert c > a and c > b
            return PyTrip(a, b, c)
    else:
        return pytrip

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



