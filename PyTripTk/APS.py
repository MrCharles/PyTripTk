#!/usr/bin/env python3

'''
Handles Arithmetic Progression of Squares (APS)
considerations for Pythagorean Triples.

See: http://math.ucr.edu/~res/math153/history07b.pdf
    Specifically: "An alternate approach" (pages 5 to 7, of 7)

'''

# Python Library
from collections import namedtuple as nt

# 3rd Party
import gmpy2 as gmp

# Local Package
import PyTripTk.PyTrip as pt

PyTripAPS=nt('PyTripAPS', ['p', 'q', 'r', 'abc'])
APSADDOC = "Represents an Arithmetic Progression of Squares, "
APSADDOC += "\n\tas it pertains to a Pythagorean Triple."
APSADDOC += "\n\tThe APS (p, q, r) should be p < q < r,"
APSADDOC += "\n\tWhereas, the 'a' and 'b' elements of PT(a, b, c)"
APSADDOC += "\n\tmay be unordered, such that a > b or b < a, but "
APSADDOC += "\n\tc is always > {a|b}."

class APS:
    '''Arithmetic Progression of Squares.

    This class represents extraneous development and is UNTESTED.

    The terms (variables) used in this class mirror those found in,
        supplementary materials for a UC Riverside, Math 153 (Math History) course...
        http://math.ucr.edu/~res/math153/history07b.pdf

    The terms (variables) in this class are different from the rest of
    the PyTripTk library's terminology. This class should be used with
    some degree of CAUTION, not only because it is UNTESTED, but due to
    possible confusion of variables.

    (SEE THE DOCUMENTATION!)
    (READ THE PAPER!)

    For example, PyTripTk designates (a, b, c) as a Pythagorean Triple.
    Therein, the definition used in the PyTripTk software is a%2 == 1, b%2 == 0,
        such that a**2+b**2==c**2 and c%4==1
    This class uses simliar nomenclature for an Arithmetic Progression of Squares,
    (a, b, c) -- BUT, this class defines c**2-b**2==b**2-a**2 and a < b < c.

    Similarly, the PyTripTk (a, b, c) would map to the (x, y, z) in this class,
    in keeping with the terms and variables used in the referenced paper.

    However:
        a in PyTripTk (a, b, c) could be y instead of x in APS (x, y, z)
        b in PyTripTk (a, b, c) could be x instead of y in APS (x, y, z)

    Since the concept of an Arithmetic Progression of Squares is also based on
    two multiplicative variables leading to a centered difference of squares,
    these terms are included and translated. This class uses (m, n) terms or
    variables, such that m < n; which in turn map to m=F1, and n=F2 in a
    Fibonacci Box. (And Center/Difference of Squares can be calculated from there.)

    This class was developed AFTER the PyTripAPS named-tuple.
    The PyTripAPS named-tuple uses (p, q, r) with p < q < r to designate the
    same as (a, b, c) in this class.

    NOTES:
        TODO: The latter PyTripAPS(p, q, r) should probably be fixed.

    '''

    @property
    def a(self):
        '''a in (a, b, c)
        a < b < c
        c**2-b**2 == b**2-a**2
        '''
        return self._a

    @property
    def b(self):
        '''b in (a, b, c)
        a < b < c
        c**2-b**2 == b**2-a**2
        '''
        return self._b

    @property
    def c(self):
        '''c in (a, b, c)
        a < b < c
        c**2-b**2 == b**2-a**2
        '''
        return self._c

    @property
    def m(self):
        '''m in (m, n)
        m < n
        a = (m**2+2*m*n-n**2)/2
        b = (m**2+n**2)/2
        c = (n**2+2*m*n-m**2)/2
        '''
        return self._m

    @property
    def n(self):
        '''n in (m, n)
        m < n
        a = (m**2+2*m*n-n**2)/2
        b = (m**2+n**2)/2
        c = (n**2+2*m*n-m**2)/2
        '''
        return self._n

    @property
    def F1(self):
        '''Maps to the F1 variable of a Pythagorean Triple's
        Fibonacci Box

        F1 < D < C < F2 | (F1+F2)/2 == C && (F2-F1)/2 == D
        F1 == m (this class)
        '''
        return self.m

    @property
    def DIF(self):
        '''Maps to the D variable of a Pythagorean Triple's
        Fibonacci Box

        F1 < D < C < F2 | (F1+F2)/2 == C && (F2-F1)/2 == D
        Supports F1 and F2
        '''
        return self._dif

    @property
    def CTR(self):
        '''Maps to the D variable of a Pythagorean Triple's
        Fibonacci Box

        F1 < D < C < F2 | (F1+F2)/2 == C && (F2-F1)/2 == D
        Supports F1 and F2
        '''
        return self._ctr

    @property
    def F2(self):
        '''Maps to the F1 variable of a Pythagorean Triple's
        Fibonacci Box

        F1 < D < C < F2 | (F1+F2)/2 == C && (F2-F1)/2 == D
        F2 == n (this class)
        '''
        return self.n

    @property
    def p(self):
        '''The number of even integers from 2a+1 to 2b-1
        p in (p, q, r)
        p = b-a
        p = q*r
        '''
        return self._p

    @property
    def q(self):
        '''The number of odd integers from 2b+1 to 2c-1
        q in (p, q, r)
        q = c-b
        q = p/r
        '''
        return self._q

    @property
    def r(self):
        '''The ratio of p to q.
        r in (p, q, r)
        r = p/q
        1-sqrt(2) <= r <= 1+sqrt(2)
        '''
        return self._r

    @property
    def x(self):
        '''The smallest (minimal) value in a Pythagorean Triple.
        x in (x, y, z)
        x < y < z
        x**2+y**2==z**2
        x = (c-abs(a))/2
        '''
        return self._x

    @property
    def y(self):
        '''The middle value in a Pythagorean Triple.
        y in (x, y, z)
        x < y < z
        x**2+y**2==z**2
        y = (c+abs(a))/2
        '''
        return self._y

    @property
    def z(self):
        '''The largest (maximal) value in a Pythagorean Triple.
        z in (x, y, z)
        x < y < z
        x**2+y**2==z**2
        z = b
        '''
        return self._z

    def __init__(self,
            a=None, b=None, c=None,
            m=None, n=None,
            p=None, q=None, r=None,
            x=None, y=None, z=None):
        # Basic test setup
        tst1_abc = (not a is None and not b is None and not c is None)
        tst2_mn  = (not m is None and not n is None)
        tst3_pqr = (not p is None and not q is None and not r is None)
        tst4_xyz = (not x is None and not y is None and not z is None)
        # Check tests
        msg = "No Variables? No class. Please be more classy. Have some class. (RTFM)"
        assert any([tst1_abc, tst2_mn, tst3_pqr, tst4_xyz]), msg
        # Instantiate local variables
        self._a, self._b, self._c = None, None, None
        self._m, self._n = None, None
        self._ctr, self._dif = None, None # Supports m, n
        self._p, self._q, self._r = None, None, None
        self._x, self._y, self._z = None, None, None
        # Configure class based on input variables
        if tst1_abc:
            a, b, c = gmp.mpz(a), gmp.mpz(b), gmp.mpz(c)
            # Set GMP precision if not appropriate (for the ratio) -- Arbitrary
            if gmp.get_context().precision < (c**2).bit_length()*4:
                gmp.get_context().precision = (c**2).bit_length()*4
            if a > b:
                a, b = b, a
            msg = "(a, b, c) not an arithmetic progression of squares"
            assert a < b and b < c, msg
            assert c**2-b**2 == b**2-a**2, msg
            self._a, self._b, self._c = a, b, c
            self._init_abc_()
        elif tst2_mn:
            m, n = gmp.mpz(m), gmp.mpz(n)
            # Set GMP precision if not appropriate (for the ratio) -- Standard
            if gmp.get_context().precision < (m*n).bit_length()*8:
                gmp.get_context().precision = (m*n).bit_length()*8
            if m > n:
                m, n = n, m
            if (m*n)%2 == 0:
                raise NotImplementedError("Not implemented for even numbers.")
            a = gmp.f_div(m**2+2*m*n-n**2, 2)
            b = gmp.f_div(m**2+n**2, 2)
            c = gmp.f_div(n**2+2*m*n-m**2, 2)
            if a > b:
                a, b = b, a
            msg = "(m, n) does not form an arithmetic progression of squares (a, b, c)"
            assert a < b and b < c, msg
            assert c**2-b**2 == b**2-a**2, msg
            self._a, self._b, self._c = a, b, c
            self._m, self._n = m, n
            self._init_mn_()
        elif tst3_pqr:
            p, q, r = gmp.mpz(p), gmp.mpz(q), gmp.mpfr(r)
            # Set GMP precision if not appropriate (for the ratio) -- Arbitrary
            if gmp.get_context().precision < (p*q).bit_length()*8:
                gmp.get_context().precision = (p*q).bit_length()*8
            assert p/q >= 1-gmp.sqrt(2) and p/q <= 1+gmp.sqrt(2), "Invalid ratio!"
            self._p, self._q = p, q
            self._r = p/q
            self._init_pqr_() # TODO: Implement last (maybe)
        elif tst4_xyz:
            x, y, z = gmp.mpz(x), gmp.mpz(y), gmp.mpz(z)
            # Set GMP precision if not appropriate (for the ratio) -- Standard
            if gmp.get_context().precision < (z).bit_length()*8:
                gmp.get_context().precision = (z).bit_length()*8
            msg = "(x, y, z) are not a valid Pythagorean Triple (no arithmetic progression of squares)"
            assert x**2+y**2==z**2, msg
            if x > y:
                x, y = y, x
            assert x < y and y < z, msg
            gcd = gmp.gcd(x, gmp.gcd(y, z))
            msg = "(x, y, z) should form a primitive Pythagorean Triple, gcd!=1, gcd=%s" % (gcd)
            assert gcd == 1, msg
            msg = "(x, y, z) should form a primitive Pythagorean Triple, x(mod)2==y(mod)2 or z(mod)4!=1"
            assert x%2 != y%2 and z%4 == 1, msg
            self._x, self._y, self._z = x, y, z
            self._init_xyz_()
        else:
            raise Exception("This should never happen!")

    def _init_abc_(self):
        assert (not self._a is None) and (not self._b is None) and (not self._c is None)
        assert (self._m is None) == (self._n is None)
        if (self._m is None) and (self._n is None):
            n = gmp.iroot_rem(gmp.f_div(2*self._b+self._c+self._a, 2), 2)[0]
            m = gmp.iroot_rem(gmp.f_div(2*self._b+self._a-self._c, 2), 2)[0]
            msg = "(a, b, c) not an arithmetic progression of squares"
            #assert n[1] == 0 and m[1] == 0 and m[0] != n[0], msg+("\n\t%s\n\t%s" % (m, n))
            #m, n = m[0], n[0]
            if (m*n)%2 == 0:
                raise NotImplementedError("Not implemented for even numbers.")
            if m > n:
                m, n = n, m
            assert self._c == gmp.f_div(n**2+2*m*n-m**2, 2), msg
            ta, tb = gmp.f_div(m**2+2*m*n-n**2, 2), gmp.f_div(m**2+n**2, 2)
            assert ta != tb and self._b in [ta, tb] and self._a in [ta, tb], msg
            self._m, self._n = m, n
        assert ((self._x is None) == (self._y is None)) and ((self._y is None) == (self._z is None))
        if self._x is None and self._y is None and self._z is None:
            assert not self._m is None and not self._n is None
            msg = "Bad initialization!"
            assert not self._m is None and not self._n is None, msg
            self._x = self._m*self._n
            self._ctr = gmp.f_div(self._m+self._n, 2)
            self._dif = gmp.f_div(self._n-self._m, 2)
            msg = "Bad programming!"
            assert self._ctr%2 != self._dif%2
            assert self._x == self._ctr**2-self._dif**2, msg
            self._y = 2*self._ctr*self._dif
            if self._x > self._y:
                self._x, self._y = self._y, self._x
            self._z = self._ctr**2+self._dif**2
            msg = "(a, b, c) not an arithmetic progression of squares"
            assert self._x**2+self._y**2 == self._z**2
        assert ((self._p is None) == (self._q is None)) and ((self._q is None) == (self._r is None))
        if self._p is None and self._q is None and self._r is None:
            assert not self._a is None and not self._b is None and not self._c is None
            q = self._c-self._b
            p = self._b-self._a
            r = p/q
            if r > 1+gmp.sqrt(2) or r < 1-gmp.sqrt(2):
                p = self._b+self._a
                r = p/q
            assert p/q >= 1-gmp.sqrt(2) and p/q <= 1+gmp.sqrt(2), "Invalid ratio!"
            assert r <= 1+gmp.sqrt(2) and r >= 1-gmp.sqrt(2), "Invalid ratio!"
            self._p, self._q, self._r = p, q, r

    def _init_mn_(self):
        assert not self._m is None and not self._n is None
        assert not self._a is None and not self._b is None and not self._c is None
        self._init_abc_()

    def _init_pqr_(self):
        assert not self._p is None and not self._q is None and not self._r is None
        denom = 2*(self._r-1)
        a = gmp.mpz(self._q*((1+2*self._r-self._r**2)/denom))
        b = gmp.mpz(self._q*((self._r**2+1)/denom))
        c = gmp.mpz(self._q*((self._r**2+2*self._r-1)/denom))
        if a > b:
            a, b = b, a
        msg = "(p, q, r) does not form an arithmetic progression of squares (a, b, c)"
        assert a < b and b < c, msg
        assert c**2-b**2 == b**2-a**2, msg
        self._a, self._b, self._c = a, b, c
        self._init_abc_()

    def _init_xyz_(self):
        assert not self._x is None and not self._y is None and not self._z is None
        if self._x%2 == 1:
            ctr = gmp.iroot_rem(gmp.f_div(self._z+self._x, 2), 2)
        else:
            assert self._y%2 == 1, "x%2!=y%2"
            ctr = gmp.iroot_rem(gmp.f_div(self._z+self._y, 2), 2)
        msg = "(x, y, z) could not define centered difference of (m, n) values"
        assert ctr[1] == 0, msg
        ctr = ctr[0]
        dif = gmp.iroot_rem(ctr**2-(self._y if self._y%2 == 1 else self._x), 2)
        assert dif[1] == 0, msg
        dif = dif[0]
        m, n = ctr-dif, ctr+dif
        if self._x%2 == 1:
            assert ctr**2-dif**2 == self._x, msg
            assert m*n == self._x, msg
            assert 2*ctr*dif == self._y, msg
        else:
            assert self._y%2 == 1, "x%2!=y%2"
            assert ctr**2-dif**2 == self._y, msg
            assert m*n == self._y, msg
            assert 2*ctr*dif == self._x, msg
        assert ctr**2+dif**2 == self._z
        self._m, self._n = m, n
        self._ctr, self._dif = ctr, dif
        a = gmp.f_div(m**2+2*m*n-n**2, 2)
        b = gmp.f_div(m**2+n**2, 2)
        c = gmp.f_div(n**2+2*m*n-m**2, 2)
        if a > b:
            a, b = b, a
        msg = "(m, n) does not form an arithmetic progression of squares (a, b, c)"
        assert a < b and b < c, msg
        assert c**2-b**2 == b**2-a**2, msg
        self._a, self._b, self._c = a, b, c
        self._init_abc_()

def APS_FromPT(pytrip=None):
    '''Returns an Arithmetic Progression of Squares (APS)
    from a given Pythagorean Triple
    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    p = gmp.mpz(pytrip[1]-pytrip[0])
    q = gmp.mpz(pytrip[2])
    r = gmp.mpz(pytrip[1]+pytrip[0])
    if p >= q: p = -p
    assert r**2-q**2 == q**2-p**2
    return PyTripAPS(p, q, r, pytrip)

def PT_ToAPS(pytrip=None):
    '''Proxy method for `APS_FromPT`
    '''
    return APS_FromPT(pytrip)

def APS_ToPT(aps=None):
    '''Proxy method for `PT_FromAPS`
    '''
    return PT_FromAPS(aps)

def PT_FromAPS(aps=None):
    '''Returns a Pythagorean Triple from a given
    Arithmetic Progression of Squares.

    This does not rely on the input aps.abc variable!

    Calculations are done from (p, q, r)

    '''
    assert not aps is None
    assert isinstance(aps, PyTripAPS)
    assert aps.p < aps.q and aps.q < aps.r
    a, b, c = gmp.f_div(aps[2]-aps[0], 2), gmp.f_div(aps[2]+aps[0], 2), gmp.mpz(aps[1])
    if gmp.gcd(a, gmp.gcd(b, c)) != 1:
        gcd = gmp.gcd(a, gmp.gcd(b, c))
        a, b, c = gmp.f_div(a, gcd), gmp.f_div(b, gcd), gmp.f_div(c, gcd)
    assert a**2+b**2==c**2 and c%4 == 1
    if a%2 == 0 and b%2 == 1:
        a, b = b, a
    return pt.PyTrip(a, b, c)

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



