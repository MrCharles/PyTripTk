#!/usr/bin/env python3

'''
Handles converting Pythagorean Triples to/from Euler Bricks

Follows the naming conventions specified here...
See Also: https://en.wikipedia.org/wiki/Euler_brick

This is to say:
    Pythagorean Triple (u, v, w) is the input, and ...
        Variables (a, b, c) are sides.
        Variables (d, e, f) are hypotenuse.
        If an input PT is primitive, then ABD is primitive, and...
            ACE is not primitive,
            and...
            BCF is not primitive.
        If an input PT IS NOT Primitive, then, ABD is not primitive, and...
            ACE is not primitive,
            and...
            BCF is not primitive,
            and...
            gcd(abd, ace, bcf) != 1,
            and...
            gcd(a, b, d) == gcd(a, c, e, gcd(b, c, f))

ToDo:
    At some random point in the future, handling of "evil twin" bricks,
    including relatives such as "cousins" or other multiplicative values
    may be handled. At present (initially), this functionality only
    includes reduced-form Euler Bricks (reduced to primitive form
    Pythagorean Triples). Handling of any multiplicative forms is not
    difficult, however, contentious.

    Since handling of non-primitive forms of PT and EB are simple enough,
    if a user needs this, it is left up to the user to handle. This is
    open-source GPL v3; feel free to fork and/or submit push-me-pull-you
    requests as whimsically desired (though, oft ignored).

'''

# Python Library
from collections import namedtuple as nt

# 3rd Party
import gmpy2 as gmp

# Local Package
import PyTripTk.PyTrip as pt

EulerBrick=nt('EulerBrick', ['a', 'b', 'c', 'd', 'e', 'f', 'abd', 'ace', 'bcf', 'uvw'])
EBADDOC="\nWhere (a, b, c, d, e, f) are calculated from (u, v, w), and"
EBADDOC+="\n\t'abd', 'ace' and 'bcf' are each of (respective) primitive form."
EBADDOC+="\n\tAn Euler Brick, by this software definition, "
EBADDOC+="\n\tshould yield reduced (primitve) form."
EulerBrick.__doc__ += EBADDOC

def EB_FromPT(pytrip=None):
    '''Returns a reduced-form (primitive) EulerBrick
    from an input Pythagorean Triple, uvw.

    If things seem off, the input form is reduced to primitive, or
    the input Pythagorean Triple may not be valid.

    '''
    assert not pytrip is None
    assert isinstance(pytrip, pt.PyTrip)
    if not pt.IsPrimitivePyTrip(pytrip):
        pytrip = pt.MakePrimitive(pytrip)
    u, v, w = gmp.mpz(pytrip[0]), gmp.mpz(pytrip[1]), gmp.mpz(pytrip[2])
    a, b, c = u*abs(4*v**2-w**2), v*abs(4*u**2-w**2), 4*u*v*w
    d, e, f = w**3, u*(4*v**2+w**2), v*(4*u**2+w**2)
    gcd = gmp.gcd(a, gmp.gcd(b, d))
    abd = pt.PyTrip(gmp.f_div(a, gcd), gmp.f_div(b, gcd), gmp.f_div(d, gcd))
    gcd = gmp.gcd(a, gmp.gcd(c, e))
    ace = pt.PyTrip(gmp.f_div(a, gcd), gmp.f_div(c, gcd), gmp.f_div(e, gcd))
    gcd = gmp.gcd(b, gmp.gcd(c, f))
    bcf = pt.PyTrip(gmp.f_div(b, gcd), gmp.f_div(c, gcd), gmp.f_div(f, gcd))
    gcd = gmp.gcd(u, gmp.gcd(v, w))
    uvw = pt.PyTrip(gmp.f_div(u, gcd), gmp.f_div(v, gcd), gmp.f_div(w, gcd))
    return EulerBrick(a, b, c, d, e, f, abd, ace, bcf, uvw)

def PT_ToEB(pytrip=None):
    '''Returns a reduced-form (primitive) EulerBrick
    from an input Pythagorean Triple, uvw.

    If things seem off, the input form is reduced to primitive, or
    the input Pythagorean Triple may not be valid.

    `PT_ToEB` is a proxy for `EB_FromPT`

    '''
    return EB_FromPT(pytrip)

def EB_ToPT(eulerBrick=None):
    '''Returns a reduced-form (primitive) Pythagorean Triple
    from an input EulerBrick.

    Calculations are done from the constitutent compoents,
    such that they do not rely upon the `uvw` variable.

    If things seem off, the input form is reduced to primitive, or
    the input EulerBrick may not be valid.

    `EB_ToPT` is a proxy for `PT_FromEB`

    '''
    return PT_FromEB(eulerBrick)

def PT_FromEB(eulerBrick=None):
    '''Returns a reduced-form (primitive) Pythagorean Triple
    from an input EulerBrick.

    Calculations are done from the constitutent compoents,
    such that they do not rely upon the `uvw` variable.

    If things seem off, the input form is reduced to primitive, or
    the input EulerBrick may not be valid.

    '''
    assert not eulerBrick is None
    assert isinstance(eulerBrick, EulerBrick)
    assert isinstance(eulerBricl.abd, pt.PyTrip)
    assert isinstance(eulerBricl.ace, pt.PyTrip)
    assert isinstance(eulerBricl.bcf, pt.PyTrip)
    # Assumes ABD are primitive.
    a, b, d = eulerBrick.abd[0], eulerBrick.abd[1], eulerBrick.abd[2]
    # D should always be cubed!
    assert gmp.iroot_rem(d, 3)[1] == 0
    assert eulerBrick.abd[0] > eulerBrick.ace[0]
    assert eulerBrick.abd[0]%eulerBrick.ace[0] == 0
    ace_gcd = gmp.f_div(eulerBrick.abd[0], eulerBrick.ace[0])
    assert ace_gcd != 1
    c, e = eulerBrick.ace[1]*ace_gcd, eulerBrick[2]*ace_gcd
    assert eulerBrick.abd[1]%eulerBrick.bcf[0] == 0
    bcf_gcd = gmp.f_div(eulerBrick.abd[1], eulerBrick.bcf[0])
    assert bcf_gcd != 1
    f = eulerBrick.bcf[2]*bcf_gcd
    u = gmp.gcd(c, gmp.gcd(a, e))
    v = gmp.gcd(b, f)
    assert gmp.iroot_rem(u**2+v**2, 2)[1] == 0
    w = gmp.iroot_rem(u**2+v**2, 2)[0]
    assert w**3 == d
    return pt.PyTrip(gmp.mpz(u), gmp.mpz(v), gmp.mpz(w))

if __name__ == '__main__':
    warn = "This script is not meant to be used this way."
    print(warn)
    raise Exception(warn)



