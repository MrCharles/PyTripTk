
# PyTripTk

A small Python Toolkit for working with Pythagorean Triples.

See: [Pythagorean Triple @ WikiPedia](https://en.wikipedia.org/wiki/Pythagorean_triple)

## Requirements

* Python3
    * https://python.org
    * `dpkg python3`
* GNU MP
    * https://gmplib.org
    * `dpkg libgmp-dev`
* Gmpy2
    * https://github.com/aleaxit/gmpy
    * `dpkg python3-gmpy2`

## Notes / ToDo:

* Not yet fully tested. (**TODO!**)
* This is a clean-up of personal tools and utilities.
* All of the usage examples below are working (recurisvely).
* Note that feeding large numbers without the `PatMat` aspect can take a very long time for known patterns.
    * **TODO** &mdash; finish implementing `PatMat` _(pattern matching)_ for known patterns.
    * Especially in the larger Classic tree.
* **ToDo** Implement settings and preferences for output _(based on import)_

## Technical Details:

There are two (2) known ternary trees of Pythagorean Triples:

* `Classic (   B.Berrgren, 1934)`
* `Price   (Price, H. Lee, 2008)`

* The Classic _(ternary)_ tree has a pure `C*` branch of sequental odd numbers, 5, 7, 9, 11, ... such that `(A, B, C=B+1)`
* The Classic _(ternary)_ tree has a pure `A*` branch of sequental `(A, B, C=A+2)`
* The Classic _(ternary)_ tree has a pure `B*` branch of sequental `(A, B=A+/-1, C)` ... where `B=A-1` if there are `3^Odd` children, and `B=A+1` if there are `3^Even` children, starting with the root at `(3, 4, 5)` being `3^0 == 1` and `3^1 == 3` with `(21, 20, 29)`
* Whereas, the Price _(ternary)_ tree distributes these values in binary sequence across the children, or leaves of the tree ...

```text
                3
             5 ... 7
         9, 11 ... 13, 15
17, 19, 21, 23 ... 25, 27, 29, 31
```

... and the `(A, B, C=A+2)` pieces are distributed exactly between sequential odd numbers, such that ...

```text
Price: '0AA'
     0AAA          0AAB           0AAC
(17, B, C=B+1) (A, B, C=A+2) (19, B, C=B+1)
```

... and the `(A, B=A+/-1, C)` are randomly distributed throughout the Price tree versus being the exact `B*` branch in the Classic tree.

Thus, certain patterns are easily matched. Especially those relating to Base-3 or where Base-2 and Base-3 have common patterns. _See: [OEIS](https://oeis.org/)_.

## Usage

### Imports
```python
>>> import PyTripTk.PyTrip as pt
>>> import PyTripTk.NextGen as ng
>>> import PyTripTk.PrevGen as pg
>>> import PyTripTk.ReduceRegen as rr
>>> rtpt = pt.ROOT
>>> rtpt2 = pt.Root()
>>> rtpt3 = pt.PyTrip(3, 4, 5)
>>> rtpt4 = pt.MakePyTrip(3, 4, 5)
>>> rtpt
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> rtpt2
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> rtpt3
PyTrip(a=3, b=4, c=5)
>>> rtpt4
PyTrip(a=mpz(3), b=mpz(4), c=mpz(5))
>>> assert rtpt == rtpt2 == rtpt3 == rtpt4
>>> assert rtpt != rtpt2 == rtpt3 == rtpt4
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AssertionError
>>> _
```

### Classic, Basics 1
```python
>>> ng.NextGen_Classic(rtpt)
PyTripNextGenTup(A=PyTrip(a=15, b=8, c=17), B=PyTrip(a=21, b=20, c=29), C=PyTrip(a=5, b=12, c=13))
>>> for x in ng.NextGen_Classic(rtpt):
...     print(x)
...
PyTrip(a=15, b=8, c=17)
PyTrip(a=21, b=20, c=29)
PyTrip(a=5, b=12, c=13)
>>> _
```

### Classic, Basics 2
```python
>>> ng.NextGen_Classic(rtpt, pt.PyTripBranch.A)
PyTrip(a=15, b=8, c=17)
>>> ng.NextGen_Classic(rtpt, pt.PyTripBranch.B)
PyTrip(a=21, b=20, c=29)
>>> ng.NextGen_Classic(rtpt, pt.PyTripBranch.C)
PyTrip(a=5, b=12, c=13)
>>> _
```

### Classic, Basics 3
```python
>>> pg.PrevGen_Classic(pt.PyTrip(15, 8, 17))
PyTrip(a=3, b=4, c=5)
>>> pg.PrevGen_IsClassicA(pt.PyTrip(15, 8, 17))
True
>>> pg.PrevGen_IsClassicB(pt.PyTrip(15, 8, 17))
False
>>> pg.PrevGen_IsClassicC(pt.PyTrip(15, 8, 17))
False
>>> pg.PrevGen_Classic(pt.PyTrip(21, 20, 29))
PyTrip(a=3, b=4, c=5)
>>> pg.PrevGen_IsClassicA(pt.PyTrip(21, 20, 29))
False
>>> pg.PrevGen_IsClassicB(pt.PyTrip(21, 20, 29))
True
>>> pg.PrevGen_IsClassicC(pt.PyTrip(21, 20, 29))
False
>>> pg.PrevGen_Classic(pt.PyTrip(5, 12, 13))
PyTrip(a=3, b=4, c=5)
>>> pg.PrevGen_IsClassicA(pt.PyTrip(5, 12, 13))
False
>>> pg.PrevGen_IsClassicB(pt.PyTrip(5, 12, 13))
False
>>> pg.PrevGen_IsClassicC(pt.PyTrip(5, 12, 13))
False
>>> _
```

### Classic, Basics 4
```python
>>> rr.Regen_Classic('0ABCABC')
PyTrip(a=7667, b=16644, c=18325)
>>> rr.Reduce_Classic(pt.PyTrip(7667, 16644, 18325))
'0ABCABC'
>>> rr.Regen_Classic2(pt.PyTrip(3, 4, 5), 'ABCABC')
PyTrip(a=7667, b=16644, c=18325)
>>> rr.Regen_Classic2(rtpt, 'ABCABC')
PyTrip(a=7667, b=16644, c=18325)
```

---

### Price, Basics 1
```python
>>> ng.NextGen_Price(rtpt)
PyTripNextGenTup(A=PyTrip(a=5, b=12, c=13), B=PyTrip(a=15, b=8, c=17), C=PyTrip(a=7, b=24, c=25))
>>> for x in ng.NextGen_Price(rtpt):
...     print(x)
...
PyTrip(a=5, b=12, c=13)
PyTrip(a=15, b=8, c=17)
PyTrip(a=7, b=24, c=25)
>>> _
```

### Price, Basics 2
```python
>>> ng.NextGen_Price(rtpt, pt.PyTripBranch.A)
PyTrip(a=15, b=8, c=17)
>>> ng.NextGen_Price(rtpt, pt.PyTripBranch.B)
PyTrip(a=21, b=20, c=29)
>>> ng.NextGen_Price(rtpt, pt.PyTripBranch.C)
PyTrip(a=5, b=12, c=13)
>>> _
```

### Price, Basics 3
```python
>>> pg.PrevGen_Price(pt.PyTrip(15, 8, 17))
PyTrip(a=3, b=4, c=5)
>>> pg.PrevGen_IsPriceA(pt.PyTrip(15, 8, 17))
True
>>> pg.PrevGen_IsPriceB(pt.PyTrip(15, 8, 17))
False
>>> pg.PrevGen_IsPriceC(pt.PyTrip(15, 8, 17))
False
>>> pg.PrevGen_Price(pt.PyTrip(21, 20, 29))
PyTrip(a=3, b=4, c=5)
>>> pg.PrevGen_IsPriceA(pt.PyTrip(21, 20, 29))
False
>>> pg.PrevGen_IsPriceB(pt.PyTrip(21, 20, 29))
True
>>> pg.PrevGen_IsPriceC(pt.PyTrip(21, 20, 29))
False
>>> ng.NextGen_Price(pt.PyTrip(5, 12, 13))
PyTrip(a=3, b=4, c=5)
>>> pg.PrevGen_IsPriceA(pt.PyTrip(5, 12, 13))
False
>>> pg.PrevGen_IsPriceB(pt.PyTrip(5, 12, 13))
False
>>> pg.PrevGen_IsPriceC(pt.PyTrip(5, 12, 13))
True
>>> _
```

### Price, Basics 4
```python
>>> rr.Regen_Price('0AABBCC')
PyTrip(a=1639, b=11040, c=11161)
>>> rr.Reduce_Price(pt.PyTrip(1639, 11040, 11161))
'0AABBCC'
>>> rr.Regen_Price2(pt.PyTrip(3, 4, 5), 'AABBCC')
PyTrip(a=1639, b=11040, c=11161)
>>> rr.Regen_Price2(rtpt, 'AABBCC')
PyTrip(a=1639, b=11040, c=11161)
```

### Advanced Usage

* [Euler Bricks (EB), Fibonacci Boxes (FB), Arithmetic Progression of Squares (APS)](./AAUsage.md)
* [PyTripExt &mdash; Memory Managed Struct including EB, FB and APS](./AAUsage2.md)

## License

GPL v3

Yay!

:smile:



